<?php

namespace App\Http\Controllers\Api\Cms;

use App\Http\Controllers\Api\Controller;
use App\Models\CmsHomepageBanner;
use App\Models\CmsMenuBottom;
use App\Models\CmsMenuTop;
use App\Models\CmsPromoBanner;
use App\Models\CmsSlideBanner;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    public function homeBanner()
    {
        $banner = CmsHomepageBanner::all();
        if (count($banner) > 0){
            return $this->responseBase($banner);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function homeSliderBanner()
    {
        $banner = CmsPromoBanner::all();
        if (count($banner) > 0){
            return $this->responseBase($banner);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function homeTopMenu()
    {
        $menu = CmsMenuTop::all();
        if (count($menu) > 0){
            return $this->responseBase($menu);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function homeBottomMenu()
    {
        $menu = CmsMenuBottom::all();
        if (count($menu) > 0){
            return $this->responseBase($menu);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }
}
