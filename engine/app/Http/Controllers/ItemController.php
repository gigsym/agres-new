<?php

namespace App\Http\Controllers;

use App\Models\ItemHot;
use Illuminate\Http\Request;
use App\Http\Controllers\AdditionalController;
use App\Models\Item;
use App\Models\ItemSelect;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use DateTime;

class ItemController extends Controller
{
    protected $AdditionalController;
    public function __construct(AdditionalController $AdditionalController)
    {
        $this->AdditionalController = $AdditionalController;
    }

    public function show($slug)
    {
        $item = Item::with("image", "review", "category", "subcategory", "longDescription", "imagePrimary","variants")->where("slug", $slug)->first();
        $item->view_counter += 1;
        $item->save();
        foreach ($item->variants as $v) {
            $end_date = new Carbon($v->end_deal);
            $diffInSeconds = Carbon::now()->diffInSeconds($end_date, false);
            // if ($diffInSeconds > 1) {
            //     $dtF = new DateTime('@0');
            //     $dtT = new DateTime("@$diffInSeconds");
            //     $finalDiff =  $dtF->diff($dtT);
            //     $diff['day'] = $finalDiff->d;
            //     $diff['jam'] = $finalDiff->h;
            //     $diff['menit'] = $finalDiff->i;
            //     $diff['detik'] = $finalDiff->s;
            //     $v->end_date = $diff;
            // }

            if ($v->discount_price != null && $diffInSeconds > 1) {
                // discount
                $discount = (($v->price - $v->discount_price) * 100) / $v->price;
                $v->discount = '-' . floor($discount) . '%';
            }
        }
        $d["data"] = $item;
        $d["variant"] = $item->variants;
        $d["featured"] = Item::with("image", "review", "category", "subcategory", "longDescription", "imagePrimary")->get()->take(5);
        return view("productdetail", $d);
    }

    public function getSubTotal(Request $request)
    {
        $varian = ItemStockVariant::find($request->variant_id);
        if ($varian->discount_price != null) {
            $sub_total = $varian->discount_price * $request->qty;
        } else {
            $sub_total = $varian->price * $request->qty;
        }
        return $sub_total;
    }
}
