<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Category;
use App\Models\City;
use App\Models\District;
use App\Models\ItemImage;
use App\Models\ItemReview;
use App\Models\Province;
use App\Models\Suburb;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;
use Carbon\Carbon;
use DateTime;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function kirimOtp($param)
    {
        $client = new Client([
            'base_uri' => 'https://admin.ladiestory.id/kirimwamo.php?act=device52',
            'timeout'  => 10,
        ]);
        $client->post("", ["json" => $param]);
        // try {
        //     $client->post("", ["json" => $param]);
        // } catch (\GuzzleHttp\Exception\GuzzleException $th) {
        //     return $th;
        // }
        return $client;
    }

    public function itemCategory()
    {
        $categories = Category::with('subcat')->get();
        foreach ($categories as $category){
            $category->spesification = json_decode($category->spesification);
        }

        return $categories;
    }
    public function getProvince()
    {
        $province = Province::get();
        return $province;
    }
    public function getCity($province_id)
    {
        $province = City::where('province_id', $province_id)->get();
        return $province;
    }
    public function getDistrict($city_id)
    {
        $province = Suburb::where('city_id', $city_id)->get();
        return $province;
    }
    public function getArea($district_id)
    {
        $province = Area::where('suburb_id', $district_id)->get();
        return $province;
    }
}
