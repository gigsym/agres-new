<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use App\Models\District;
use App\Models\MemberAddress;
use App\Models\Province;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index($member_id, $message = '')
    {
        $addresses = MemberAddress::with('province','city','district','area')->where('member_id', $member_id)->orderBy('is_primary', 'desc')->get();
        foreach ($addresses as $k => $address){
            $address->province_name = $address->province->name ?? 'province null';
            $address->city_name = ($address->city->name ?? 'city type null');
            $address->district_name = $address->district->name ?? 'district null';
            $address->area_name = $address->area->name ?? 'district null';
            $address->unsetRelation('province')->unsetRelation('city')->unsetRelation('district')->unsetRelation('area');
        }
        if ($addresses->isNotEmpty()) {
            return $this->responseBase($addresses, 200, $message);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function store(Request $request, $member_id)
    {
        $store = new MemberAddress;
        $store->member_id = $member_id;
        $store->address_name = $request->address_name;
        $store->name = $request->nama_penerima;
        $store->phone = $request->phone_penerima;
        $store->address = $request->full_address;
        $store->province_id = $request->province_id;
        $store->city_id = $request->city_id;
        $store->district_id = $request->district_id;
        $store->area_id = $request->area_id;
        $store->postal_code = $request->postal_code;
        $store->save();

        return $this->index($member_id, 'success add address');
    }

    public function show($member_id,$id)
    {
        $address = MemberAddress::find($id);

        if ($address != null) {
            return $this->responseBase($address);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function update(Request $request,$member_id, $id)
    {
        $update = MemberAddress::find($id);
        $update->address_name = $request->address_name;
        $update->name = $request->nama_penerima;
        $update->phone = $request->phone_penerima;
        $update->address = $request->full_address;
        $update->province_id = $request->province_id;
        $update->city_id = $request->city_id;
        $update->district_id = $request->district_id;
        $update->area_id = $request->area_id;
        $update->postal_code = $request->postal_code;
        $update->save();

        return $this->index($member_id, 'success update address');
    }

    public function destroy($member_id, $id)
    {
        $address = MemberAddress::find($id);
        $address->delete();
        return $this->index($member_id, 'success delete address');
    }

    public function updateStatus(Request $request, $member_id, $id)
    {
        $check = MemberAddress::where('status',$request->status)->where('member_id', $member_id)->first();
        if ($check != null){
            $check->status = 0;
            $check->save();
        }
        $address = MemberAddress::find($id);
        $address->status = $request->status;
        $address->save();
        return $this->index($member_id, 'success update status address');
    }

    public function setPrimaryAddress($member_id, $id)
    {
        $setzero = MemberAddress::where('member_id', $member_id)->update(['is_primary' => 0]);
        $setone = MemberAddress::find($id)->update(['is_primary' => 1]);
        return $this->responseBase([], 200, "success set primary address");
    }
}
