<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberPasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\verifRegister;
use Kreait\Firebase\Request as FirebaseRequest;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use JWTAuth;
use Ramsey\Uuid\Uuid;
use Image;

class LoginController extends Controller
{
    public function showLoginEmail()
    {
        return view("auth.login");
    }

    public function loginEmail(Request $request)
    {
        $login_type = filter_var($request->email_phone, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $credentials = ['email' => $request->email, 'password' => $request->password];
        if (!Auth::attempt($credentials)) {
            // RateLimiter::hit($this->throttleKey());

            throw ValidationException::withMessages([
                'email' => __('auth.failed'),
            ]);
        }
        if (empty(Auth::user()->email_verified_at)) {
            return response()->json('email belum di verif', 400);
        }
        // $token = [
        //     'access_token' => $token,
        //     'token_type' => 'bearer',
        // ];
        return redirect(route('home'));
    }

    public function showRegister()
    {
        return view("auth.register-email");
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:members,email',
            'password' => 'required|min:8',
            'policy' => 'required'
        ]);

        // $subno = $request->phone;
        // if ($request->phone[0] == "+") {
        //     $subno = substr($request->phone, 3, strlen($subno));
        // } else if (substr($request->phone, 0, 2) == "62") {
        //     $subno = substr($request->phone,  2, strlen($subno));
        // } else if ($request->phone[0] == "0") {
        //     $subno = substr($request->phone, 1, strlen($subno));
        // }

        // if (Member::where('email', $request->password)->orWhere('phone', $subno)->first() != null) {
        //     return response()->json('email / phone sudah di pakai', 400);
        // }
        // $arr['phone'] = $subno;
        $arr['password'] = bcrypt($request->password);
        $arr['email_verified_at'] = Carbon::now();
        $request->merge($arr);
        $member = Member::create($request->except('_token', 'policy'));
        /////////////////////////
        Auth::login($member);
        //////////////////
        $token = Uuid::uuid4()->getHex();
        MemberPasswordReset::create([
            'member_id' => $member->id,
            'token' => $token,
            'type' => 'verification'

        ]);
        $url = url('/') . '/member/verification?token=' . $token;

        $details = [
            'url' => $url,
        ];

        Mail::to($member->email)->send(new verifRegister($details));

        return redirect(route('home'));
    }

    public function verification(Request $request)
    {
        $check = MemberPasswordReset::where('token', $request->token)->where('type', 'verification')->first();
        if ($check != null) {
            $member = Member::find($check->member_id);
            $member->email_verified_at = Carbon::now();
            $member->save();

            $check->delete();
            Auth::login($member);
            return redirect(route('home'));
        } else {
            return response()->json('token invalid', 200);
        }
    }

    public function loginWa()
    {
        return view('auth.login-wa');
    }
    public function loginWaOtp(Request $request)
    {
        $member = null;
        $subno = $request->phone;
        if ($request->phone[0] == "+") {
            $subno = substr($request->phone, 3, strlen($subno));
        } else if (substr($request->phone, 0, 2) == "62") {
            $subno = substr($request->phone,  2, strlen($subno));
        } else if ($request->phone[0] == "0") {
            $subno = substr($request->phone, 1, strlen($subno));
        }
        $member = Member::where("phone", "like", "%" . $subno . "%")->first();

        if ($member == null) {
            $member = Member::create([
                'phone' => $request->phone,
            ]);
        }

        $otp =  mt_rand(100000, 999999);
        $passres = MemberPasswordReset::where("member_id", $member->id)->first();
        if ($passres) {
            $passres->token = strtoupper($otp);
            $passres->type = "login";
            $passres->save();
        } else {
            $passres = MemberPasswordReset::create([
                "member_id" => $member->id,
                "token" => strtoupper($otp),
                "type" => 'login',
                "updated_at" => Carbon::now()
            ]);
        }

        $string = "otp anda: *" . strtoupper($otp) . "* expired dalam 15 menit.";
        $data["contact"] = $request->phone;
        $data["msg"] = $string;
        // $this->kirimOtp($data);

        return redirect('member/login/wa-verif/' . $request->phone);
    }
    public function loginWaVerif($phone)
    {
        $d['phone'] = $phone;
        return view('auth.verify-wa', $d);
    }

    public function loginWaVerifOtp(Request $request)
    {
        $member = null;
        $subno = $request->phone;
        if ($request->phone[0] == "+") {
            $subno = substr($request->phone, 3, strlen($subno));
        } else if (substr($request->phone, 0, 2) == "62") {
            $subno = substr($request->phone,  2, strlen($subno));
        } else if ($request->phone[0] == "0") {
            $subno = substr($request->phone, 1, strlen($subno));
        }
        $member = Member::where("phone", "like", "%" . $subno . "%")->first();

        if ($member != null) {
            $checkotp = MemberPasswordReset::where("member_id", $member->id)->where("token", $request->otp)->where("type", "login")->first();
            if ($checkotp) {
                $now = Carbon::now();
                $minutes = $now->diffInMinutes($checkotp->updated_at);
                if ($minutes <= 15) {

                    $member->email_verified_at = Carbon::now();
                    $member->save();

                    Auth::login($member);
                    // Auth::loginUsingId($member->id, true);
                    return redirect('/');
                } else {
                    return back()->with(['error' => "otp expired"]);
                }
            } else {
                return back()->with(['error' => "otp yang anda masukan salah"]);
            }
        } else {
            return redirect()->back()->with(['error' => "(harusnya error ini tidak muncul) member dengan no hp tersebut tidak ditemukan"]);
        }
        return redirect()->back()->with(['error' => "SERVER ERROR"]);
    }

    public function loginWithSosmed(Request $request)
    {
        $member = Member::where('email', $request->email)->first();

        if ($member != null) {
            Auth::login($member);
            // Auth::loginUsingId($member->id, true);
        } else {


            $member = new Member;
            $member->email = $request->email;
            // $member->password = bcrypt($request->token);
            $member->name = $request->name;
            $member->phone = $request->phone;

            if ($request->has('image')) {

                $image = file_get_contents($request->image);
                $destinationPath = 'assets/image/profile/';
                $img = Image::make($image);
                $ex = explode('/', $img->mime);
                $imageName =  uniqid() . '_' . str_replace(' ', '_', $request->name) . '.' . $ex[1];
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $img->resize(674, 674, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $member->image = $destinationPath . '/' . $imageName;
            }
            $member->save();

            Auth::login($member);
        }
        $res = Auth::check();
        return $res;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
