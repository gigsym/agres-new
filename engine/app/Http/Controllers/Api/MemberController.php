<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models\Category;
use App\Models\ItemImage;
use App\Models\Member;
use App\Models\MemberAddress;
use App\Models\MemberWishlist;
use App\Models\SubCategory;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Image;

class MemberController extends Controller
{
    public function profile()
    {
        $profile = Member::with('primaryAddress.province', 'primaryAddress.city', 'primaryAddress.district', 'primaryAddress.area')->where('id', Auth('api')->user()->id)->first();
        if ($profile->primaryAddress != null) {
            $address = $profile->primaryAddress;
            $address->province_name = $profile->primaryAddress->province->name ?? 'province null';
            $address->city_name = ($profile->primaryAddress->city->name ?? 'city type null');
            $address->district_name = $profile->primaryAddress->district->name ?? 'district null';
            $address->area_name = $profile->primaryAddress->area->name ?? 'district null';
            $address->unsetRelation('province')->unsetRelation('city')->unsetRelation('district')->unsetRelation('area');
        }

        return $this->responseBase($profile);
    }

    public function updateProfile(Request $request)
    {
        $profile = Member::find(Auth('api')->user()->id);
        if ($request->name != null) {
            $profile->name = $request->name; // ingatkan saya untuk mengganti ini dengan foreach
        }
        if ($request->phone != null) {
            $profile->phone = $request->phone;
        }
        if ($request->email != null && $profile->login_from != 2) { // login_from 2 == sosmed tidak update email
            $profile->email = $request->email;
        }
        if ($request->password != null) {
            $profile->password = bcrypt($request->password);
        }
        if ($request->birthday != null) {
            $profile->birthday = Carbon::parse($request->birthday);
        }
        if ($request->gender != null) {
            $profile->gender = $request->gender;
        }

        if (($request->has("image")) && ($request->image != null)) {
            $image = file_get_contents($request->image);
            $destinationPath = 'assets/image/profile/';
            $img = Image::make($image);
            $ex = explode('/', $img->mime);
            $imageName =  uniqid() . '_' . str_replace(' ', '_', $request->name) . '.' . $ex[1];
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $img->resize(674, 674, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);

            $profile->image = $destinationPath . '/' . $imageName;
        }
        $profile->save();
        return $this->responseBase([], 200, "success edit profile");
    }
    public function getWishlist()
    {
        $wishlist = MemberWishlist::with('product')->where('member_id', auth('api')->user()->id)->get();
        foreach ($wishlist as $w) {
            $image = ItemImage::where('item_id', $w->product->id)->where('is_primary', 1)->first();
            $w->product->image = null;
            if ($image != null) {
                $w->product->image = $image->file;
            } else {
                $image = ItemImage::where('item_id', $w->product->id)->first();
                if ($image != null) {
                    $w->product->image = $image->file;
                }
            }

            $cat = Category::where('id', $w->product->category_id)->first();
            $w->product->category_name = null;
            if ($cat != null) {
                $w->product->category_name = $cat->name;
            }

            $subcat = SubCategory::where('id', $w->product->sub_category_id)->first();
            $w->product->sub_category_name = null;
            if ($subcat != null) {
                $w->product->sub_category_name = $subcat->name;
            }
        }

        if ($wishlist != null) {
            return $this->responseBase($wishlist);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function createWishlist(Request $request)
    {
        $create = MemberWishlist::create([
            "member_id" => auth('api')->user()->id,
            "item_id" => $request->item_id
        ]);

        return $this->responseBase([], 200, 'Success add to wishlist');
    }

    public function deleteWishlist($id)
    {
        $detele = MemberWishlist::find($id);
        $detele->delete();

        return $this->responseBase([], 200, 'Success delete wishlist');
    }
}
