@extends('layouts.master')
@section('content-main')

<!-- start:banner -->
<section class="banner banner--rounded">
    <div class="container">
        <!-- start:breadcrumb navigation -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item active">{{$category->name}}</li>
            </ol>
        </nav>
        <!-- end:/breadcrumb navigation -->

        <!-- start:carousel slider -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($banners as $k => $v)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}" @if ($loop->iteration == 1)
                    class="active" @endif></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($banners as $banner)
                <div class="carousel-item @if ($loop->iteration == 1) active @endif">
                    <img src="https://picsum.photos/600/400" class="d-block w-100" alt="Banner">
                    {{-- <img src="{{asset($banner->image_desktop)}}" class="d-block w-100" alt="Banner"> --}}
                </div>
                @endforeach
                {{-- <div class="carousel-item active">
                    <img src="./assets/images/banner-slider.jpg" class="d-block w-100" alt="Banner">
                </div>
                <div class="carousel-item">
                    <img src="./assets/images/banner-slider.jpg" class="d-block w-100" alt="Banner">
                </div>
                <div class="carousel-item">
                    <img src="./assets/images/banner-slider.jpg" class="d-block w-100" alt="Banner">
                </div> --}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- end:/carousel slider -->
    </div>
</section>
<!-- end:/banner -->

<!-- start:category item -->
<section class="category category--item">
    <div class="container">
        <div class="category__wrap">
            <!-- start:category box -->
            @foreach ($sub_categories as $sub_cat)
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="https://picsum.photos/100/100" alt="Image product">
                    {{-- <img src="{{asset($sub_cat->icon)}}" alt="Image product"> --}}
                </div>
                <p>{{$sub_cat->name}}</p>
            </a>
            @endforeach
            <!-- end:/category box -->

            {{-- <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/products/product-category-3.jpg" alt="Image product">
                </div>
                <p>Laptop Hybrid</p>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/products/product-category-4.jpg" alt="Image product">
                </div>
                <p>Laptop Multimedia</p>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/products/product-category-5.jpg" alt="Image product">
                </div>
                <p>Laptop Bisnis</p>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/products/product-category-6.jpg" alt="Image product">
                </div>
                <p>Laptop <br> Sehari-hari</p>
            </a>
            <!-- end:/category box --> --}}
        </div>
    </div>
</section>
<!-- end/:category item -->

<!-- start:product terlaris -->
<section class="product mt-4">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>TERLARIS</h3>
            <p>Laptop Terlaris Minggu Ini!</p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--category slider--category--full">
                @foreach ($bestsellers as $best)
                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <a href="" class="addToWishlist" data-item_id="{{$best->id}}">
                            <div
                                class="slider__box__favorite @if (Auth::check() && in_array($best->id, Auth::user()->wishlist->pluck('item_id')->toArray())) active @endif">
                                <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                        fill="white" stroke="#B21628" />
                                </svg>
                            </div>
                        </a>
                        <div class="slider__box__image">
                            <img class="image-slider" src="{{asset($best->image)}}" alt="image product">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">{{$best->subCategory->name}}</div>
                                <div class="block__title">{{$best->name}}</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="{{$best->avg_rating}}"></div>
                                    <span>{{$best->total_rating}}</span>
                                </div>
                                @if ($best->discount != null)
                                <div class="block__discount">
                                    <div class="percent">{{$best->dicsount}}</div>
                                    <div class="price">{{$best->dicsount_price}}</div>
                                </div>
                                <div class="block__price">{{$best->price}}</div>
                                @else
                                <div class="block__price">{{$best->price}}</div>
                                @endif
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="featured" id="featured_{{$best->id}}"
                                            data-item_id="{{$best->id}}" data-src="{{asset($best->image)}}"
                                            data-label="featured_" data-name="{{$best->name}}" type="checkbox">
                                        <label class="form-check-label" for="featured_{{$best->id}}">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary addToCart"
                                            data-item_id="{{$best->id}}" data-price="{{$best->dicsount_price}}">+
                                            Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->
                @endforeach

                {{-- <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-2.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN 5-3550H/GTX
                                    1050/3GB/...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris2" type="checkbox"
                                            checked>
                                        <label class="form-check-label" for="terlaris2">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris3" type="checkbox">
                                        <label class="form-check-label" for="terlaris3">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-2.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN 5-3550H/GTX
                                    1050/3GB/...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris4" type="checkbox">
                                        <label class="form-check-label" for="terlaris4">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-1.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris5" type="checkbox">
                                        <label class="form-check-label" for="terlaris5">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-2.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN 5-3550H/GTX
                                    1050/3GB/...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris6" type="checkbox">
                                        <label class="form-check-label" for="terlaris6">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-small" data-rating="4"></div>
                                    <span>(25)</span>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">60999000</div>
                                </div>
                                <div class="block__price">48799200</div>
                                <div class="block__checkbox">
                                    <div class="form-check abc-checkbox abc-checkbox-primary">
                                        <input class="form-check-input" name="terlaris" id="terlaris7" type="checkbox">
                                        <label class="form-check-label" for="terlaris7">
                                            Compare
                                        </label>
                                    </div>
                                </div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box --> --}}
            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/product terlaris -->

<!-- start:banner promo -->
<section class="banner-promo banner-promo--same margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>PROMO MENARIK LAINNYA</h3>
            <p>Rekomendasi Promo Bulan Ini!</p>
        </div>
        <!-- end:/title section -->

        <!-- start:block -->
        <div class="block">
            <div class="banner-promo__left">
                @if (count($promos) >= 1)
                <div class="banner-promo__left__top">
                    <a href="{{$promos[0]->url ?? "#"}}" class="block__link">
                        <img class="block__image" src="{{asset($promos[0]->banner_desktop)}}" alt="Banner promo">
                    </a>
                </div>
                @endif
                @if (count($promos) >= 2)
                <div class="banner-promo__left__bottom">
                    <a href="{{$promos[1]->url ?? "#"}}" class="block__link">
                        <img class="block__image" src="{{asset($promos[1]->banner_desktop)}}" alt="Banner promo">
                    </a>
                </div>
                @endif
            </div>
            <div class="banner-promo__right">
                @if (count($promos) >= 3)
                <div class="banner-promo__right__top">
                    <a href="{{$promos[2]->url ?? "#"}}" class="block__link">
                        <img class="block__image" src="{{asset($promos[2]->banner_desktop)}}" alt="Banner promo">
                    </a>
                </div>
                @endif
                @if (count($promos) >= 4)
                <div class="banner-promo__right__bottom">
                    <a href="{{$promos[3]->url ?? "#"}}" class="block__link">
                        <img class="block__image" src="{{asset($promos[3]->banner_desktop)}}" alt="Banner promo">
                    </a>
                </div>
                @endif
            </div>
        </div>
        <!-- end:/block -->
    </div>
</section>
<!-- end:/banner promo -->
@foreach ($sub_categories as $subcat)
<!-- start:product rekomendasi laptop gaming -->
<section class="product product--featured product--category">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI {{$subcat->name}}</h3>
            <p>{{$subcat->name}} Paling Diminati</p>
        </div>
        <!-- end:/title section -->

        <div class="row">
            <div class="col-lg-4 col-padless-right">
                <div class="block">
                    <a href="#" class="block__link">
                        <img class="block__image" src="{{asset($subcat->banner)}}" alt="Banner image">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--category slider--category--featured">
                            @foreach ($subcat->recItem as $item)
                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <a href="" class="addToWishlist" data-item_id="{{$item->id}}">
                                        <div
                                            class="slider__box__favorite @if (Auth::check() && in_array($item->id, Auth::user()->wishlist->pluck('item_id')->toArray())) active @endif">
                                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                    fill="white" stroke="#B21628" />
                                            </svg>
                                        </div>
                                    </a>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="{{asset($item->image)}}" alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">{{$subcat->name}}</div>
                                            <div class="block__title">{{$item->name}}</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="{{$item->avg_rating}}"></div>
                                                <span>{{$item->total_rating}}</span>
                                            </div>
                                            @if ($item->discount != null)
                                            <div class="block__discount">
                                                <div class="percent">{{$item->dicsount}}</div>
                                                <div class="price">{{$item->dicsount_price}}</div>
                                            </div>
                                            <div class="block__price">{{$item->price}}</div>
                                            @else
                                            <div class="block__price">{{$item->price}}</div>
                                            @endif
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="featured"
                                                        id="rekomendasi_{{$item->id}}" data-item_id="{{$item->id}}"
                                                        data-src="{{asset($item->image)}}" data-name="{{$item->name}}"
                                                        data-label="rekomendasi_" type="checkbox">
                                                    <label class="form-check-label" for="rekomendasi_{{$item->id}}">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary addToCart"
                                                        data-item_id="{{$item->id}}"
                                                        data-price="{{$item->dicsount_price}}">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->
                            @endforeach

                        </div>
                        <!-- end:/slider -->
                    </div>
                    <!-- end:/product slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- emd:/product rekomendasi laptop gaming -->
@endforeach


{{-- <!-- start:product rekomendasi laptop hybrid -->
<section class="product product--featured product--category margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI LAPTOP HYBRID</h3>
            <p>Laptop Hybrid Paling Diminati</p>
        </div>
        <!-- end:/title section -->

        <div class="row">
            <div class="col-lg-4 col-padless-right">
                <div class="block">
                    <a href="#" class="block__link">
                        <img class="block__image" src="./assets/images/banner-featured-2.jpg" alt="Banner image">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--category slider--category--featured">
                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-hybrid"
                                                        id="laptop-hybrid1" type="checkbox">
                                                    <label class="form-check-label" for="laptop-hybrid1">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-hybrid"
                                                        id="laptop-hybrid2" type="checkbox" checked>
                                                    <label class="form-check-label" for="laptop-hybrid2">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-hybrid"
                                                        id="laptop-hybrid3" type="checkbox">
                                                    <label class="form-check-label" for="laptop-hybrid3">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-hybrid"
                                                        id="laptop-hybrid4" type="checkbox">
                                                    <label class="form-check-label" for="laptop-hybrid4">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-hybrid"
                                                        id="laptop-hybrid5" type="checkbox">
                                                    <label class="form-check-label" for="laptop-hybrid5">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->
                        </div>
                        <!-- end:/slider -->
                    </div>
                    <!-- end:/product slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- emd:/product rekomendasi laptop hybrid -->

<!-- start:banner promo special deals -->
<section class="banner-promo banner-promo--same margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>SPECIAL DEALS</h3>
            <p>Promo Menarik Lainnya</p>
        </div>
        <!-- end:/title section -->

        <!-- start:block -->
        <div class="block block--special">
            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-1.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->

            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-2.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->

            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-3.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->

            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-4.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->
        </div>
        <!-- end:/block -->
    </div>
</section>
<!-- end:/banner promo special deals -->

<!-- start:product rekomendasi laptop multimedia -->
<section class="product product--featured product--category">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI LAPTOP MULTIMEDIA</h3>
            <p>Laptop Multimedia Paling Diminati</p>
        </div>
        <!-- end:/title section -->

        <div class="row">
            <div class="col-lg-4 col-padless-right">
                <div class="block">
                    <a href="#" class="block__link">
                        <img class="block__image" src="./assets/images/banner-featured-1.jpg" alt="Banner image">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--category slider--category--featured">
                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite active">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="https://picsum.photos/600/400"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-multimedia"
                                                        id="laptop-multimedia1" type="checkbox">
                                                    <label class="form-check-label" for="laptop-multimedia1">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-multimedia"
                                                        id="laptop-multimedia2" type="checkbox" checked>
                                                    <label class="form-check-label" for="laptop-multimedia2">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-multimedia"
                                                        id="laptop-multimedia3" type="checkbox">
                                                    <label class="form-check-label" for="laptop-multimedia3">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-multimedia"
                                                        id="laptop-multimedia4" type="checkbox">
                                                    <label class="form-check-label" for="laptop-multimedia4">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-multimedia"
                                                        id="laptop-multimedia5" type="checkbox">
                                                    <label class="form-check-label" for="laptop-multimedia5">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->
                        </div>
                        <!-- end:/slider -->
                    </div>
                    <!-- end:/product slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- emd:/product rekomendasi laptop multimedia -->

<!-- start:product rekomendasi laptop bisnis -->
<section class="product product--featured product--category margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI LAPTOP BISNIS</h3>
            <p>Laptop Bisnis Paling Diminati</p>
        </div>
        <!-- end:/title section -->

        <div class="row">
            <div class="col-lg-4 col-padless-right">
                <div class="block">
                    <a href="#" class="block__link">
                        <img class="block__image" src="./assets/images/banner-featured-2.jpg" alt="Banner image">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--category slider--category--featured">
                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-bisnis"
                                                        id="laptop-bisnis1" type="checkbox">
                                                    <label class="form-check-label" for="laptop-bisnis1">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-bisnis"
                                                        id="laptop-bisnis2" type="checkbox" checked>
                                                    <label class="form-check-label" for="laptop-bisnis2">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-bisnis"
                                                        id="laptop-bisnis3" type="checkbox">
                                                    <label class="form-check-label" for="laptop-bisnis3">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-bisnis"
                                                        id="laptop-bisnis4" type="checkbox">
                                                    <label class="form-check-label" for="laptop-bisnis4">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-bisnis"
                                                        id="laptop-bisnis5" type="checkbox">
                                                    <label class="form-check-label" for="laptop-bisnis5">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->
                        </div>
                        <!-- end:/slider -->
                    </div>
                    <!-- end:/product slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- emd:/product rekomendasi laptop bisnis -->

<!-- start:product rekomendasi laptop sehari-hari -->
<section class="product product--featured product--category margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI LAPTOP SEHARI-HARI</h3>
            <p>Laptop Sehari-hari Paling Diminati</p>
        </div>
        <!-- end:/title section -->

        <div class="row">
            <div class="col-lg-4 col-padless-right">
                <div class="block">
                    <a href="#" class="block__link">
                        <img class="block__image" src="./assets/images/banner-featured-1.jpg" alt="Banner image">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--category slider--category--featured">
                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-sehari-hari"
                                                        id="laptop-sehari-hari1" type="checkbox">
                                                    <label class="form-check-label" for="laptop-sehari-hari1">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-sehari-hari"
                                                        id="laptop-sehari-hari2" type="checkbox" checked>
                                                    <label class="form-check-label" for="laptop-sehari-hari2">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-sehari-hari"
                                                        id="laptop-sehari-hari3" type="checkbox">
                                                    <label class="form-check-label" for="laptop-sehari-hari3">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-2.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">HP Pavilion Gaming 15-EC0001AX-AMD RYZEN
                                                5-3550H/GTX 1050/3GB/...</div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-sehari-hari"
                                                        id="laptop-sehari-hari4" type="checkbox">
                                                    <label class="form-check-label" for="laptop-sehari-hari4">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->

                            <!-- start:slider box -->
                            <div class="slider__box">
                                <div class="slider-wrap">
                                    <div class="slider__box__label">BEST</div>
                                    <div class="slider__box__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="./assets/images/products/product-1.jpg"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T
                                                I7-9750H/32GB/1TB SSD/RTX2070...
                                            </div>
                                            <div class="block__star">
                                                <div class="star-small" data-rating="4"></div>
                                                <span>(25)</span>
                                            </div>
                                            <div class="block__discount">
                                                <div class="percent">-20%</div>
                                                <div class="price">60999000</div>
                                            </div>
                                            <div class="block__price">48799200</div>
                                            <div class="block__checkbox">
                                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                                    <input class="form-check-input" name="laptop-sehari-hari"
                                                        id="laptop-sehari-hari5" type="checkbox">
                                                    <label class="form-check-label" for="laptop-sehari-hari5">
                                                        Compare
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="block__action">
                                                <div class="block__action__cart">
                                                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/slider box -->
                        </div>
                        <!-- end:/slider -->
                    </div>
                    <!-- end:/product slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- emd:/product rekomendasi laptop sehari-hari --> --}}

<!-- start:banner promo special deals -->
<section class="banner-promo banner-promo--same margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>SPECIAL DEALS</h3>
            <p>Promo Menarik Lainnya</p>
        </div>
        <!-- end:/title section -->

        <!-- start:block -->
        <div class="block block--special">
            @foreach ($spesial_deals as $spesial_deal)
            <!-- start:promo item -->
            <a href="{{$spesial_deal->url ?? '#'}}" class="banner-promo__item">
                <img src="{{asset($spesial_deal->banner_desktop)}}" alt="banner promo">
            </a>
            <!-- end:/promo item -->
            @endforeach

            {{-- <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-2.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->

            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-3.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item -->

            <!-- start:promo item -->
            <a href="#" class="banner-promo__item">
                <img src="./assets/images/banner-special-4.jpg" alt="banner promo">
            </a>
            <!-- end:/promo item --> --}}
        </div>
        <!-- end:/block -->
    </div>
</section>
<!-- end:/banner promo special deals -->

<!-- start:category brand favorit -->
<section class="category category--brand margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section text-center">
            <h3>BRAND FAVORIT KAMU</h3>
            <p>Dapatkan Laptop Dari Brand Favorit</p>
        </div>
        <!-- end:/title section -->

        <div class="category__wrap">
            @foreach ($brands as $brand)
            <!-- start:category box -->
            <a href="{{$brand->url}}" class="category__box">
                <div class="category__box__image">
                    <img src="{{asset($brand->icon)}}" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box -->
            @endforeach

            {{-- <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/product-brand/brand-2.jpg" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/product-brand/brand-1.jpg" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/product-brand/brand-2.jpg" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/product-brand/brand-1.jpg" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box -->

            <!-- start:category box -->
            <a href="#" class="category__box">
                <div class="category__box__image">
                    <img src="./assets/images/product-brand/brand-2.jpg" alt="Image brand">
                </div>
            </a>
            <!-- end:/category box --> --}}
        </div>
    </div>
</section>
<!-- end/:category brand favorit -->

<!-- start:product rekomendasi -->
<section class="product">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>REKOMENDASI UNTUK KAMU</h3>
            <p>Laptop Rekomendasi Terbaik</p>
        </div>
        <!-- end:/title section -->

        <!-- start:product list -->
        <div class="product-list list-6">
            <div class="flex-container wrap">
                @foreach ($data as $product)
                <!-- start:product item -->
                <div class="product-list__item">
                    <div class="item__label">BEST</div>
                    <div class="item__favorite">
                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                fill="white" stroke="#B21628" />
                        </svg>
                    </div>
                    <div class="item__image">
                        <img class="image-slider" src="{{asset($product->image)}}"
                            alt="image product">
                    </div>
                    <div class="item__content">
                        <div class="block">
                            <div class="block__tags">{{$subcat->name}}</div>
                            <div class="block__title">{{$item->name}}
                            </div>
                            <div class="block__star">
                                <div class="star-small" data-rating="{{$item->avg_rating}}"></div>
                                <span>{{$item->total_rating}}</span>
                            </div>
                            <div class="block__discount">
                                <div class="percent">-20%</div>
                                <div class="price">Rp. 60.999.000</div>
                            </div>
                            <div class="block__price">Rp. 48.799.200</div>
                            @if ($item->discount != null)
                            <div class="block__discount">
                                <div class="percent">{{$item->dicsount}}</div>
                                <div class="price">{{$item->price}}</div>
                            </div>
                            <div class="block__price">{{$item->dicsount_price}}</div>
                            @else
                            <div class="block__price">{{$item->price}}</div>
                            @endif
                            <div class="block__checkbox">
                                <div class="form-check abc-checkbox abc-checkbox-primary">
                                    <input class="form-check-input" name="featured" id="rek_{{$item->id}}"
                                        data-item_id="{{$item->id}}" data-src="{{asset($item->image)}}"
                                        data-label="rek_" data-name="{{$item->name}}" type="checkbox">
                                    <label class="form-check-label" for="rek_{{$item->id}}">
                                        Compare
                                    </label>
                                </div>
                            </div>
                            <div class="block__action">
                                <div class="block__action__cart">
                                    <button type="button" class="btn btn-primary addToCart" data-item_id="{{$item->id}}"
                                        data-price="{{$item->dicsount_price}}">+ Keranjang</button>
                                </div>
                                <div class="block__action__buy">
                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/product item -->
                @endforeach

                {{--<!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-2.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product2" type="checkbox">
                    <label class="form-check-label" for="product2">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-3.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product3" type="checkbox">
                    <label class="form-check-label" for="product3">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-4.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product4" type="checkbox">
                    <label class="form-check-label" for="product4">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-5.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product5" type="checkbox">
                    <label class="form-check-label" for="product5">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-6.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product6" type="checkbox">
                    <label class="form-check-label" for="product6">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-6.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product7" type="checkbox">
                    <label class="form-check-label" for="product7">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-5.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product8" type="checkbox">
                    <label class="form-check-label" for="product8">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-4.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product9" type="checkbox">
                    <label class="form-check-label" for="product9">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-3.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product10" type="checkbox">
                    <label class="form-check-label" for="product10">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-2.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product11" type="checkbox">
                    <label class="form-check-label" for="product11">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end:/product item -->

          <!-- start:product item -->
          <div class="product-list__item">
            <div class="item__label">BEST</div>
            <div class="item__favorite">
              <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                  fill="white" stroke="#B21628" />
              </svg>
            </div>
            <div class="item__image">
              <img class="image-slider" src="./assets/images/products/product-category-1.jpg" alt="image product">
            </div>
            <div class="item__content">
              <div class="block">
                <div class="block__tags">Laptop Gaming</div>
                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB SSD/RTX2070...
                </div>
                <div class="block__star">
                  <div class="star-small" data-rating="4"></div>
                  <span>(25)</span>
                </div>
                <div class="block__discount">
                  <div class="percent">-20%</div>
                  <div class="price">Rp. 60.999.000</div>
                </div>
                <div class="block__price">Rp. 48.799.200</div>
                <div class="block__checkbox">
                  <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="product" id="product12" type="checkbox">
                    <label class="form-check-label" for="product12">
                      Compare
                    </label>
                  </div>
                </div>
                <div class="block__action">
                  <div class="block__action__cart">
                    <button type="button" class="btn btn-primary">+ Keranjang</button>
                  </div>
                  <div class="block__action__buy">
                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                  </div>
                </div>
              </div>
            </div>
          </div> --}}
                <!-- end:/product item -->
            </div>
        </div>
        <!-- end:/product list -->

        {{-- <p class="color-red text-center mt-3"><em>Memuat . . .</em></p> --}}
    </div>
</section>
<!-- end:/product rekomendasi -->

<!-- start:category floating -->
<section class="category-floating">
    <div class="container">
        <div class="flex-container">
            <div class="category-floating__left">
                <div class="flex-container" id="compare-list">
                    {{-- <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-1.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS 300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare -->

                    <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-2.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS
                            300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare -->

                    <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-3.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS
                            300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare -->

                    <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-4.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS
                            300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare -->

                    <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-5.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS
                            300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare -->

                    <!-- start:product compare -->
                    <div class="compare-box">
                        <button type="button" class="btn btn-close">
                            <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h15v15H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <img src="./assets/images/products/product-category-6.jpg" alt="Product image">
                        <p class="mb-0">Asus Rog Zephyrus S GX701GWR-I7R72T - I7-9750H/32GB/1TB SSD/RTX2070 8GB/WIN
                            10/17.3 SLIM FHD IPS
                            300HZ/PER KEY RGB/NUMPAD/MOUSE/WEBCAM</p>
                    </div>
                    <!-- end:/product compare --> --}}
                </div>
            </div>
            <div class="category-floating__right">
                <button type="button" class="btn btn-primary btn-block" id="sumbit-compare">Compare</button>
            </div>
        </div>
    </div>
</section>
<!-- end:/category floating -->
@endsection
@push("js")
<script>
    // Star rating in product
    $(".star-small").starRating({
      totalStars: 5,
      strokeColor: '#FDD835',
      readOnly: true,
      starSize: 14,
      useFullStars: true,
      useGradient: false
    });

    $("input[name ='featured']").change(function(){

        var img = $(this).data('src');
        var item_id = $(this).data('item_id');
        var name = $(this).data('name');
        var label = $(this).data('label');

        if ($(this).prop('checked')){
            $('#compare-list').append(`
            <div class="compare-box" id="compare-box-`+item_id+`">
                <button type="button" class="btn btn-close delete-compare" data-item_id="`+item_id+`" data-label="`+label+`">
                    <svg width="15" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                            <path
                                d="M10.595 9.711a.625.625 0 11-.884.884l-2.21-2.21-2.21 2.21a.626.626 0 01-.884-.885l2.21-2.21-2.21-2.21a.626.626 0 01.884-.884l2.21 2.21 2.21-2.21a.626.626 0 01.884.884L8.385 7.5l2.21 2.211zm2.212-7.516a7.5 7.5 0 100 10.611 7.512 7.512 0 000-10.611z"
                                fill="#333" />
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <path fill="#fff" d="M0 0h15v15H0z" />
                            </clipPath>
                        </defs>
                    </svg>
                </button>
                <img src="`+img+`" alt="Product image">
                <p class="mb-0">`+name+`</p>
                <input name="compare-item[]" value="`+item_id+`" type="hidden">
            </div>
            `)
        } else {
            $('#compare-box-'+item_id).remove()
        }
    })

    $("#compare-list").on("click",".delete-compare",function(){
        $(this).parent().remove()
        $('#'+$(this).data('label')+$(this).data('item_id')).prop('checked', false)
    });

    $('#sumbit-compare').click(function(){
        var arr = $("input[name ='compare-item[]']").map(function () {
            return this.value; // $(this).val()
        }).get();
        if (arr != null){
            $.post( "{{url('/')}}/compare", { arr })
        }
    })
</script>
@endpush
