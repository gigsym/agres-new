@extends('layouts.auth-master')
@section('content')

<!-- start:acccounts -->
<section class="accounts accounts--login">
    <div class="accounts__content">
        <!-- start:title -->
        <div class="title">
            <img width="50px" class="m-auto" src="{{asset('assets/images/icons/icon-register-whatsapp.svg')}}"
                alt="Icon Whatsapp">
            <h3 class="mt-3">Masukan Kode Verifikasi</h3>
            <p>Kode Verifikasi telah dikirimkan melalui WhatsApp ke {{$phone}}
            </p>
        </div>
        <!-- end:/title -->

        <!-- start:content verification -->

        <div class="accounts__content__verification">
            <div id="otp" class="d-flex justify-center">
                <input class="text-center form-control inp-0" name="otp[]" type="tel" id="first" maxlength="1" />
                <input class="text-center form-control inp-1" name="otp[]" type="tel" id="second" maxlength="1" />
                <input class="text-center form-control inp-2" name="otp[]" type="tel" id="third" maxlength="1" />
                <input class="text-center form-control inp-3" name="otp[]" type="tel" id="fourth" maxlength="1" />
                <input class="text-center form-control inp-4" name="otp[]" type="tel" id="fifth" maxlength="1" />
                <input class="text-center form-control inp-5" name="otp[]" type="tel" id="sixth" maxlength="1" />
            </div>
            @if ($error = Session::get('error'))
            <p class="information text-danger">{{$error}}</p>
            @endif
            <p class="information">Mohon tunggu dalam <b>30 Detik</b> untuk <a href="javascript:;"><b>kirim
                        ulang</b></a></p>
            <form>
                <a class="btn btn-primary d-block m-auto" id="submitotp">Masuk</a>
            </form>

            <div class="link-info">
                <p class="mb-0">Kembali Ke <a href="login_email.html">Halaman Masuk</a></p>
            </div>
        </div>
        <form action="{{route('login.wa-verif')}}" method="POST" id="formotp">
            @csrf
            <input type="hidden" name="phone" value="{{$phone}}" />
            <input type="hidden" name="otp" value="" />
        </form>
        <!-- end:/content verification -->
    </div>
</section>
@endsection
@push('js')

<script>
    $('#submitotp').click(function(e){
        e.preventDefault;
        var otp = $("input[name='otp[]']").map(function(){return $(this).val();}).get();
        $("input[name='otp']").val('');
        var length = 0
        $.each(otp, function (index, value) {
            if (value == ""){
                $('.inp-'+index).focus()
            } else {
                length++
                $("input[name='otp']").val($("input[name='otp']").val()+value);
            }
        })
        if (length == 6){
            $('#formotp').submit()
        }
    })

    function OTPInput() {
        const inputs = document.querySelectorAll('#otp > *[id]');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('keydown', function (event) {
            if (event.key === "Backspace") {
                inputs[i].value = '';
                if (i !== 0)
                inputs[i - 1].focus();
            } else {
                if (i === inputs.length - 1 && inputs[i].value !== '') {
                    return true;
                } else if (event.keyCode > 47 && event.keyCode < 58) {
                    inputs[i].value = event.key;
                    if (i !== inputs.length - 1)
                        inputs[i + 1].focus();
                        event.preventDefault();
                } else if (event.keyCode > 64 && event.keyCode < 91) {
                    inputs[i].value = String.fromCharCode(event.keyCode);
                    if (i !== inputs.length - 1)
                        inputs[i + 1].focus();
                        event.preventDefault();
                }
            }
            });
        }
    }
    OTPInput();
</script>
@endpush
