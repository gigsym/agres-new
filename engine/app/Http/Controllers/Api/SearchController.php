<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models\Item;
use App\Models\MemberSearch;
use Illuminate\Http\Request;
use Auth;
use JWTAuth;
class SearchController extends Controller
{
    function search(Request $request, $search)
    {
        // $token = JWTAuth::parseToken($request->header()["authorization"][0]);
        // $user = JWTAuth::toUser($token);
        // dd($user);
        $payload = JWTAuth::getPayload($request->header()["authorization"][0]);
        dd($payload);
        $searchlow = strtolower($search);
        $searchres = Item::select("id", "name", "price", "discount_price", "start_deal", "end_deal", "category_id", "sub_category_id", "slug")->with("imagePrimary", "category", "subcategory")->where("name", "like", "%" . $searchlow . "%")->get();

        if (count($searchres) > 0) {
            if (Auth::user()) {
                MemberSearch::create([
                    "member_id" => Auth::user()->id,
                    "search_text" => $search,
                ]);
            }
            return $this->responseBase($searchres);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    function historysearch()
    {

        $search = Item::with("imagePrimary", "category", "subcategory")->where("name", "like", "%" . $search . "%")->get();

        if (count($search) > 0) {
            return $this->responseBase($search);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }
}
