
<?php $__env->startSection('content-main'); ?>

<!-- start:banner -->
<section class="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <?php $__currentLoopData = $home_banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="carousel-item w-100 <?php if($loop->iteration == 1): ?> active <?php endif; ?>">
                <img src="<?php echo e($home_banner->image_desktop); ?>" class="d-block img-fluid w-100" alt="Banner homepage">
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- end:/banner -->

<!-- start:banner promo -->
<section class="banner-promo">
    <div class="container">
        <div class="block">
            <div class="banner-promo__left">
                <?php $__currentLoopData = $promotion_banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotionbanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($promotionbanner->order == 1): ?>
                <a href="<?php echo e($promotionbanner->url); ?>" class="block__link">
                    <img class="block__image" src="<?php echo e($promotionbanner->file); ?>" alt="Banner promo">
                </a>
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="banner-promo__right">
                <div class="banner-promo__right__top">
                    <?php $__currentLoopData = $promotion_banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotionbanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($promotionbanner->order == 2): ?>
                    <a href="<?php echo e($promotionbanner->url); ?>" class="block__link">
                        <img class="block__image" src="<?php echo e($promotionbanner->file); ?>" alt="Banner promo">
                    </a>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="banner-promo__right__bottom">
                    <?php $__currentLoopData = $promotion_banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotionbanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($promotionbanner->order == 3): ?>
                    <a href="<?php echo e($promotionbanner->url); ?>" class="block__link">
                        <img class="block__image" src="<?php echo e($promotionbanner->file); ?>" alt="Banner promo">
                    </a>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end:/banner promo -->

<!-- start:hot deals product -->
<section class="product">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>HOT DEALS</h3>
            <p>Produk Populer Minggu Ini! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--horizontal">
                <!-- start:slider box -->
                <?php $__currentLoopData = $hot_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hot_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">DEALS</div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="<?php echo e(asset($hot_item->image)); ?>" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title"><?php echo e($hot_item->product->description); ?></div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="<?php echo e($hot_item->avg_rating); ?>"></div>
                                </div>
                                <div class="block__discount">
                                    <div class="percent"><?php echo e($hot_item->discount); ?></div>
                                    <div class="price"><?php echo e($hot_item->product->price); ?></div>
                                </div>
                                <div class="block__price"><?php echo e($hot_item->discount_price); ?></div>
                                <p>Availability : <span><?php echo e($hot_item->product->stock); ?> In Stock</span></p>
                                <input type="hidden" class="countdownDate" value="<?php echo e($hot_item->end_deal); ?>">

                                <p class="countdown">
                                </p>
                                <div class="block__action">
                                    <div class="block__action__favorite active">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                
                <!-- end:/slider box -->
            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end/:hot deals product -->

<!-- start:product pilihan -->
<section class="product product--featured">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <a href="<?php echo e($choosen_item_banners->url); ?>" class="block__link">
                        <img class="block__image" src="<?php echo e($choosen_item_banners->image); ?>" alt="Banner image">
                        <div class="block__action">
                            <span>BELI SEKARANG</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                <g transform="translate(-332 -694.939)">
                                    <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893" fill="none"
                                        stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="1.5" />
                                    <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                        transform="translate(332 698.945)" fill="#fff" />
                                </g>
                            </svg>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block">
                    <!-- start:title section -->
                    <div class="title-section">
                        <h3>Produk Pilihan</h3>
                        <p>Produk Pilihan Hari Ini! <a href="#">Lihat Semua</a></p>
                    </div>
                    <!-- end:/title section -->

                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--horizontal">
                            <!-- start:slider box -->

                            <?php for($i=0;$i<count($choosen_items);$i++): ?> <?php if($i%2 !=1): ?> <div class="slider__box">
                                <?php
                                $y = $i+1;
                                ?>
                                <div class="slider-wrap" style="height: 188px;">
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="<?php echo e(asset($choosen_items[$i]->image)); ?>"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags"><?php echo e($choosen_items[$i]->category_name); ?></div>
                                            <div class="block__title"><?php echo e($choosen_items[$i]->description); ?></div>
                                            <div class="block__star">
                                                <div class="star-featured"
                                                    data-rating="<?php echo e($choosen_items[$i]->avg_ranting); ?>">
                                                </div>
                                                <span>(<?php echo e($choosen_items[$i]->total_rating); ?>)</span>
                                            </div>
                                            <div class="block__price"><?php echo e($choosen_items[$i]->price); ?></div>
                                            <div class="block__action">
                                                <div class="block__action__favorite active">
                                                    <svg width="14" height="12" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M6.641 1.818L7 2.187l.358-.369c.399-.41.88-.739 1.418-.966A4.487 4.487 0 0110.479.5C12.1.501 13.5 2.034 13.5 4.193c0 2.337-1.638 5.729-6.498 7.282C2.138 9.922.5 6.528.5 4.193.5 2.033 1.9.5 3.52.5a4.488 4.488 0 011.703.352c.537.227 1.02.556 1.418.966z"
                                                            fill="#fff" stroke="#B21628" /></svg>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($y<count($choosen_items)): ?> <div class="slider-wrap" style="height: 188px;">
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="<?php echo e(asset($choosen_items[$y]->image)); ?>"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title"><?php echo e($choosen_items[$y]->description); ?></div>
                                            <div class="block__star">
                                                <div class="star-featured"
                                                    data-rating="<?php echo e($choosen_items[$y]->avg_ranting); ?>">
                                                </div>
                                                <span>(<?php echo e($choosen_items[$y]->total_rating); ?>)</span>
                                            </div>
                                            <div class="block__price"><?php echo e($choosen_items[$y]->price); ?></div>
                                            <div class="block__action">
                                                <div class="block__action__favorite active">
                                                    <svg width="14" height="12" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M6.641 1.818L7 2.187l.358-.369c.399-.41.88-.739 1.418-.966A4.487 4.487 0 0110.479.5C12.1.501 13.5 2.034 13.5 4.193c0 2.337-1.638 5.729-6.498 7.282C2.138 9.922.5 6.528.5 4.193.5 2.033 1.9.5 3.52.5a4.488 4.488 0 011.703.352c.537.227 1.02.556 1.418.966z"
                                                            fill="#fff" stroke="#B21628" /></svg>
                                                </div>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                        <?php endif; ?>
                    </div>

                    <?php endif; ?>

                    <?php endfor; ?>
                    <!-- end:/slider box -->

                    <!-- end:/slider box -->
                </div>
                <!-- end:/slider -->
            </div>
            <!-- end:/product slider -->
        </div>
    </div>
    </div>
    </div>
</section>
<!-- emd:/product pilihan -->

<!-- start:product best seller -->
<section class="product">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>Best Seller</h3>
            <p>Produk Terlaris Minggu Ini! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--vertical">
                <!-- start:slider box -->
                <?php $__currentLoopData = $best_seller_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $best_seller_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite active">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="<?php echo e(asset($best_seller_item->image)); ?>" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title"><?php echo e($best_seller_item->description); ?></div>
                                
                                <?php if($best_seller_item->discount != null): ?>
                                <div class="block__discount">
                                    <div class="percent"><?php echo e($best_seller_item->dicsount); ?></div>
                                    <div class="price"><?php echo e($best_seller_item->dicsount_price); ?></div>
                                </div>
                                <div class="block__price"><?php echo e($best_seller_item->price); ?></div>
                                <?php else: ?>
                                <div class="block__price"><?php echo e($best_seller_item->price); ?></div>
                                <?php endif; ?>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- end:/slider box -->

            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/product best seller -->

<!-- start:new product -->
<section class="product product--margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>New Product</h3>
            <p>Berbagai Produk Terbaru yang Bisa Kamu Dapatkan! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--vertical">
                <!-- start:slider box -->
                <?php $__currentLoopData = $newest_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newest_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <div class="slider__box__favorite">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="<?php echo e(asset($newest_item->image)); ?>" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title"><?php echo e($newest_item->description); ?></div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="<?php echo e($newest_item->avg_rating); ?>"></div>
                                </div>
                                <?php if($newest_item->discount != null): ?>
                                <div class="block__discount">
                                    <div class="percent"><?php echo e($newest_item->dicsount); ?></div>
                                    <div class="price"><?php echo e($newest_item->dicsount_price); ?></div>
                                </div>
                                <div class="block__price"><?php echo e($newest_item->price); ?></div>
                                <?php else: ?>
                                <div class="block__price"><?php echo e($newest_item->price); ?></div>
                                <?php endif; ?>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- end:/slider box -->

            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/new product -->

<!-- start:article -->
<section class="product product--margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>Artikel Terbaru</h3>
            <p>Berbagai Produk Terbaru yang Bisa Kamu Dapatkan! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--horizontal">
                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-1.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>Dell G15 Special Edition</h3>
                                <p>Dell G15 Special Edition, menggunakan prosesor AMD Ryzen 4000H dengan 8 core dan 16
                                    thread. Untuk grafis nya sendiri,
                                    Dell memilih GPU AMD Radeon RX 5600M yang baru diluncurkan.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-2.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>ASUS ROG Zephyrus G14</h3>
                                <p>Asus ROG Zephyrus G14, dengan menggunakan prosesor mobile AMD Ryzen 4000 yang baru,
                                    telah memberikan dampak yang besar
                                    pada ajang CES 2020.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-1.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>Dell G15 Special Edition</h3>
                                <p>Dell G15 Special Edition, menggunakan prosesor AMD Ryzen 4000H dengan 8 core dan 16
                                    thread. Untuk grafis nya sendiri,
                                    Dell memilih GPU AMD Radeon RX 5600M yang baru diluncurkan.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-2.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>ASUS ROG Zephyrus G14</h3>
                                <p>Asus ROG Zephyrus G14, dengan menggunakan prosesor mobile AMD Ryzen 4000 yang baru,
                                    telah memberikan dampak yang
                                    besar pada ajang CES 2020.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->
            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/article -->

<!-- start:info service -->
<section class="info-service">
    <div class="container">
        <!-- start:info service wrap -->
        <div class="info-service__wrap">
            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-free-delivery.svg" alt="Icon delivery">
                <div class="block">
                    <div class="title">Free Shipping</div>
                    <p class="mb-0">Untuk Order Di Atas 10jt</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-return.svg" alt="Icon return">
                <div class="block">
                    <div class="title">Free Returns</div>
                    <p class="mb-0">Jika produk yang diterima rusak</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-payment-secure.svg" alt="Icon payment secure">
                <div class="block">
                    <div class="title">100% Pay Secure</div>
                    <p class="mb-0">Pembayaranmu terlindungi</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-cs.svg" alt="Icon customer service">
                <div class="block">
                    <div class="title">Support 24/7</div>
                    <p class="mb-0">Kontak kami 24 jam sehari</p>
                </div>
            </div>
            <!-- end:/info service item -->
        </div>
        <!-- end:/info service wrap -->
    </div>
</section>
<!-- end:/info service -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\codehouse\agres-new\engine\resources\views/homepage.blade.php ENDPATH**/ ?>