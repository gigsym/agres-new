<!-- start:header -->
<header class="header">
    <!-- start:header top -->
    <div class="header__top">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">TENTANG AGRES.ID</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">KONTAK</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">MEMBER</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">KONFIRMASI PEMBAYARAN</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ARTIKEL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">PROMO</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">
                            <svg width="21" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M10.5 19l-.196-.066C2.698 16.385-.004 10.718-.004 6.639-.004 2.919 2.316 0 5.276 0h.005a7.14 7.14 0 012.843.62c.896.4 1.703.98 2.371 1.705A7.229 7.229 0 0112.867.619C13.763.22 14.73.01 15.709 0h.004c2.962 0 5.282 2.916 5.282 6.639 0 4.079-2.698 9.746-10.303 12.295L10.5 19zM5.278 1.273c-2.257 0-4.024 2.359-4.024 5.367 0 3.62 2.426 8.655 9.246 11.02 6.82-2.365 9.246-7.4 9.246-11.02 0-3.008-1.768-5.365-4.025-5.367-.81.008-1.61.184-2.352.516a5.97 5.97 0 00-1.96 1.416 1.192 1.192 0 01-.909.426 1.177 1.177 0 01-.908-.426 5.971 5.971 0 00-1.961-1.417 5.896 5.896 0 00-2.353-.515z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h21v19H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                            <span class="ml-1">WISHLIST @if(Auth::check()) ({{count(Auth::user()->wishlist)}}) @else() (0) @endif</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <svg width="21" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path
                                        d="M20.88 3.623a.667.667 0 00-.229-.197.696.696 0 00-.296-.08L7.33 2.809a.7.7 0 00-.491.167.65.65 0 00-.221.452.625.625 0 00.18.47.674.674 0 00.474.206l12.145.503-2.389 7.162H6.402L4.48 1.718a.638.638 0 00-.143-.294.674.674 0 00-.273-.192L.92.045a.698.698 0 00-.505.022.659.659 0 00-.347.353.624.624 0 00.006.485.66.66 0 00.356.345l2.79 1.057 1.96 10.225c.028.15.11.284.231.381a.69.69 0 00.432.15h.325l-.744 1.977a.519.519 0 00.068.492.579.579 0 00.458.23h.52c-.261.279-.433.625-.494.996-.061.37-.01.75.15 1.094.159.343.418.635.746.84.327.204.71.313 1.1.313.391 0 .774-.109 1.102-.314.327-.204.586-.496.745-.839.16-.343.211-.723.15-1.094a1.914 1.914 0 00-.494-.997h4.407c-.262.28-.433.626-.495.997-.06.37-.008.75.15 1.094.16.343.418.635.746.84.328.204.71.313 1.101.313s.773-.109 1.101-.314c.328-.204.587-.496.746-.839.159-.343.211-.723.15-1.094a1.913 1.913 0 00-.494-.997h.63a.572.572 0 00.396-.157.528.528 0 00.164-.381.528.528 0 00-.164-.38.572.572 0 00-.396-.159H6.75l.606-1.619h10.162a.693.693 0 00.397-.124.65.65 0 00.245-.326l2.807-8.42a.628.628 0 00-.088-.568v-.004zM7.974 17.921a.924.924 0 01-.5-.146.872.872 0 01-.33-.387.832.832 0 01-.051-.5.854.854 0 01.246-.441.931.931 0 01.979-.187.891.891 0 01.402.318.84.84 0 01-.112 1.09.918.918 0 01-.634.253zm7.411 0a.924.924 0 01-.499-.146.872.872 0 01-.33-.387.832.832 0 01-.051-.5.853.853 0 01.245-.441.931.931 0 01.979-.187.891.891 0 01.403.318.84.84 0 01-.112 1.09.917.917 0 01-.635.253z"
                                        fill="#333" />
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <path fill="#fff" d="M0 0h21v19H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                            <span class="ml-1">KERANJANG  @if(Auth::check()) ({{count(Auth::user()->cart)}}) @else() (0) @endif</span>
                        </a>
                    </li>
                    @if (Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('profile.index')}}">{{Auth::user()->name ?? Auth::user()->email}}</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login.email')}}">MASUK</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('register.email')}}">DAFTAR</a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
    <!-- end:/header top -->

    <!-- start:header main -->
    <div class="header__main">
        <div class="container">
            <nav class="header__main__box navbar navbar-expand-lg">
                <a class="navbar-brand" href="{{url('/')}}">
                    <img width="100" src="{{asset('assets/images/logo-color.svg')}}" alt="Logo Agres ID">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- start:menu -->
                    <ul class="navbar-nav mr-auto">
                        @foreach ($categories as $category)
                        <li class="nav-item">
                            <a class="nav-link" href="#">{{$category->name}}</a>
                        </li>
                        @endforeach
                        {{-- <li class="nav-item">
                            <a class="nav-link add-button" href="#">A2HS</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Bundling Office</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Laptop</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">AIO & Desktop PC</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PC / Desktop</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Printer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Software</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Aksesoris</a>
                        </li> --}}
                    </ul>
                    <!-- end:/menu -->

                    <!-- start:search -->
                    <div class="search">
                        <form method="POST">
                            <input type="text" class="form-control" placeholder="cari produk disini" autocomplete="off">
                            <button type="submit" class="btn">
                                <svg width="14" height="14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M13.915 13.09L9.848 9.023a5.553 5.553 0 10-.825.825l4.067 4.066a.295.295 0 00.413 0l.412-.412a.295.295 0 000-.412zM5.542 9.916a4.375 4.375 0 110-8.75 4.375 4.375 0 010 8.75z"
                                            fill="#333" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <path fill="#fff" d="M0 0h14v14H0z" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </button>
                        </form>
                    </div>
                    <!-- end:/search -->
                </div>
            </nav>
        </div>
    </div>
    <!-- end:/header main -->
</header>
<!-- end:/header -->
