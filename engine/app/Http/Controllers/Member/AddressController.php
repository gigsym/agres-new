<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\MemberAddress;
use App\Models\Province;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = MemberAddress::with('province', 'city', 'district','area')->where('member_id', Auth::user()->id)->get();
        return view();
    }

    public function show($address_id)
    {
        $address = MemberAddress::find($address_id);
        return view();
    }
    public function create()
    {
        $province = Province::all();
        return view();
    }
    public function store(Request $request)
    {
        $request->validate([
            // 'name' => 'required',
            // 'phone' => 'required',
        ]);

        $d['member_id'] = Auth::user()->id;
        $request->merge($d);
        MemberAddress::create($request->except('_token'));
        return view();
    }
    public function edit($address_id)
    {
        $d['address'] = MemberAddress::find($address_id);
        $d['provinces'] = Province::all();
        return view();
    }
    public function update(Request $request, $address_id)
    {
        $request->validate([
            // 'name' => 'required',
            // 'phone' => 'required',
        ]);

        $address = MemberAddress::find($address_id);
        $address->name = $request->name;
        $address->phone = $request->phone;
        $address->address = $request->address;
        $address->province_id = $request->province_id;
        $address->city_id = $request->city_id;
        $address->district_id = $request->district_id;
        $address->area_id = $request->area_id;
        $address->postal_code = $request->postal_code;
        $address->save();
    }
    public function destroy($address_id)
    {
        $address = MemberAddress::find($address_id);
        $address->delete();
        return redirect()->back()->with(['success' => " delete member address success"]);
    }
}
