<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemHot extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
    function product(){
        return $this->belongsTo('App\Models\Item' ,'variant_id');
    }
    function variants(){
        return $this->belongsTo('App\Models\ItemStockVariant' ,'variant_id');
    }
}
