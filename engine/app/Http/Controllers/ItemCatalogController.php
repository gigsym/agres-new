<?php

namespace App\Http\Controllers;

use App\Models\ItemHot;
use Illuminate\Http\Request;
use App\Http\Controllers\AdditionalController;
use App\Models\Item;
use App\Models\ItemSelect;

class ItemCatalogController extends Controller
{
    protected $AdditionalController;
    public function __construct(AdditionalController $AdditionalController)
    {
        $this->AdditionalController = $AdditionalController;
    }

    public function hotDeal()
    {
        $items = $this->ChildController->ItemFormat(ItemHot::with('product')->get());
    }
    public function selected()
    {
        $items = $this->ChildController->ItemFormat(ItemSelect::with('product')->get());
    }
    public function bestSeller()
    {
        $items = $this->ChildController->ItemFormat(Item::where('is_bestseller', 1)->get());
    }
    public function newest()
    {
        $items = $this->ChildController->ItemFormat(Item::latest()->get());
    }
}
