<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - Email</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- start:stylesheets -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
        crossorigin="anonymous" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/awesome-bootstrap-checkbox/1.0.0-alpha.2/awesome-bootstrap-checkbox.min.css">
    <link rel="stylesheet" href="{{asset("assets/vendor/bootstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/main.css")}}">
    @stack('css')
    <!-- end:/stylesheets -->
    @laravelPWA
</head>

<body>
    <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '216792006848050',
            cookie     : true,
            xfbml      : true,
            version    : 'v10.0'
          });

          FB.AppEvents.logPageView();

        };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- start:header -->
    <header class="header header--other-page">
        <img src="{{asset("assets/images/logo-white.svg")}}" alt="Logo Agres ID">
    </header>
    <!-- end:/header -->

    @yield('content')

    <!-- start:footer -->
    <footer class="footer footer--other-page">
        <!-- start:footer copyright -->
        <div class="footer__copyright">
            <p>Copyright © 2021 AGRES. All Rights Reserved</p>
        </div>
        <!-- end:/footer copyright -->
    </footer>
    <!-- end:/footer -->

    <!-- start:javascripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="{{asset("assets/vendor/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("assets/js/style.js")}}"></script>
    <!-- end:/javascripts -->


    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
   https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js"></script>

    <script>
        // Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
      apiKey: "AIzaSyBKy9xFR6x-L_ufxg9nRsev1VtVyO3fu9U",
      authDomain: "web-app-agres.firebaseapp.com",
      projectId: "web-app-agres",
      storageBucket: "web-app-agres.appspot.com",
      messagingSenderId: "594855414554",
      appId: "1:594855414554:web:8e904d2d89d94268e83aa3",
      measurementId: "G-C9VPBELP6C"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
    </script>
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-firestore.js"></script>

    <script>
        $("#google_signin").click(function(){

            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().languageCode = 'en';
            firebase.auth()
            .signInWithPopup(provider)
            .then((result) => {
                /** @type {firebase.auth.OAuthCredential} */
                var credential = result.credential;
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = credential.accessToken;
                // The signed-in user info.
                var email = result.user.email;
                var name = result.user.displayName;
                var phone = result.user.phoneNumber;
                var image = result.user.photoURL;
                $.ajax({
                        url: "{{url('/')}}/member/login/sosmed",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'post',
                        data: {token:token, email:email, name:name, phone:phone, image:image},
                        success: function (data){
                            if (data == true){
                                window.location.replace("{{url('/')}}")
                            } else {
                                window.location.refresh()
                            }
                        }
                    })
            }).catch((error) => {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/account-exists-with-different-credential'){
                    var email = error.email
                    var token = error.credential.accessToken
                    $.ajax({
                            url: "{{url('/')}}/member/login/sosmed",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'post',
                            data: {token:token, email:email},
                            success: function (data){
                                if (data == true){
                                    window.location.replace("{{url('/')}}")
                                } else {
                                    window.location.refresh()
                                }
                            }
                        })
                }
            });

        });

        $("#facebook_signin").click(function(){
            var provider = new firebase.auth.FacebookAuthProvider();
            firebase.auth().useDeviceLanguage();
            // provider.setCustomParameters({
            //     'display': 'popup'
            // });
            firebase
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
                /** @type {firebase.auth.OAuthCredential} */
                console.dir(result);

                var credential = result.credential;
                // The signed-in user info.
                var email = result.user.email;
                // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                var accessToken = credential.accessToken;
                var name = result.user.displayName;
                var phone = result.user.phoneNumber;
                var image = result.user.photoURL;
                $.ajax({
                        url: "{{url('/')}}/member/login/sosmed",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'post',
                        data: {token:accessToken, email:email, name:name, phone:phone, image:image},
                        success: function (data){
                            if (data == true){
                                window.location.replace("{{url('/')}}")
                            } else {
                                window.location.refresh()
                            }
                        }
                    })
            })
            .catch((error) => {
                console.log(error)

                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/account-exists-with-different-credential'){
                    var email = error.email
                    var token = error.credential.accessToken
                    $.ajax({
                            url: "{{url('/')}}/member/login/sosmed",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'post',
                            data: {token:token, email:email},
                            success: function (data){
                                if (data == true){
                                    window.location.replace("{{url('/')}}")
                                } else {
                                    window.location.refresh()
                                }
                            }
                        })
                }
            });
        })

    </script>

    @stack('js')
</body>

</html>
