<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Mail\SendOtp;
use App\Models\Member;
use App\Models\MemberPasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class KeamananController extends Controller
{
    public function index()
    {
        $d['categories'] = $this->itemCategory();
        $d['profile'] = Member::where('id', Auth::user()->id)->first();
        return view('member.keamanan', $d);
    }

    public function sendOtp(Request $request)
    {
        $member = Member::find(Auth::user()->id);

        $otp =  mt_rand(100000, 999999);
        $passres = MemberPasswordReset::where("member_id", $member->id)->first();
        if ($passres) {
            $passres->token = strtoupper($otp);
            $passres->type = $request->type;
            $passres->save();
        } else {
            $passres = MemberPasswordReset::create([
                "member_id" => $member->id,
                "token" => strtoupper($otp),
                "type" => 'verification',
                "updated_at" => Carbon::now()
            ]);
        }

        if ($request->type == 'email') {
            if ($request->email != null) {
                $email = $request->email;
                $member->email_verified_at = null;
                $member->email = $request->email;
            } else {
                $email = $member->email;
            }
            $details = [
                'otp' => $otp,
            ];
            // Mail::to($email)->send(new SendOtp($details));
        } else if ($request->type == 'phone') {
            if ($request->phone != null) {
                $phone = $request->phone;
                $member->phone = $request->phone;
                $member->phone_verified_at = null;
            } else {
                $phone = $member->phone;
            }

            $string = "otp anda: *" . strtoupper($otp) . "* expired dalam 15 menit.";
            $data["contact"] = $phone;
            $data["msg"] = $string;
            // $this->kirimOtp($data);
        }

        $member->save();
        return response()->json(['message' => "success"]);
    }

    public function verifyOtp(Request $request)
    {
        $member = Member::find(Auth::user()->id);
        $checkotp = MemberPasswordReset::where("member_id", Auth::user()->id)->where("type", $request->type)->where("token", $request->otp)->first();
        if ($checkotp != null) {
            $now = Carbon::now();
            $minutes = $now->diffInMinutes($checkotp->updated_at);
            if ($minutes <= 15) {
                if ($request->type == 'email') {
                    $member->email_verified_at = Carbon::now();
                } else if ($request->type == 'phone') {
                    $member->phone_verified_at = Carbon::now();
                }
                $member->save();
                return response()->json(['message' => "success"]);
            } else {
                return response()->json(['message' => "otp expired"]);
            }
        } else {
            return response()->json(['message' => 'otp salah']);
        }
    }
}
