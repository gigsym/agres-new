<?php $__env->startSection('content'); ?>
<!-- start:acccounts -->
<section class="accounts accounts--login">
    <div class="accounts__content">
        <!-- start:title -->
        <div class="title">
            <h3>Masuk ke Akunmu</h3>
            <p>Belum Punya Akun Agres.id? <a href="<?php echo e(route('register.email')); ?>">Klik Disini!</a></p>
        </div>
        <!-- end:/title -->

        <!-- start:form -->
        <form action="<?php echo e(route('login.email')); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <input type="email" name="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" required>
                <label class="placeholder">Email</label>
                <span class="icon icon--close"></span>
                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <p class="invalid-info"><?php echo e($message); ?></p>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
            <div class="form-group">
                <input id="password-field" name="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" required>
                <label class="placeholder">Password</label>
                <span toggle="#password-field" class="icon icon--eye close toggle-password"></span>
                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <p class="invalid-info"><?php echo e($message); ?></p>
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            </div>
            <div class="form-group d-flex">
                <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="membership" id="membership" type="checkbox">
                    <label class="form-check-label" for="membership">
                        Ingat saya
                    </label>
                </div>
                <a href="javascript:;" class="forgot-password">Lupa Kata Sandi?</a>
            </div>
            <button type="submit" class="btn btn-primary d-block m-auto">Masuk</button>
        </form>
        <!-- end:/form -->

        <!-- start:content bottom -->
        <div class="accounts__content__bottom">
            <p class="italic">Atau masuk menggunakan</p>
            <ul class="list-unstyled">
                <li>
                    <a href="javascript:;" id="google_signin">
                        <img src="<?php echo e(asset("assets/images/icons/icon-register-google.svg")); ?>" alt="Icon google">
                        <span>Google</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" id="facebook_signin">
                        <img src="<?php echo e(asset("assets/images/icons/icon-register-facebook.svg")); ?>" alt="Icon facebook">
                        <span>Facebook</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo e(url('/')); ?>/member/login/wa">
                        <img src="<?php echo e(asset("assets/images/icons/icon-register-whatsapp.svg")); ?>" alt="Icon whatsapp">
                        <span>Whatsapp</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end:/content bottom -->
    </div>
</section>
<!-- end:/acccounts -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
<script>
    $(".toggle-password").click(function () {
        $(this).toggleClass("open");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\agres-new\engine\resources\views/auth/login.blade.php ENDPATH**/ ?>