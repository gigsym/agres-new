<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('sendotplogin', 'Api\LoginController@sendotp');
// Route::post('verifyotplogin', 'Api\LoginController@verifyotp');

Route::middleware(['auth', 'second'])->group(function () {
});
Route::get('member/verification', 'LoginController@verification')->name('register.verification');

Route::post('login/email', 'Api\LoginController@login');
Route::post('register/email', 'Api\LoginController@register');
Route::post('login/wa', 'Api\LoginController@loginWaOtp');
Route::post('login/wa-verif', 'Api\LoginController@loginWaOtpVerif');
Route::post('login/sosmed', 'Api\LoginController@loginSosmed');

Route::post('logout', 'Api\LoginController@logout');

Route::get('cms/homepage/banner', 'Api\Cms\CmsController@homeBanner');

Route::get('cms/homepage/sliderbanner', 'Api\Cms\CmsController@homeSliderBanner');
Route::get('cms/homepage/topmenu', 'Api\Cms\CmsController@homeTopMenu');
Route::get('cms/homepage/bottommenu', 'Api\Cms\CmsController@homeBottomMenu');

Route::post('item/filter', 'Api\ItemController@filtersortItem');
Route::post('item/search', 'Api\ItemController@searchAll');

Route::get('item/get/hotdeal', 'Api\ItemController@getHotdealProduct');
Route::get('item/get/choosen', 'Api\ItemController@getChoosenProduct');
Route::get('item/get/bestseller', 'Api\ItemController@getBestSellserProduct');
Route::get('item/get/newest', 'Api\ItemController@getNewProduct');
Route::get('item/get/{id}/detail', 'Api\ItemController@getItemDetail');
Route::get('item/get/{id}/review', 'Api\ItemController@getItemReview');
Route::get('item/get/category', 'Api\ItemController@getItemCategory');
Route::get('item/get/filter', 'Api\ItemController@getFilter');
Route::get('item/get/sort', 'Api\ItemController@getSort');

Route::post('item/get/search/all', 'Api\ItemController@getItemBySearch');
Route::get('item/get/{category_id}/category', 'Api\ItemController@getItemByCategory');
Route::get('item/get/{sub_category_id}/subcategory', 'Api\ItemController@getItemBySubCategory');

Route::get('item/get/{id}/itemreview', 'Api\ItemController@getItemReview');

Route::get('getprovince', 'Controller@getProvince');
Route::get('getcity/{province_id}', 'Controller@getCity');
Route::get('getdistrict/{city_id}', 'Controller@getDistrict');
Route::get('getarea/{district_id}', 'Controller@getArea');



Route::group(['middleware' => ['auth:api']], function () {

    Route::get('member/get/profile', 'Api\MemberController@profile');
    Route::post('member/update/profile', 'Api\MemberController@updateProfile');
    Route::get('member/get/wishlist', 'Api\MemberController@getWishlist');
    Route::post('member/add/wishlist', 'Api\MemberController@createWishlist');
    Route::post('member/delete/{id}/wishlist', 'Api\MemberController@deleteWishlist');

    Route::get('member/get/cart', 'Api\CartController@getCart');
    Route::post('member/add/cart', 'Api\CartController@createCart');
    Route::post('member/delete/{id}/cart', 'Api\CartController@deleteCart');
    Route::post('member/updateqty/{id}/cart', 'Api\CartController@updateQuantity');
    Route::get('member/getsubtotal/cart', 'Api\CartController@getSubTotal');

    Route::get('member/{member_id}/address', 'Api\AddressController@index');
    Route::post('member/{member_id}/address', 'Api\AddressController@store');
    Route::get('member/{member_id}/address/{address_id}/detail', 'Api\AddressController@show');
    Route::post('member/{member_id}/address/{address_id}/update', 'Api\AddressController@update');
    Route::post('member/{member_id}/address/{address_id}/delete', 'Api\AddressController@destroy');
    Route::post('member/{member_id}/address/{address_id}/updatestatus', 'Api\AddressController@updateStatus');
    Route::post('member/{member_id}/address/{address_id}/setprimary', 'Api\AddressController@setPrimaryAddress');
});

Route::post('cart/getsubtotal', 'Member\CartController@getSubTotal');
/**
 * search API
 */
/////////////////////////////////////////////////////////////
Route::get("search/{query}", 'Api\SearchController@search');
/////////////////////////////////////////////////////////////
