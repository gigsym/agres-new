<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberAddress;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $d['categories'] = $this->itemCategory();
        $d['profile'] = Member::where('id', Auth::user()->id)->first();
        $d['alamat_pengiriman'] = MemberAddress::with('province','city','district', 'area')->where('member_id', Auth::user()->id)->where('status', 1)->first();
        $d['alamat_penagihan'] = MemberAddress::with('province','city','district', 'area')->where('member_id', Auth::user()->id)->where('status', 2)->first();
        return view('member.profile', $d);
    }

    public function update(Request $request, $member_id)
    {
        $member = Member::find($member_id);
        foreach ($request->except('_token','_method') as $k => $v) {
            if ($k == 'image') {
                $imageName = $v->getClientOriginalName();
                $imageNewName =  uniqid() . '_' . $imageName;
                $destinationPath = 'assets/member/profile/';
                $v->move($destinationPath, $imageNewName);
                $path_file =  $destinationPath . $imageNewName;

                $member->$k = $path_file;
            } else if($k == 'birthday'){
                $member->$k = Carbon::parse($v);
            } else {
                $member->$k = $v;
            }
        }
        $member->save();
        return back()->with('success', 'berhasil update profile picture');
    }
}
