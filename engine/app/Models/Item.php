<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    function image()
    {
        return $this->hasMany('App\Models\ItemImage');
    }
    function imageOne()
    {
        return $this->hasOne('App\Models\ItemImage');
    }
    function imagePrimary()
    {
        $primary = $this->hasOne('App\Models\ItemImage')->where('is_primary', 1);
        if ($primary != null) {
            return $primary;
        } else {
            return $this->hasOne('App\Models\ItemImage');
        }
    }

    function review()
    {
        return $this->hasMany('App\Models\ItemReview');
    }

    function longDescription()
    {
        return $this->hasOne('App\Models\ItemDescription');
    }

    function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    function subCategory()
    {
        return $this->belongsTo('App\Models\SubCategory');
    }

    function variants()
    {
        return $this->hasMany('App\Models\ItemStockVariant');
    }

    function isNew()
    {
        $diff = Carbon::createFromTimeStamp(strtotime($this->created_at))->diffInDays();
        if ($diff < 7) {
            return true;
        } else {
            return false;
        }
    }
}
