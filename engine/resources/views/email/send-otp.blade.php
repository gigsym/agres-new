<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Agres.id Notification Email</title>
</head>
<body style="background:#f3e7ed; color:#000; font-family:Georgia, 'Times New Roman', Times, serif; font-size:12px"
alink="#FF0000" link="#FF0000" bgcolor="#f3e7ed" text="#000" yahoo="fix">
<div id="body_style" style="padding:15px;">

    <table cellpadding="0" cellspacing="10" border="0" bgcolor="#fff" width="800" align="center" style="line-height:18px; padding:0; border:5px solid #7d003e; background:#fff;">
        <tr>
            <td style="border-bottom:1px solid #b21628;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <h1 style="margin:20px 0; color:#000; font-size:18px;">Hi!<h1>

                <p style="margin:10px 0;font-size:12px;">
                <br /><br />
                Your OTP is:<br />
                {{$details['otp']}}
                <br /><br /><br />

                Please feel free to ask us any questions, or  tell us anything about the registration process, or make any other constructive comments by sending an e-mail to webmaster@agres.id
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 0 20px;">
                <p style="font-size:12px;">Best regards,</p>
                <br /><br />
                <span style="font-size:14px;">Agres.id</span>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin:10px 0; font-size:10px; color:#999; border-top:1px solid #b21628; padding:10px 0;">
                    This email is intended for <br />
                    Agres.id copyright &copy; 2021. All Rights Reserved
                </p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
