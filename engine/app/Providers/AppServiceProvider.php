<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\CmsHomepageBanner;
use App\Models\CmsMenuBottom;
use App\Models\CmsMenuTop;
use App\Models\CmsPromoBanner;
use App\Models\Item;
use App\Models\ItemHot;
use App\Models\ItemImage;
use App\Models\ItemReview;
use App\Models\ItemSelect;
use App\Models\ItemSelectBanner;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $d['categories'] = $this->itemCategory();
        $d['home_banners'] = CmsHomepageBanner::all();
        $d['promotion_banners'] = CmsPromoBanner::all();
        $d['top_menus'] = CmsMenuTop::all();
        $d['bottom_menus'] = CmsMenuBottom::all();

        $d['menu_bottoms'] = CmsMenuBottom::all();
        foreach ($d as $key => $value) {
            View::share($key, $value);
        }
    }

    public function itemCategory()
    {
        $categories = Category::with('subcat')->get();
        foreach ($categories as $category) {
            $category->spesification = json_decode($category->spesification);
        }

        return $categories;
    }


}
