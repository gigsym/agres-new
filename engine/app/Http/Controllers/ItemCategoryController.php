<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CmsCatalogBanner;
use App\Models\CmsCatalogBrand;
use App\Models\CmsCatalogPromoBanner;
use App\Models\CmsCatalogSpecialDealBanner;
use App\Models\CmsPromoBanner;
use App\Models\Item;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ItemCategoryController extends Controller
{
    protected $AdditionalController;
    public function __construct(AdditionalController $AdditionalController)
    {
        $this->AdditionalController = $AdditionalController;
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if ($category != null) {
            $item = Item::where("category_id", $category->id);
            $subcat = SubCategory::with('recItem')->where('category_id', $category->id)->get();
            // foreach ($subcat as $k => $v) {
            //     $v->recItem = $this->AdditionalController->ItemFormat($v->recItem);
            // }
            $d["category"] = $category;
            $d["sub_categories"] = $subcat;
            $d["banners"] = CmsCatalogBanner::where("category_id", $category->id)->get();
            $d["data"] = $item->get();
            $d["bestsellers"] = $item->with('subCategory')->orderby('sold_counter', 'desc')->get()->take(12);
            $d['promos'] = CmsCatalogPromoBanner::get()->take(4);
            $d['spesial_deals'] = CmsCatalogSpecialDealBanner::get()->take(4);
            $d['brands'] = CmsCatalogBrand::get();
            return view("category", $d);
        } else {
            return route('home');
        }
    }
}
