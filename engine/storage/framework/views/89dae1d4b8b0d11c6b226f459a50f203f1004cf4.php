<div class="container-footer">
    <div class="container">
        <footer>
            <div class="row pt-2 pb-2">
                <div class="col-md-4">
                    <img class="logo-footer" src="<?php echo e(asset('assets/image/logo/agres-id-logo-white.png')); ?>" alt="">
                    <h6 class="pt-4">PT.AGRES INFO TEKNOLOGI (AGRES.ID)</h6>
                    <span>Ruko Mangga Dua Square, Jl. Gn. Sahari No.1, Jakarta 14420</span>
                    <span>Telpon : (021) 22681040</span>
                    <span>Mobile : +62 812-9700-9700</span>
                    <span>Email : info@agres.co.id</span>
                </div>
                <div class="col-md-2">
                    <h5>Kategori</h5>
                    <span>Laptop</span>
                    <span>PC / Desktop</span>
                    <span>Tablet</span>
                </div>
                <div class="col-md-2">
                    <h5>Sitemap</h5>
                    <span>Tentang Agres ID</span>
                    <span>Kontak</span>
                    <span>Member</span>
                </div>
                <div class="col-md-4">
                    <h5>Sign Up Newsletter</h5>
                    <span>Daftar & dapatkan update terbaru serta penawaran spesial</span>
                    <form action="" class="pt-3">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Recipient's username"
                                aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">Button</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 pt-4 pb-1">
                    <span>Agres.id menjual berbagai macam kategori produk laptop, gaget dan komputer. mulai dari
                        Ultrabook, PC tablet, Printer, Projector, Scanner, Software, hingga berbagai macam peripheral
                        dan aksesoris baik untuk laptop maupun PC desktop. Anda dapat melihat lokasi dimana toko-toko
                        cabang Agres.id</span>
                </div>
            </div>
        </footer>
    </div>
</div>
<div class="copyright">
    <span>Copyright 2021 AGRES, All Rights Reserved</span>
</div>
<?php /**PATH C:\xampp\htdocs\agres-server\engine\resources\views/layouts/footer.blade.php ENDPATH**/ ?>