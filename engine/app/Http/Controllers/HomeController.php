<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CmsHomepageBanner;
use App\Models\CmsMenuBottom;
use App\Models\CmsMenuTop;
use App\Models\CmsPromoBanner;
use App\Models\CmsSlideBanner;
use App\Models\Item;
use App\Models\ItemHot;
use App\Models\ItemImage;
use App\Models\ItemReview;
use App\Models\ItemSelect;
use App\Models\ItemSelectBanner;
use App\Models\SubCategory;
use Carbon\Carbon;
use DateTime;
use App\Http\Controllers\AdditionalController;
use App\Models\ItemBest;
use App\Models\ItemNewest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $AdditionalCont;
    public function __construct(AdditionalController $AdditionalCont)
    {
        $this->AdditionalCont = $AdditionalCont;
    }

    public function index()
    {

        $d['categories'] = $this->itemCategory();
        $d['home_banners'] = CmsHomepageBanner::all();
        $d['promotion_banners'] = CmsPromoBanner::all();
        $d['top_menus'] = CmsMenuTop::all();
        $d['bottom_menus'] = CmsMenuBottom::all();
        $d['hot_items'] = $this->AdditionalCont->ItemFormat(ItemHot::with('variants')->take(12)->get());
        $d['choosen_items'] =  $this->AdditionalCont->ItemFormat(ItemSelect::with('variants')->take(12)->get());
        $d['choosen_item_banners'] = ItemSelectBanner::first();
        $d['best_seller_items'] =  $this->AdditionalCont->ItemFormat(ItemBest::with('variants')->take(12)->get());
        $d['newest_items'] =  $this->AdditionalCont->ItemFormat(ItemNewest::with('variants')->take(12)->get());

        $d['menu_bottoms'] = CmsMenuBottom::all();

        return view('homepage', $d);
    }
}
