@extends('layouts.master')
@section('content-main')

<!-- start:banner -->
<section class="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            @foreach ($home_banners as $home_banner)
            <div class="carousel-item w-100 @if ($loop->iteration == 1) active @endif">
                <img src="{{$home_banner->image_desktop}}" class="d-block img-fluid w-100" alt="Banner homepage">
            </div>
            @endforeach
            {{-- <div class="carousel-item active">
                <img src="./assets/images/banner-slider-homepage.jpg" class="d-block w-100" alt="Banner homepage">
            </div>
            <div class="carousel-item">
                <img src="./assets/images/banner-slider-homepage.jpg" class="d-block w-100" alt="Banner homepage">
            </div>
            <div class="carousel-item">
                <img src="./assets/images/banner-slider-homepage.jpg" class="d-block w-100" alt="Banner homepage">
            </div> --}}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- end:/banner -->

<!-- start:banner promo -->
<section class="banner-promo">
    <div class="container">
        <div class="block">
            <div class="banner-promo__left">
                @foreach ($promotion_banners as $promotionbanner)
                @if ($promotionbanner->order == 1)
                <a href="{{$promotionbanner->url}}" class="block__link">
                    <img class="block__image" src="{{$promotionbanner->file}}" alt="Banner promo">
                </a>
                @endif
                @endforeach
            </div>
            <div class="banner-promo__right">
                <div class="banner-promo__right__top">
                    @foreach ($promotion_banners as $promotionbanner)
                    @if ($promotionbanner->order == 2)
                    <a href="{{$promotionbanner->url}}" class="block__link">
                        <img class="block__image" src="{{$promotionbanner->file}}" alt="Banner promo">
                    </a>
                    @endif
                    @endforeach
                </div>
                <div class="banner-promo__right__bottom">
                    @foreach ($promotion_banners as $promotionbanner)
                    @if ($promotionbanner->order == 3)
                    <a href="{{$promotionbanner->url}}" class="block__link">
                        <img class="block__image" src="{{$promotionbanner->file}}" alt="Banner promo">
                    </a>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end:/banner promo -->

<!-- start:hot deals product -->
<section class="product">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>HOT DEALS</h3>
            <p>Produk Populer Minggu Ini! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--horizontal">
                <!-- start:slider box -->
                @foreach ($hot_items as $hot_item)
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">DEALS</div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="{{asset($hot_item->imagePrimary)}}" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">{{$hot_item->variants->product->subCategory->name ?? ''}}</div>
                                <div class="block__title">{{$hot_item->variants->product->name}}</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="{{$hot_item->avg_rating}}"></div>
                                </div>
                                @if ($hot_item->discount != null)
                                <div class="block__discount">
                                    <div class="percent">{{$hot_item->discount}}</div>
                                    <div class="price">{{$hot_item->variants->price}}</div>
                                </div>
                                <div class="block__price">{{$hot_item->discount_price}}</div>
                                @else
                                <div class="block__price">{{$hot_item->variants->price}}</div>
                                @endif
                                <p>Availability : <span>{{$hot_item->variants->stock}} In Stock</span></p>
                                @if ($hot_item->discount != null)
                                <input type="hidden" class="countdownDate" value="{{$hot_item->variants->end_deal}}">
                                @endif

                                <p class="countdown">
                                </p>
                                <div class="block__action">
                                    <a href="" class="addToWishlist" data-item_id="{{$hot_item->variant_id}}">
                                        <div
                                            class="block__action__favorite @if (Auth::check() && in_array($hot_item->variant_id, Auth::user()->wishlist->pluck('variant_id')->toArray())) active @endif">
                                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                    fill="white" stroke="#B21628" />
                                            </svg>
                                        </div>
                                    </a>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- end:/slider box -->

                <!-- start:slider box -->
                {{-- <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">DEALS</div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-2.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="4"></div>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">Rp. 60.999.000</div>
                                </div>
                                <div class="block__price">Rp. 48.799.200</div>
                                <p>Availability : <span>10 In Stock</span></p>
                                <p><span>01</span> Hari : <span>01</span> Jam : <span>54</span> Menit : <span>32</span>
                                    Detik</p>
                                <div class="block__action">
                                    <div class="block__action__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">DEALS</div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-1.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="4"></div>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">Rp. 60.999.000</div>
                                </div>
                                <div class="block__price">Rp. 48.799.200</div>
                                <p>Availability : <span>10 In Stock</span></p>
                                <p><span>01</span> Hari : <span>01</span> Jam : <span>54</span> Menit : <span>32</span>
                                    Detik</p>
                                <div class="block__action">
                                    <div class="block__action__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">DEALS</div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="./assets/images/products/product-2.jpg" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">Laptop Gaming</div>
                                <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                    SSD/RTX2070...</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="4"></div>
                                </div>
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">Rp. 60.999.000</div>
                                </div>
                                <div class="block__price">Rp. 48.799.200</div>
                                <p>Availability : <span>10 In Stock</span></p>
                                <p><span>01</span> Hari : <span>01</span> Jam : <span>54</span> Menit : <span>32</span>
                                    Detik</p>
                                <div class="block__action">
                                    <div class="block__action__favorite">
                                        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                                fill="white" stroke="#B21628" />
                                        </svg>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!-- end:/slider box -->
            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end/:hot deals product -->

<!-- start:product pilihan -->
<section class="product product--featured">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <a href="{{$choosen_item_banners->url}}" class="block__link">
                        <img class="block__image" src="{{$choosen_item_banners->image}}" alt="Banner image">
                        <div class="block__action">
                            <span>BELI SEKARANG</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                <g transform="translate(-332 -694.939)">
                                    <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893" fill="none"
                                        stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-width="1.5" />
                                    <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                        transform="translate(332 698.945)" fill="#fff" />
                                </g>
                            </svg>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block">
                    <!-- start:title section -->
                    <div class="title-section">
                        <h3>Produk Pilihan</h3>
                        <p>Produk Pilihan Hari Ini! <a href="#">Lihat Semua</a></p>
                    </div>
                    <!-- end:/title section -->

                    <!-- start:product slider -->
                    <div class="product__slider">
                        <!-- start:slider -->
                        <div class="slider slider--horizontal">
                            <!-- start:slider box -->

                            @for($i=0;$i<count($choosen_items);$i++) @if($i%2 !=1) <div class="slider__box">
                                @php
                                $y = $i+1;
                                @endphp
                                <div class="slider-wrap" style="height: 188px;">
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="{{asset($choosen_items[$i]->imagePrimary)}}"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">
                                                {{$choosen_items[$i]->variants->product->subCategory->name}}</div>
                                            <div class="block__title">{{$choosen_items[$i]->variants->product->name}}
                                            </div>
                                            <div class="block__star">
                                                <div class="star-featured"
                                                    data-rating="{{$choosen_items[$i]->avg_ranting}}">
                                                </div>
                                                <span>({{$choosen_items[$i]->total_rating}})</span>
                                            </div>
                                            @if ($choosen_items[$i]->discount != null)
                                            <div class="block__price">{{$choosen_items[$i]->discount_price}}</div>
                                            @else
                                            <div class="block__price">{{$choosen_items[$i]->variants->price}}</div>
                                            @endif
                                            <div class="block__action">
                                                <a href="" class="addToWishlist"
                                                    data-item_id="{{$choosen_items[$i]->variant_id}}">
                                                    <div
                                                        class="block__action__favorite @if (Auth::check() && in_array($choosen_items[$i]->variant_id, Auth::user()->wishlist->pluck('variant_id')->toArray())) active @endif">
                                                        <svg width="14" height="12" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M6.641 1.818L7 2.187l.358-.369c.399-.41.88-.739 1.418-.966A4.487 4.487 0 0110.479.5C12.1.501 13.5 2.034 13.5 4.193c0 2.337-1.638 5.729-6.498 7.282C2.138 9.922.5 6.528.5 4.193.5 2.033 1.9.5 3.52.5a4.488 4.488 0 011.703.352c.537.227 1.02.556 1.418.966z"
                                                                fill="#fff" stroke="#B21628" /></svg>
                                                    </div>
                                                </a>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($y<count($choosen_items)) <div class="slider-wrap" style="height: 188px;">
                                    <div class="slider__box__image">
                                        <img class="image-slider" src="{{asset($choosen_items[$y]->imagePrimary)}}"
                                            alt="image slider">
                                    </div>
                                    <div class="slider__box__content">
                                        <div class="block">
                                            <div class="block__tags">Laptop Gaming</div>
                                            <div class="block__title">
                                                {{$choosen_items[$y]->variants->product->description}}</div>
                                            <div class="block__star">
                                                <div class="star-featured"
                                                    data-rating="{{$choosen_items[$y]->avg_ranting}}">
                                                </div>
                                                <span>({{$choosen_items[$y]->total_rating}})</span>
                                            </div>
                                            @if ($choosen_items[$i]->discount != null)
                                            <div class="block__price">{{$choosen_items[$i]->discount_price}}</div>
                                            @else
                                            <div class="block__price">{{$choosen_items[$i]->variants->price}}</div>
                                            @endif
                                            <div class="block__action">
                                                <a href="" class="addToWishlist"
                                                    data-item_id="{{$choosen_items[$i]->variant_id}}">
                                                    <div
                                                        class="block__action__favorite @if (Auth::check() && in_array($choosen_items[$i]->variant_id, Auth::user()->wishlist->pluck('variant_id')->toArray())) active @endif">
                                                        <svg width="14" height="12" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M6.641 1.818L7 2.187l.358-.369c.399-.41.88-.739 1.418-.966A4.487 4.487 0 0110.479.5C12.1.501 13.5 2.034 13.5 4.193c0 2.337-1.638 5.729-6.498 7.282C2.138 9.922.5 6.528.5 4.193.5 2.033 1.9.5 3.52.5a4.488 4.488 0 011.703.352c.537.227 1.02.556 1.418.966z"
                                                                fill="#fff" stroke="#B21628" /></svg>
                                                    </div>
                                                </a>
                                                <div class="block__action__buy">
                                                    <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                        @endif
                    </div>

                    @endif

                    @endfor
                    <!-- end:/slider box -->

                    <!-- end:/slider box -->
                </div>
                <!-- end:/slider -->
            </div>
            <!-- end:/product slider -->
        </div>
    </div>
    </div>
    </div>
</section>
<!-- emd:/product pilihan -->

<!-- start:product best seller -->
<section class="product">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>Best Seller</h3>
            <p>Produk Terlaris Minggu Ini! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--vertical">
                <!-- start:slider box -->
                @foreach ($best_seller_items as $best_seller_item)
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <a href="" class="addToWishlist" data-item_id="{{$best_seller_item->variant_id}}">
                            <div
                                class="slider__box__favorite @if (Auth::check() && in_array($best_seller_item->variant_id, Auth::user()->wishlist->pluck('variant_id')->toArray())) active @endif">
                                <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                        fill="white" stroke="#B21628" />
                                </svg>
                            </div>
                        </a>
                        <div class="slider__box__image">
                            <img class="image-slider" src="{{asset($best_seller_item->imagePrimary)}}"
                                alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">{{$best_seller_item->variants->product->subCategory->name}}
                                </div>
                                <div class="block__title">{{$best_seller_item->variants->product->name}}</div>
                                {{-- <div class="block__star">
                                    <div class="star-product" data-rating="4"></div>
                                </div> --}}
                                @if ($best_seller_item->discount != null)
                                <div class="block__discount">
                                    <div class="percent">{{$best_seller_item->discount}}</div>
                                    <div class="price">{{$best_seller_item->variants->price}}</div>
                                </div>
                                <div class="block__price">{{$best_seller_item->discount_price}}</div>
                                @else
                                <div class="block__price">{{$best_seller_item->variants->price}}</div>
                                @endif
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary addToCart"
                                            data-item_id="{{$best_seller_item->variant_id}}"
                                            data-price="{{$best_seller_item->discount_price}}">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- end:/slider box -->

            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/product best seller -->

<!-- start:new product -->
<section class="product product--margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>New Product</h3>
            <p>Berbagai Produk Terbaru yang Bisa Kamu Dapatkan! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--vertical">
                <!-- start:slider box -->
                @foreach ($newest_items as $newest_item)
                <div class="slider__box">
                    <div class="slider-wrap">
                        <div class="slider__box__label">BEST</div>
                        <a href="" class="addToWishlist" data-item_id="{{$newest_item->variant_id}}">
                            <div
                                class="slider__box__favorite @if (Auth::check() && in_array($newest_item->variant_id, Auth::user()->wishlist->pluck('variant_ids')->toArray())) active @endif">
                                <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                        fill="white" stroke="#B21628" />
                                </svg>
                            </div>
                        </a>
                        <div class="slider__box__image">
                            <img class="image-slider" src="{{asset($newest_item->imagePrimary)}}" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">{{$newest_item->variants->product->subCategory->name}}</div>
                                <div class="block__title">{{$newest_item->variants->product->name}}</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="{{$newest_item->avg_rating}}"></div>
                                </div>
                                @if ($newest_item->discount != null)
                                <div class="block__discount">
                                    <div class="percent">{{$newest_item->discount}}</div>
                                    <div class="price">{{$newest_item->variants->price}}</div>
                                </div>
                                <div class="block__price">{{$newest_item->discount_price}}</div>
                                @else
                                <div class="block__price">{{$newest_item->variants->price}}</div>
                                @endif
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary addToCart"
                                            data-item_id="{{$newest_item->variant_id}}"
                                            data-price="{{$newest_item->discount_price}}">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- end:/slider box -->

            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/new product -->

<!-- start:article -->
<section class="product product--margin-less">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>Artikel Terbaru</h3>
            <p>Berbagai Produk Terbaru yang Bisa Kamu Dapatkan! <a href="#">Lihat Semua</a></p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--horizontal">
                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-1.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>Dell G15 Special Edition</h3>
                                <p>Dell G15 Special Edition, menggunakan prosesor AMD Ryzen 4000H dengan 8 core dan 16
                                    thread. Untuk grafis nya sendiri,
                                    Dell memilih GPU AMD Radeon RX 5600M yang baru diluncurkan.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-2.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>ASUS ROG Zephyrus G14</h3>
                                <p>Asus ROG Zephyrus G14, dengan menggunakan prosesor mobile AMD Ryzen 4000 yang baru,
                                    telah memberikan dampak yang besar
                                    pada ajang CES 2020.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-1.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>Dell G15 Special Edition</h3>
                                <p>Dell G15 Special Edition, menggunakan prosesor AMD Ryzen 4000H dengan 8 core dan 16
                                    thread. Untuk grafis nya sendiri,
                                    Dell memilih GPU AMD Radeon RX 5600M yang baru diluncurkan.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->

                <!-- start:slider box -->
                <div class="slider__box">
                    <div class="slider-wrap slider-wrap--article">
                        <div class="article">
                            <div class="article__overlay"></div>
                            <img src="./assets/images/banner-artikel-2.jpg" alt="Banner Artikel">
                            <div class="article__content">
                                <h3>ASUS ROG Zephyrus G14</h3>
                                <p>Asus ROG Zephyrus G14, dengan menggunakan prosesor mobile AMD Ryzen 4000 yang baru,
                                    telah memberikan dampak yang
                                    besar pada ajang CES 2020.</p>
                                <a href="#" class="btn btn-primary">
                                    <span>Read More</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.762" height="9.907">
                                        <g transform="translate(-332 -694.939)">
                                            <path data-name="Path 1" d="M339.119 695.998l3.894 3.9-3.893 3.893"
                                                fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="1.5" />
                                            <rect data-name="Rectangle 10" width="11.171" height="1.505" rx=".753"
                                                transform="translate(332 698.945)" fill="#fff" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:/slider box -->
            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/article -->

<!-- start:info service -->
<section class="info-service">
    <div class="container">
        <!-- start:info service wrap -->
        <div class="info-service__wrap">
            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-free-delivery.svg" alt="Icon delivery">
                <div class="block">
                    <div class="title">Free Shipping</div>
                    <p class="mb-0">Untuk Order Di Atas 10jt</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-return.svg" alt="Icon return">
                <div class="block">
                    <div class="title">Free Returns</div>
                    <p class="mb-0">Jika produk yang diterima rusak</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-payment-secure.svg" alt="Icon payment secure">
                <div class="block">
                    <div class="title">100% Pay Secure</div>
                    <p class="mb-0">Pembayaranmu terlindungi</p>
                </div>
            </div>
            <!-- end:/info service item -->

            <!-- start:info service item -->
            <div class="info-service__item">
                <img src="./assets/images/icons/icon-cs.svg" alt="Icon customer service">
                <div class="block">
                    <div class="title">Support 24/7</div>
                    <p class="mb-0">Kontak kami 24 jam sehari</p>
                </div>
            </div>
            <!-- end:/info service item -->
        </div>
        <!-- end:/info service wrap -->
    </div>
</section>
<!-- end:/info service -->

@endsection
@push('js')
<script>
    init_countdown()
</script>
@endpush
