<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ItemBest;
use App\Models\ItemHot;
use App\Models\ItemImage;
use App\Models\ItemNewest;
use App\Models\ItemReview;
use App\Models\ItemSelect;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class AdditionalController extends Controller
{
    public function ItemFormat($items)
    {
        foreach ($items as $item) {
            if ($item->variants != null) {
                $item_id = $item->variants->product->id;
            } else {
                $item_id = $item->id;
            }
            // image
            $item->imagePrimary = null;
            if ($item->variants->product->primaryImage != null) {
                $image = $item->variants->product->primaryImage->img_path ?? '' . $item->variants->product->primaryImage->img_path ?? '';
                $item->imagePrimary = $image;
            }
            // if ($image != null) {
            //     $item->image = $image->file;
            // } else {
            //     $image = ItemImage::where('item_id', $item_id)->first();
            //     if ($image != null) {
            //         $item->image = $image->file;
            //     }
            // }

            // rating
            $rating = ItemReview::where('item_id', $item_id)->get();
            $avg_rating = $rating->avg('rating');
            $total_rating = count($rating);
            $item->avg_rating = $avg_rating;
            $item->total_rating = $total_rating;

            // if ($item->price != null) {
            //     $price = $item->price;
            // } else {
            //     $price = $item->variants->product->price;
            // }
            $price = $item->variants->price;
            $item->price = $price;

            // end date
            $end_date = new Carbon($item->variants->end_deal);
            $diffInSeconds = Carbon::now()->diffInSeconds($end_date, false);
            if ($diffInSeconds > 1) {
                $dtF = new DateTime('@0');
                $dtT = new DateTime("@$diffInSeconds");
                $finalDiff =  $dtF->diff($dtT);
                $diff['day'] = $finalDiff->d;
                $diff['jam'] = $finalDiff->h;
                $diff['menit'] = $finalDiff->i;
                $diff['detik'] = $finalDiff->s;
                $item->end_date = $diff;
            }

            if ($item->variants->discount_price != null && $diffInSeconds > 1) {
                // discount
                $discount = (($price - $item->variants->discount_price) * 100) / $price;
                $item->discount = '-' . floor($discount) . '%';
                $item->discount_price = $item->variants->discount_price;
            }
            // if ($item->description != null) {
            //     $description = $item->description;
            // } else {
            //     $description = $item->variants->product->description;
            // }
            $description = $item->variants->product->description;
            $item->description = $description;

            // if ($item->category_id != null) {
            //     $cat = Category::where('id', $item->category_id)->first();
            //     if ($cat != null) {
            //         $item->category_name = $cat->name;
            //     }
            // } else {
            //     $cat = Category::where('id', $item->product->category_id)->first();
            //     if ($cat != null) {
            //         $item->product->category_name = $cat->name;
            //     }
            // }

            // if ($item->sub_category_id != null) {
            //     $subcat = Category::where('id', $item->sub_category_id)->first();
            //     if ($subcat != null) {
            //         $item->sub_category_name = $subcat->name;
            //     }
            // } else {
            //     $subcat = Category::where('id', $item->product->sub_category_id)->first();
            //     if ($subcat != null) {
            //         $item->product->sub_category_name = $subcat->name;
            //     }
            // }

            // $variant = ItemStockVariant::where('item_id', $item_id)->get();
            // $item->variants = $variant;
        }
        return $items;
    }

    // public function checkSectionItem($item_id)
    // {
    //     $hot = ItemHot::find($item_id);
    //     $pilihan = ItemSelect::find($item_id);
    //     $best = ItemBest::find($item_id);
    //     $newest = ItemNewest::find($item_id);
    //     if ($hot != null){
    //         return $hot;
    //     } else if ($pilihan != null){
    //         return $pilihan;
    //     } else if ($best != null){
    //         return $best;
    //     } else if ($newest != null){
    //         return $newest;
    //     } else {
    //         return false;
    //     }
    // }
}
