<!doctype html>
<html lang="id">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex, nofollow">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- start:stylesheets -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/star-rating-svg@3.5.0/src/css/star-rating-svg.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/main.css')); ?>">
    <!-- end:/stylesheets -->
    <?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>
    <?php echo $__env->yieldPushContent('css'); ?>

</head>

<body>
    <?php echo $__env->make('layouts.loading', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('content-main'); ?>
    <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <!-- start:javascripts -->
    <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/star-rating-svg@3.5.0/src/jquery.star-rating-svg.js"></script>
    <script src="<?php echo e(asset('assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/style.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/slick-slider.js')); ?>"></script>
    <!-- end:/javascripts -->
    <script>
        let deferredPrompt;
        const addBtn = document.querySelector('.add-button');
        window.addEventListener('beforeinstallprompt', (e) => {
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            // Stash the event so it can be triggered later.
            deferredPrompt = e;
            // Update UI to notify the user they can add to home screen
            addBtn.style.display = 'block';

            addBtn.addEventListener('click', (e) => {
                // hide our user interface that shows our A2HS button
                addBtn.style.display = 'none';
                // Show the prompt
                deferredPrompt.prompt();
                // Wait for the user to respond to the prompt
                deferredPrompt.userChoice.then((choiceResult) => {
                    if (choiceResult.outcome === 'accepted') {
                    console.log('User accepted the A2HS prompt');
                    } else {
                    console.log('User dismissed the A2HS prompt');
                    }
                    deferredPrompt = null;
                });
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js"></script>

    <script>
        // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
            apiKey: "AIzaSyBKy9xFR6x-L_ufxg9nRsev1VtVyO3fu9U",
            authDomain: "web-app-agres.firebaseapp.com",
            projectId: "web-app-agres",
            storageBucket: "web-app-agres.appspot.com",
            messagingSenderId: "594855414554",
            appId: "1:594855414554:web:8e904d2d89d94268e83aa3",
            measurementId: "G-C9VPBELP6C"
        };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    </script>

    <script src="<?php echo e(asset("assets/js/custom.js")); ?>"></script>
    <script>
        $('.addToWishlist').click(function(e){
            e.preventDefault();
            var item_id = $(this).data('item_id')
            var url = `<?php echo e(url('/')); ?>/member/wishlist`
            $.ajax({
                url:url,
                method:"POST",
                data: {item_id:item_id},
                success:function(data){
                }
            });
        })

        $('.addToCart').click(function(e){
            e.preventDefault();
            var item_id = $(this).data('item_id')
            var price = $(this).data('price')
            var url = `<?php echo e(url('/')); ?>/member/cart`
            $.ajax({
                url:url,
                method:"POST",
                data: {item_id:item_id,price:price},
                success:function(data){
                }
            });
        })

        $('.check').change(function(){
            var arr = []
            $('input[type=checkbox]:checked').each(function() {
                var item_id = $(this).data('item_id')
                var qty = $(this).data('qty')
                var current = {'item_id':item_id,'qty':qty}
                arr.push(current)
            });

            $.ajax({
                url: '<?php echo e(url('/')); ?>/member/cart/subtotal',
                method:"POST",
                data: {arr:arr},
                success:function(data){
                }
            });
        })

        $('.qty').change(function(){
            var cart_id = $(this).data('cart_id')
            var qty = $(this).data('qty')
            $.ajax({
                url:'<?php echo e(url('/')); ?>/member/cart/updateqty',
                method:"POST",
                data: {item_id:item_id,qty:qty},
                success:function(data){
                }
            });
        })
    </script>
    <?php echo $__env->yieldPushContent('js'); ?>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\agres-new\engine\resources\views/layouts/master.blade.php ENDPATH**/ ?>