<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models\Category;
use App\Models\ItemHot;
use App\Models\ItemSelect;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemReview;
use App\Models\ItemSelectBanner;
use App\Models\ItemSort;
use App\Models\SubCategory;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ItemController extends Controller
{
    public function getItemCategory()
    {
        $categories = Category::with('subcat')->get();
        foreach ($categories as $category) {
            $category->spesification = json_decode($category->spesification);
        }
        if (count($categories) > 0) {
            return $this->responseBase($categories);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }
    public function getFilter()
    {
        $d['category'] = Category::all();
        // $maxprice = Item::orderBy('price', 'desc')->first();
        // $minprice = Item::orderBy('price', 'asc')->first();
        // $d['max_price'] = $maxprice->price;
        // $d['minprice'] = $minprice->price;
        return $this->responseBase($d);
    }
    public function getSort()
    {
        $sort = ItemSort::all();
        return $this->responseBase($sort);
    }
    public function responseItemFormat($items)
    {
        foreach ($items as $item) {

            if ($item->product != null) {
                $item_id = $item->product->id;
            } else {
                $item_id = $item->id;
            }
            // image
            $item->image = null;
            $image = ItemImage::where('item_id', $item_id)->where('is_primary', 1)->first();
            if ($image != null) {
                $item->image = $image->img_path . $image->img_name;
            } else {
                $image = ItemImage::where('item_id', $item_id)->first();
                if ($image != null) {
                    $item->image = $image->img_path . $image->img_name;
                }
            }
            // rating
            $rating = ItemReview::where('item_id', $item_id)->get();
            $avg_rating = $rating->avg('rating');
            $total_rating = count($rating);
            $item->avg_rating = $avg_rating;
            $item->total_rating = $total_rating;


            // discount
            if ($item->price != null) {
                $price = $item->price;
            } else {
                $price = $item->product->price;
            }
            if ($item->discount_price != null) {
                $dicsount = (($price - $item->discount_price) * 100) / $price;
                $item->discount = floor($dicsount) . '%';
            }

            // end date
            $now = Carbon::now();
            $end_date = new Carbon($item->end_deal);
            $diffInSeconds = $end_date->diffInSeconds($now);
            if ($diffInSeconds > 1) {
                $dtF = new DateTime('@0');
                $dtT = new DateTime("@$diffInSeconds");
                $finalDiff =  $dtF->diff($dtT);
                $diff['day'] = $finalDiff->d;
                $diff['jam'] = $finalDiff->h;
                $diff['menit'] = $finalDiff->i;
                $diff['detik'] = $finalDiff->s;
                $item->end_date = $diff;
            }

            if ($item->description != null) {
                $description = $item->description;
            } else {
                $description = $item->product->description;
            }
            $item->description = $description;

            //cat
            if ($item->category_id != null) {
                $cat = Category::where('id', $item->category_id)->first();
                if ($cat != null) {
                    $item->category_name = $cat->name;
                    $slug_cat = str_replace(' ', '-', $cat->name);
                }
            } else {
                $cat = Category::where('id', $item->product->category_id)->first();
                if ($cat != null) {
                    $item->product->category_name = $cat->name;
                    $slug_cat = str_replace(' ', '-', $cat->name);
                }
            }

            //subcat
            if ($item->sub_category_id != null) {
                $subcat = SubCategory::where('id', $item->sub_category_id)->first();
                if ($subcat != null) {
                    $item->sub_category_name = $subcat->name;
                    $slug_subcat = str_replace(' ', '-', $subcat->name);
                }
            } else {
                $subcat = SubCategory::where('id', $item->product->sub_category_id)->first();
                if ($subcat != null) {
                    $item->product->sub_category_name = $subcat->name;
                    $slug_subcat = str_replace(' ', '-', $subcat->name);
                }
            }

            //slug
            if ($item->slug != null) {
                $item->slug = url('/') . '/' . $slug_cat . '/' . $slug_subcat . '/' . $item->slug;
            } else {
                $item->product->slug = url('/') . '/' . $slug_cat . '/' . $slug_subcat . '/' . $item->slug;
            }
        }
        return $items;
    }
    public function getHotdealProduct(Request $request)
    {
        if ($request->keyword != null) {
            $masterItem = Item::where('name', 'like', '%' . $request->keyword . '%')->get()->pluck('id')->toArray();
            $items = $this->responseItemFormat(ItemHot::whereIn('id', $masterItem)->with('product')->paginate(12));
        } else {
            $items = $this->responseItemFormat(ItemHot::with('product')->paginate(12));
        }
        if (count($items) > 0) {
            return $this->responseBase($items->items());
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getChoosenProduct(Request $request)
    {
        if ($request->keyword != null) {
            $masterItem = Item::where('name', 'like', '%' . $request->keyword . '%')->get()->pluck('id')->toArray();
            $d['item'] = $this->responseItemFormat(ItemSelect::whereIn('id', $masterItem)->with('product')->paginate(12));
        } else {
            $d['item'] = $this->responseItemFormat(ItemSelect::with('product')->paginate(12));
        }
        $d['banner'] = ItemSelectBanner::first();

        return $this->responseBase($d);
    }

    public function getBestSellserProduct(Request $request)
    {
        if ($request->keyword != null) {
            $items = $this->responseItemFormat(Item::where('name', 'like', '%' . $request->keyword . '%')->where('is_bestseller', 1)->latest()->paginate(12));
        } else {
            $items = $this->responseItemFormat(Item::where('is_bestseller', 1)->latest()->paginate(12));
        }
        if (count($items) > 0) {
            return $this->responseBase($items->items());
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getNewProduct(Request $request)
    {
        if ($request->keyword != null) {
            $items = $this->responseItemFormat(Item::where('name', 'like', '%' . $request->keyword . '%')->latest()->paginate(12));
        } else {
            $items = $this->responseItemFormat(Item::latest()->paginate(12));
        }
        if (count($items) > 0) {
            return $this->responseBase($items->items());
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getItemReview($id)
    {
        $item = ItemReview::where('item_id', $id)->get();
        if (count($item) > 0) {
            return $this->responseBase($item);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getItemDetail($id)
    {
        $item = Item::with('image', 'review', 'longDescription')->find($id);
        if ($item != null) {
            return $this->responseBase($item);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getItemBySearch(Request $request)
    {
        if ($request->main_category != null) {
            if ($request->main_category == 'hot_deal') {
            }
        }
        $main_cat = str_replace(' ', '', ucwords(str_replace('_', ' ', $request->main_category)));
        return response()->json($main_cat, 200);
        $d['item'] = $this->responseItemFormat(ItemSelect::with('product')->paginate(12));
    }

    public function getItemByCategory($category_id)
    {
        $items = $this->responseItemFormat(Item::where('category_id', $category_id)->latest()->paginate(12));
        if (count($items) > 0) {
            return $this->responseBase($items->items());
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function getItemBySubCategory($sub_category_id)
    {
        $items = $this->responseItemFormat(Item::where('sub_category_id', $sub_category_id)->latest()->paginate(12));
        if (count($items) > 0) {
            return $this->responseBase($items->items());
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    /**
     * spesifikasi
     * operator
     * value
     */
    public function filtersortItem(Request $request)
    {
        //get index spesifikasi
        $specCat = Category::find($request->category_id);
        $bagIndex = [];
        foreach ($request->spesification as $value) {
            $indexC = array_search($value, json_decode($specCat->spesification));
            if (is_int($indexC)) {
                array_push($bagIndex, $indexC);
            }
        }
        $dataFilter = Item::select("*")->where("category_id", $request->category_id)->orderBy($request->sort["field"], $request->sort["direction"])->get();
        $filtered = new Collection();
        foreach ($dataFilter as  $value) {
            $specData = json_decode($value->spesification, TRUE);
            $flag = 0;
            for ($i = 0; $i < count($request->spesification); $i++) {

                if ($specData[$request->spesification[$i]] == $request->value[$i]) {
                    $flag++;
                };
            }
            if ($flag == count($request->spesification)) {
                $filtered->push($value);
            }
        }
        // return response()->json($filtered);

        return $this->responseBase($filtered);
    }

    public function searchAll(Request $request)
    {
        $main_category = $request->main_category;
        $category_id = $request->category_id;
        $sub_category_id = $request->sub_category_id;
        $sort = $request->sort;

        $items = Item::where('name', 'like', "%$request->keyword%");
        if ($main_category != null) {
            if ($main_category == 'hot_deal') {
                $id = ItemHot::get()->pluck('item_id')->toArray();
            } else if ($main_category == 'prod_pilihan') {
                $id = ItemSelect::get()->pluck('item_id')->toArray();
            } else if ($main_category == 'bestseller') {
                $id = Item::where('is_bestseller', 1)->get()->pluck('id')->toArray();
            }
            $items = $items->whereIn('id', $id);
        }

        if ($category_id != null) {
            $items = $items->where('category_id', $category_id);
        }

        if ($sub_category_id != null) {
            $items = $items->where('sub_category_id', $sub_category_id);
        }

        if ($sort != null && count($sort) == 2) {
            $items = $items->orderBy($request->sort[0], $request->sort[1]);
        }

        $d['items'] = $this->responseItemFormat($items->get());
        $d['main_category'] = [
            'hot_deal', 'prod_pilihan', 'bestseller'
        ];
        return $this->responseBase($d);
    }
}
