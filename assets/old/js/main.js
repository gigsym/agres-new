// header sticky
window.onscroll = function () { myFunction() };

var header = $('.navbar-first');
var offset = header.offset();

function myFunction() {
    if (window.pageYOffset > offset.top) {
        header.addClass("sticky");
        $('.navbar-second').addClass("sticky");
        $('.navbar-first .navbar').removeClass("shadow");
        $('#main_container').addClass('container-with-sticky')
        $('.logo-homepage').attr('src', "./assets/image/logo/agres-id-logo-white.png")
    } else {
        header.removeClass("sticky");
        $('.navbar-second').removeClass("sticky");
        $('.navbar-first .navbar').addClass("shadow");
        $('#main_container').removeClass('container-with-sticky')
        $('.logo-homepage').attr('src', "./assets/image/logo/agres-id-logo-color.png")
    }

    // var lastScrollTop = 0, delta = 5;
    // $(window).scroll(function () {
    //     var nowScrollTop = $(this).scrollTop();
    //     if (Math.abs(lastScrollTop - nowScrollTop) >= delta) {
    //         if (nowScrollTop > lastScrollTop) {
    //             console.log('scrollDown');
    //         } else if (nowScrollTop < lastScrollTop) {
    //             console.log('scrollUp');
    //         }
    //         lastScrollTop = nowScrollTop;
    //     }
    // });
    // let oldValue = 0;
    // window.addEventListener('scroll', function (e) {
    //     // Get the new Value
    //     newValue = window.pageYOffset;
    //     //Subtract the two and conclude
    //     if (oldValue - newValue < 0) {
    //         console.log("Up");
    //     } else if (oldValue - newValue > 0) {
    //         console.log("Down");
    //     }
    //     // Update the old value
    //     oldValue = newValue;
    // });

    var c, currentScrollTop = 0;
    navbar_first = $('.navbar-first');
    navbar_second = $('.navbar-second');

    $(window).scroll(function () {
        var a = $(window).scrollTop();
        var b = navbar_first.height() + navbar_second.height();

        currentScrollTop = a;
        if (c < currentScrollTop && a > b + b) {
            navbar_first.css("top", 0);
        } else if (c > currentScrollTop && !(a <= b)) {
            navbar_first.css("top", '55px');
        }
        c = currentScrollTop;
    });

    $('.homepage-banner').slick({
    });

}


// end header sticky