@extends('layouts.master')
@push('css')
<link rel="stylesheet" href="{{asset("css/custom.css")}}">
<link rel="stylesheet" href="{{asset('assets/css/tempusdominus-bootstrap-4.min.css')}}">
@endpush
@section('content-main')
<!-- start:profile -->
<section class="profile">
    <div class="container">
        <!-- start:profile main -->
        <div class="profile__main">
            <div class="block">
                <!-- start:block profile -->
                <div class="block__profile">
                    <!-- start:profile box -->
                    <div class="block__profile__box">
                        <img src="{{asset($profile->image ?? '/assets/images/profile-photo.jpg')}}" alt="Profile Photo"
                            id="profilepict">
                        <form action="{{route('profile.update',[Auth::user()->id])}}" method="POST"
                            enctype="multipart/form-data" id="formpict">
                            @csrf
                            @method('PUT')
                            <input class="input-img" name="image" type="file" accept="image/x-png,image/gif,image/jpeg"
                                id="inputpict">
                            <label for="inputpict" class="btn btn-primary btn-block" id="image-profile">Ubah/Pilih
                                Foto</label>
                        </form>
                        <p class="info">Besar file Max. 10MB <br> File harus berupa .JPG .JPEG .PNG</p>
                    </div>
                    <!-- end:/profile box -->

                    <!-- start:profile info -->
                    <div class="block__profile__info">
                        <h4>{{$profile->name ?? '-'}}</h4>
                        <p class="mb-0">Member sejak {{(Carbon\Carbon::parse($profile->created_at)->format('d F Y')) ?? '-'}}</p>
                    </div>
                    <!-- end:/profile info -->

                    <!-- start:profile menu -->
                    <div class="block__profile__menu">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                            aria-orientation="vertical">

                            <a class="nav-link" id="v-pills-account-tab" data-toggle="pill" href="#v-pills-account"
                                role="tab" aria-controls="v-pills-account" aria-selected="true">Informasi Akun</a>

                            <a class="nav-link" id="v-pills-security-tab" data-toggle="pill" href="#v-pills-security"
                                role="tab" aria-controls="v-pills-security" aria-selected="true">Keamanan</a>

                            <a class="nav-link" id="v-pills-address-tab" data-toggle="pill" href="#v-pills-address"
                                role="tab" aria-controls="v-pills-address" aria-selected="true">Daftar Alamat</a>

                            <a class="nav-link" id="v-pills-wishlist-tab" data-toggle="pill" href="#v-pills-wishlist"
                                role="tab" aria-controls="v-pills-wishlist" aria-selected="true">Wishlist</a>

                            <a class="nav-link" id="v-pills-order-tab" data-toggle="pill" href="#v-pills-order"
                                role="tab" aria-controls="v-pills-order" aria-selected="true">Pesanan</a>

                            <a class="nav-link" id="v-pills-discussion-tab" data-toggle="pill"
                                href="#v-pills-discussion" role="tab" aria-controls="v-pills-discussion"
                                aria-selected="true">Diskusi</a>

                            <a class="nav-link" href="{{route('logout')}}" role="tab" aria-controls=""
                                aria-selected="true">Keluar</a>
                            {{--
                                    <a href="#">Wishlist</a>
                                </li>
                                <li>
                                    <a href="#">Pesanan</a>
                                </li>
                                <li>
                                    <a href="#">Ulasan</a>
                                </li>
                                <li>
                                    <a href="#">Diskusi</a>
                                </li>
                                <li>
                                    <a href="#">Keluar</a>
                                </li>
                            </ul> --}}
                        </div>
                    </div>
                    <!-- end:/profile menu -->
                </div>
                <!-- end:/block profile -->

                <!-- start:block content -->
                <div class="block__content">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-account" role="tabpanel"
                            aria-labelledby="v-pills-account-tab">
                            @include("member.page.datapengguna")
                        </div>
                        <div class="tab-pane fade" id="v-pills-security" role="tabpanel"
                            aria-labelledby="v-pills-security-tab">
                            @include("member.page.keamanan")
                        </div>
                        <div class="tab-pane fade" id="v-pills-address" role="tabpanel"
                            aria-labelledby="v-pills-address-tab">
                            <div class="block__content__title">
                                <h2>Daftar Alamat</h2>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-wishlist" role="tabpanel"
                            aria-labelledby="v-pills-wishlist-tab">
                            <div class="block__content__title">
                                <h2>Wishlist</h2>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-order" role="tabpanel"
                            aria-labelledby="v-pills-order-tab">
                            <div class="block__content__title">
                                <h2>Pesanan</h2>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-discussion" role="tabpanel"
                            aria-labelledby="v-pills-discussion-tab">
                            <div class="block__content__title">
                                <h2>Diskusi</h2>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end:/block content -->
            </div>
        </div>
        <!-- end:/profile main -->
    </div>
</section>
{{-- <div class="row">
    <div class="col-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
                aria-controls="v-pills-profile" aria-selected="false">Profile</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab"
                aria-controls="v-pills-messages" aria-selected="false">Messages</a>
            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab"
                aria-controls="v-pills-settings" aria-selected="false">Settings</a>
        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-account" role="tabpanel"
                aria-labelledby="v-pills-account-tab">
                ...</div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...
            </div>
            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...
            </div>
            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...
            </div>
        </div>
    </div>
</div> --}}
<!-- end:/profile -->
@endsection
@include("member.page.modal")


@push('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script>
    $('#birthday').datetimepicker({
        format: 'L'
    });

    // Modal Trigger Next Email
    $('#nextEmailVerification').click(function () {
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'email'},
            success:function(data){
                if (data.message == 'success'){
                    $('#modalEmail').modal('hide')
                    $('#modalEmailVerification').modal('show')
                } else {
                }
            }
        });

    });
    $('#nextNewEmail').click(function (e) {
        e.preventDefault;
        var otp = $("input[name='otpemail[]']").map(function(){return $(this).val();}).get();
        $("input[name='otpemailfinal']").val('');
        var length = 0
        $.each(otp, function (index, value) {
            if (value == ""){
                $('#otpemailinp-'+index).focus()
            } else {
                length++
                $("input[name='otpemailfinal']").val($("input[name='otpemailfinal']").val()+value);
            }
        })
        if (length == 6){
            var finalotp = $("input[name='otpemailfinal']").val()
            $.ajax({
                url:"{{url('/')}}/member/verifyOtp",
                method:"POST",
                data: {otp:finalotp,type:'email'},
                success:function(data){
                    if (data.message == 'success'){
                          $('#modalEmailVerification').modal('hide')
                          $('#modalNewEmail').modal('show')
                    } else {
                        $('#resotpemail').empty().append(data.message)
                    }
                }
            });
        }
    });
    $('#nextNewEmailVerification').click(function () {
        var email = $("input[name='newemail']").val();
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'email', email:email},
            success:function(data){
                if (data.message == 'success'){
                    $('#modalNewEmail').modal('hide')
                    $('#modalNewEmailVerification').modal('show')
                } else {
                }
            }
        });
    });
    $('#nextNewEmailVerificationFinish').click(function () {
        var otp = $("input[name='otpnewemail[]']").map(function(){return $(this).val();}).get();
        $("input[name='otpnewemailfinal']").val('');
        var length = 0
        $.each(otp, function (index, value) {
            if (value == ""){
                $('#otpnewemailinp-'+index).focus()
            } else {
                length++
                $("input[name='otpnewemailfinal']").val($("input[name='otpnewemailfinal']").val()+value);
            }
        })
        if (length == 6){
            var finalotp = $("input[name='otpnewemailfinal']").val()
            $.ajax({
                url:"{{url('/')}}/member/verifyOtp",
                method:"POST",
                data: {otp:finalotp,type:'email'},
                success:function(data){
                    if (data.message == 'success'){
                        $('#modalNewEmailVerification').modal('hide')
                        $('#modalNewEmailVerificationFinish').modal('show')
                    } else {
                        $('#resotpnewemail').empty().append(data.message)
                    }
                }
            });
        }
    });

    // Modal Trigger Next Nomor Handphone
    $('#nextHandphoneVerification').click(function () {
      $('#modalHandphone').modal('hide')
      $('#modalHandphoneVerification').modal('show')
    });
    $('.next-verification-code').click(function () {
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'phone'},
            success:function(data){
                if (data.message == 'success'){
                    $('#modalHandphoneVerification').modal('hide')
                    $('#modalHandphoneVerificationCode').modal('show')
                } else {
                }
            }
        });
    });
    $('#nextNewHandphone').click(function () {
        var otp = $("input[name='otpphone[]']").map(function(){return $(this).val();}).get();
        $("input[name='otpphonefinal']").val('');
        var length = 0
        $.each(otp, function (index, value) {
            if (value == ""){
                $('#otpphoneinp-'+index).focus()
            } else {
                length++
                $("input[name='otpphonefinal']").val($("input[name='otpphonefinal']").val()+value);
            }
        })
        if (length == 6){
            var finalotp = $("input[name='otpphonefinal']").val()
            $.ajax({
                url:"{{url('/')}}/member/verifyOtp",
                method:"POST",
                data: {otp:finalotp,type:'phone'},
                success:function(data){
                    if (data.message == 'success'){
                        $('#modalHandphoneVerificationCode').modal('hide')
                        $('#modalNewHandphone').modal('show')
                    } else {
                        $('#resotpphone').empty().append(data.message)
                    }
                }
            });
        }
    });
    $('#nextHandphoneVerificationSms').click(function () {
        var phone = $("input[name='newphone']").val();
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'phone', phone:phone},
            success:function(data){
                if (data.message == 'success'){
                    $('#modalNewHandphone').modal('hide')
                    $('#modalHandphoneVerificationCodeSms').modal('show')
                } else {
                }
            }
        });
    });
    $('#nextHandphoneVerificationFinish').click(function () {
        var otp = $("input[name='otpnewphone[]']").map(function(){return $(this).val();}).get();
        $("input[name='otpnewphonefinal']").val('');
        var length = 0
        $.each(otp, function (index, value) {
            if (value == ""){
                $('#otpnewphoneinp-'+index).focus()
            } else {
                length++
                $("input[name='otpnewphonefinal']").val($("input[name='otpnewphonefinal']").val()+value);
            }
        })
        if (length == 6){
            var finalotp = $("input[name='otpnewphonefinal']").val()
            $.ajax({
                url:"{{url('/')}}/member/verifyOtp",
                method:"POST",
                data: {otp:finalotp,type:'phone'},
                success:function(data){
                    if (data.message == 'success'){
                        $('#modalHandphoneVerificationCodeSms').modal('hide')
                        $('#modalHandphoneVerificationFinish').modal('show')
                    } else {
                        $('#resotpnewphone').empty().append(data.message)
                    }
                }
            });
        }
    });

    $('.resendemail').click(function(e){
        e.preventDefault
        if ($(this).attr('disabled')== 'disabled'){
            return
        }
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'email'},
            success:function(data){
                if (data.message == 'success'){
                    $('.resendemail').css("pointer-events","none")
                    setTimeout(function() {
                        $(".resendemail").css("pointer-events","auto")
                    }, 30000)
                } else {
                }
            }
        });
    })
    $('.resendphone').click(function(e){
        e.preventDefault
        if ($(this).attr('disabled')== 'disabled'){
            return
        }
        $.ajax({
            url:"{{url('/')}}/member/sendotp",
            method:"POST",
            data: {type:'phone'},
            success:function(data){
                if (data.message == 'success'){
                    $('.resendphone').css("pointer-events","none")
                    setTimeout(function() {
                        $(".resendphone").css("pointer-events","auto")
                    }, 30000)
                } else {
                }
            }
        });
    })
</script>

<script>
    // Input OTP Verification
    function OTPInput() {
      const inputs = document.querySelectorAll('#otp > *[id]');
      for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('keydown', function (event) {
          if (event.key === "Backspace") {
            inputs[i].value = '';
            if (i !== 0)
              inputs[i - 1].focus();
          } else {
            if (i === inputs.length - 1 && inputs[i].value !== '') {
              return true;
            } else if (event.keyCode > 47 && event.keyCode < 58) {
              inputs[i].value = event.key;
              if (i !== inputs.length - 1)
                inputs[i + 1].focus();
              event.preventDefault();
            } else if (event.keyCode > 64 && event.keyCode < 91) {
              inputs[i].value = String.fromCharCode(event.keyCode);
              if (i !== inputs.length - 1)
                inputs[i + 1].focus();
              event.preventDefault();
            }
          }
        });
      }
    }
    OTPInput();
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader()

            reader.onload = function(e) {
                $('#profilepict').attr('src', e.target.result);
                $('#profilepict').css('padding', '15px')
            }

            reader.readAsDataURL(input.files[0])
        }
    }

    $("#inputpict").change(function() {
        $("#formpict").submit();
        // readURL(this)
    });

</script>
@endpush
