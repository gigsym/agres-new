<?php

namespace App\Http\Controllers\Api;

use App\Models\MemberCart;
use App\Models\MemberWishlist;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseBase($data, $status = 200, $message = '')
    {
        $responsebase["status"] = $status;
        $responsebase["time"] = date("d-m-Y H:i:s");
        if (auth('api')->check()){
            $responsebase["user"] = auth('api')->user();
            $responsebase["user"]["fav_length"] = count(MemberWishlist::where("member_id",auth('api')->user()->id)->get());
            $responsebase["user"]["cart_length"] = count(MemberCart::where("member_id",auth('api')->user()->id)->get());
        }
        $responsebase["message"] = $message;
        $responsebase["data"] = $data;

        return response()->json($responsebase);
    }

    function kirimOtp($param)
    {
        $client = new Client([
            'base_uri' => 'https://admin.ladiestory.id/kirimwamo.php?act=device52',
            'timeout'  => 10,
        ]);
        $client->post("", ["json" => $param]);
        // try {
        //     $client->post("", ["json" => $param]);
        // } catch (\GuzzleHttp\Exception\GuzzleException $th) {
        //     return $th;
        // }
        return $client;
    }
}
