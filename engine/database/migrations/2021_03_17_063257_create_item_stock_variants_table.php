<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStockVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stock_variants', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('item_id');
            $table->string('variant', 50);
            $table->integer('stock');
            $table->integer('price')->default(0);
            $table->timestamps();
            $table->softDeletes();        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stock_variants');
    }
}
