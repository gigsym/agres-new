@extends('layouts.auth-master')
@section('content')
<!-- start:acccounts -->
<section class="accounts accounts--login">
    <div class="accounts__content">
        <!-- start:title -->
        <div class="title">
            <h3>Masuk ke Akunmu</h3>
            <p>Belum Punya Akun Agres.id? <a href="{{route('register.email')}}">Klik Disini!</a></p>
        </div>
        <!-- end:/title -->

        <!-- start:form -->
        <form action="{{route('login.email')}}" method="POST">
            @csrf
            <div class="form-group">
                <input type="email" name="email" class="form-control @error('email') invalid @enderror" required>
                <label class="placeholder">Email</label>
                <span class="icon icon--close"></span>
                @error('email')
                <p class="invalid-info">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <input id="password-field" name="password" type="password" class="form-control @error('password') invalid @enderror" required>
                <label class="placeholder">Password</label>
                <span toggle="#password-field" class="icon icon--eye close toggle-password"></span>
                @error('password')
                <p class="invalid-info">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group d-flex">
                <div class="form-check abc-checkbox abc-checkbox-primary">
                    <input class="form-check-input" name="membership" id="membership" type="checkbox">
                    <label class="form-check-label" for="membership">
                        Ingat saya
                    </label>
                </div>
                <a href="javascript:;" class="forgot-password">Lupa Kata Sandi?</a>
            </div>
            <button type="submit" class="btn btn-primary d-block m-auto">Masuk</button>
        </form>
        <!-- end:/form -->

        <!-- start:content bottom -->
        <div class="accounts__content__bottom">
            <p class="italic">Atau masuk menggunakan</p>
            <ul class="list-unstyled">
                <li>
                    <a href="javascript:;" id="google_signin">
                        <img src="{{asset("assets/images/icons/icon-register-google.svg")}}" alt="Icon google">
                        <span>Google</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" id="facebook_signin">
                        <img src="{{asset("assets/images/icons/icon-register-facebook.svg")}}" alt="Icon facebook">
                        <span>Facebook</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/')}}/member/login/wa">
                        <img src="{{asset("assets/images/icons/icon-register-whatsapp.svg")}}" alt="Icon whatsapp">
                        <span>Whatsapp</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end:/content bottom -->
    </div>
</section>
<!-- end:/acccounts -->
@endsection
@push('js')
<script>
    $(".toggle-password").click(function () {
        $(this).toggleClass("open");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@endpush
