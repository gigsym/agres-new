<!-- ###### MODALS ###### -->
<!-- start: Modal Data Pengguna -->
<div class="modal modal--simple fade" id="modalDataPengguna" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <form action="{{route('profile.update', Auth::user()->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="modal-title">
                        <h3>Ubah Data Pengguna</h3>
                        <p>Ubah atau lengkapi data akun kamu, agar akun berjalan lebih optimal</p>
                    </div>
                    <div class="block">
                        <div class="row justify-content-center">
                            <div class="col-lg-9">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" class="form-control" name="name" placeholder="Nama Lengkap"
                                        value="{{$profile->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress">Tanggal Lahir</label>
                                    <div class="input-group date" id="birthday" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="birthday" ata-target="#birthday"/>
                                        <div class="input-group-append" data-target="#birthday" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-row">
                                        <div class="col">
                                            <select class="form-control">
                                                @for ($i = 1; $i < 32; $i++)
                                                    <option>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <select class="form-control">
                                                <option>Januari</option>
                                                <option>Februari</option>
                                                <option>Maret</option>
                                                <option>April</option>
                                                <option>Mei</option>
                                                <option>Juni</option>
                                                <option>July</option>
                                                <option>Agustus</option>
                                                <option>September</option>
                                                <option>October</option>
                                                <option>November</option>
                                                <option>Desember</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <select class="form-control">
                                                @for ($i = 1990; $i < 32; $i++)
                                                    <option>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <div class="toggle-choose">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="laki-laki"
                                                value="1" @if ($profile->gender ==
                                            1 && $profile->gender != null)checked @endif>
                                            <label class="form-check-label" for="laki-laki">Laki-Laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="perempuan"
                                                value="0" @if ($profile->gender ==
                                            0 && $profile->gender != null)checked @endif>
                                            <label class="form-check-label" for="perempuan">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:/ Modal Data Pengguna -->

<!-- start: Modal Email -->
<!-- Modal email konfirmasi ketika lanjut next modal -->
<div class="modal modal--simple fade" id="modalEmail" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Ubah Email</h3>
                    <p>Setelah email diubah, akun akan keluar dari semua perangkat dan lakukan verifikasi email
                        kembali</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextEmailVerification">Lanjut</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal email verifikasi kode -->
<div class="modal modal--simple fade" id="modalEmailVerification" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <img width="50px" class="m-auto" src="{{asset('assets/images/icons/icon-register-email.svg')}}"
                        alt="Icon Email">
                    <h3 class="mt-3">Masukan Kode Verifikasi</h3>
                    <p>Kode Verifikasi telah dikirimkan melalui Email ke I**********a@mail.com</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <!-- start:verification -->
                            <div class="block__verification">
                                <div id="otp" class="d-flex justify-center">
                                    <input class="text-center form-control" type="tel" id="otpemailinp-0" maxlength="1" name="otpemail[]"/>
                                    <input class="text-center form-control" type="tel" id="otpemailinp-1" maxlength="1" name="otpemail[]"/>
                                    <input class="text-center form-control" type="tel" id="otpemailinp-2" maxlength="1" name="otpemail[]"/>
                                    <input class="text-center form-control" type="tel" id="otpemailinp-3" maxlength="1" name="otpemail[]"/>
                                    <input class="text-center form-control" type="tel" id="otpemailinp-4" maxlength="1" name="otpemail[]"/>
                                    <input class="text-center form-control" type="tel" id="otpemailinp-5" maxlength="1" name="otpemail[]"/>
                                    <input type="hidden" name="otpemailfinal" value=""/>
                                </div>
                                <p id="resotpemail"></p>
                                <p class="information">Mohon tunggu dalam <b>30 Detik</b> untuk <a
                                        href="javascript:;" class="resendemail"><b>kirim ulang</b></a></p>
                            </div>
                            <!-- end:/verification -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextNewEmail">Verifikasi</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal email baru -->
<div class="modal modal--simple fade" id="modalNewEmail" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Masukan Email Baru</h3>
                    <p>Setelah email diubah, akun akan keluar dari semua perangkat dan lakukan verifikasi email
                        kembali</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <form method="POST" action="">
                                <div class="form-group">
                                    <label>Email Baru</label>
                                    <input type="email" class="form-control" placeholder="Email Baru" name="newemail">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextNewEmailVerification">Lanjut</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal email baru verifikasi kode -->
<div class="modal modal--simple fade" id="modalNewEmailVerification" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <img width="50px" class="m-auto" src="{{asset('assets/images/icons/icon-register-email.svg')}}"
                        alt="Icon Email">
                    <h3 class="mt-3">Masukan Kode Verifikasi</h3>
                    <p>Kode Verifikasi telah dikirimkan melalui Email ke I**********a@mail.com</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <!-- start:verification -->
                            <div class="block__verification">
                                <div id="otp" class="d-flex justify-center">
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-0" maxlength="1" name="otpnewemail[]" />
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-1" maxlength="1" name="otpnewemail[]" />
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-2" maxlength="1" name="otpnewemail[]" />
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-3" maxlength="1" name="otpnewemail[]" />
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-4" maxlength="1" name="otpnewemail[]" />
                                    <input class="text-center form-control" type="tel" id="otpnewemailinp-5" maxlength="1" name="otpnewemail[]" />
                                    <input type="hidden" name="otpnewemailfinal" value=""/>
                                </div>
                                <p id="resotpnewemail"></p>
                                <p class="information">Mohon tunggu dalam <b>30 Detik</b> untuk <a
                                        href="#" class="resendemail"><b>kirim
                                            ulang</b></a></p>
                            </div>
                            <!-- end:/verification -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextNewEmailVerificationFinish">Verifikasi</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal email baru berhasil diubah -->
<div class="modal modal--simple fade" id="modalNewEmailVerificationFinish" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Email Berhasil Diubah</h3>
                    <p>Buka email yang telah didaftarkan dan segera lakukan verifikasi email</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="window.location.reload();">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- end:/ Modal Email -->

<!-- start: Modal Nomor Handphone -->
<!-- Modal Nomor Handphone konfirmasi ketika lanjut next modal -->
<div class="modal modal--simple fade" id="modalHandphone" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Ubah Nomor Handphone</h3>
                    <p>Setelah Nomor Handphone diubah, akun akan keluar dari semua perangkat dan lakukan
                        verifikasi kembali</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextHandphoneVerification">Lanjut</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Nomor Handphone pilih metode verifikasi -->
<div class="modal modal--simple fade" id="modalHandphoneVerification" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Verifikasi</h3>
                    <p class="mb-0">Pilih salah satu metode verifikasi dibawah ini
                        untuk mendapatkan kode verifikasi</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <!-- start:verification -->
                            <div class="block__verification">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="javascript:;" class="next-verification-code">
                                            <img src="{{asset('assets/images/icons/icon-register-whatsapp.svg')}}"
                                                alt="Icon Whatsapp">
                                            <div class="block">
                                                <label>Whatsapp Ke</label>
                                                <p class="mb-0">085745000000</p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="next-verification-code">
                                            <img src="{{asset('assets/images/icons/icon-sms-verifikasi.svg')}}" alt="Icon SMS">
                                            <div class="block">
                                                <label>SMS Ke</label>
                                                <p class="mb-0">085745000000</p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="next-verification-code">
                                            <img src="{{asset('assets/images/icons/icon-telpon-verifikasi.svg')}}"
                                                alt="Icon Telephone">
                                            <div class="block">
                                                <label>Telpon Ke</label>
                                                <p class="mb-0">085745000000</p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end:/verification -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Nomor Handphone verifikasi kode -->
<div class="modal modal--simple fade" id="modalHandphoneVerificationCode" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <img width="50px" class="m-auto" src="./assets/images/icons/icon-register-whatsapp.svg"
                        alt="Icon Whatsapp">
                    <h3 class="mt-3">Masukan Kode Verifikasi</h3>
                    <p>Kode Verifikasi telah dikirimkan melalui
                        WhatsApp ke 0856-2439-7228</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <!-- start:verification -->
                            <div class="block__verification">
                                <div id="otp" class="d-flex justify-center">
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-0" maxlength="1" name="otpphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-1" maxlength="1" name="otpphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-2" maxlength="1" name="otpphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-3" maxlength="1" name="otpphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-4" maxlength="1" name="otpphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpphoneinp-5" maxlength="1" name="otpphone[]"/>
                                    <input type="hidden" name="otpphonefinal" value=""/>
                                </div>
                                <p id="resotpphone"></p>
                                <p class="information">Mohon tunggu dalam <b>30 Detik</b> untuk <a
                                        href="javascript:;" class="resendphone"><b>kirim
                                            ulang</b></a></p>
                            </div>
                            <!-- end:/verification -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextNewHandphone">Verifikasi</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Nomor Handphone baru -->
<div class="modal modal--simple fade" id="modalNewHandphone" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Masukan Nomor Handphone Baru</h3>
                    <p>Setelah memasukan Nomor Handphone Baru segera lakukan verifikasi untuk melanjutkan</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <form>
                                <div class="form-group">
                                    <label>Nomor Handphone Baru</label>
                                    <input type="tel" class="form-control" placeholder="Nomor Handphone Baru" name="newphone">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextHandphoneVerificationSms">Lanjut</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Nomor Handphone verifikasi kode SMS -->
<div class="modal modal--simple fade" id="modalHandphoneVerificationCodeSms" data-backdrop="static"
    data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <img width="50px" class="m-auto" src="{{asset('assets/images/icons/icon-sms-verifikasi.svg')}}" alt="Icon SMS">
                    <h3 class="mt-3">Masukan Kode Verifikasi</h3>
                    <p>Kode Verifikasi telah dikirimkan melalui
                        WhatsApp ke 0856-2439-7228</p>
                </div>
                <div class="block">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <!-- start:verification -->
                            <div class="block__verification">
                                <div id="otp" class="d-flex justify-center">
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-0" maxlength="1" name="otpnewphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-1" maxlength="1" name="otpnewphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-2" maxlength="1" name="otpnewphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-3" maxlength="1" name="otpnewphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-4" maxlength="1" name="otpnewphone[]"/>
                                    <input class="text-center form-control" type="tel" id="otpnewphoneinp-5" maxlength="1" name="otpnewphone[]"/>
                                    <input type="hidden" name="otpnewphonefinal" value=""/>
                                </div>
                                <p id="resotpnewphone"></p>
                                <p class="information">Mohon tunggu dalam <b>30 Detik</b> untuk <a
                                        href="javascript:;" class="resendphone"><b>kirim
                                            ulang</b></a></p>
                            </div>
                            <!-- end:/verification -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="nextHandphoneVerificationFinish">Verifikasi</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Nomor Handphone konfirmasi berhasil diubah -->
<div class="modal modal--simple fade" id="modalHandphoneVerificationFinish" data-backdrop="static" data-keyboard="false"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-title">
                    <h3>Nomor Handphone Diubah</h3>
                    <p>Nomor handphone 0878-8876-2913 Berhasil ditambahkan ke akun ini</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="window.location.reload();">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- end:/ Modal Nomor Handphone -->
