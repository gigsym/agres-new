<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use App\Models\ItemBest;
use App\Models\ItemHot;
use App\Models\ItemNewest;
use App\Models\ItemReview;
use App\Models\ItemSelect;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    // nunggu data product agres dulu
    public function itemFormat($items)
    {
        foreach ($items as $item) {
            $item_id = $item->id;

            // image
            $item->imagePrimary = null;
            if ($item->primaryImage != null) {
                $image = $item->primaryImage->img_path ?? '' . $item->primaryImage->img_path ?? '';
                $item->imagePrimary = $image;
            }
            // if ($image != null) {
            //     $item->image = $image->file;
            // } else {
            //     $image = ItemImage::where('item_id', $item_id)->first();
            //     if ($image != null) {
            //         $item->image = $image->file;
            //     }
            // }

            // rating
            $rating = ItemReview::where('item_id', $item_id)->get();
            $avg_rating = $rating->avg('rating');
            $total_rating = count($rating);
            $item->avg_rating = $avg_rating;
            $item->total_rating = $total_rating;

            // if ($item->price != null) {
            //     $price = $item->price;
            // } else {
            //     $price = $item->variants->product->price;
            // }
            $price = $item->price;
            $item->price = $price;

            // end date
            $end_date = new Carbon($item->end_deal);
            $diffInSeconds = Carbon::now()->diffInSeconds($end_date, false);
            if ($diffInSeconds > 1) {
                $dtF = new DateTime('@0');
                $dtT = new DateTime("@$diffInSeconds");
                $finalDiff =  $dtF->diff($dtT);
                $diff['day'] = $finalDiff->d;
                $diff['jam'] = $finalDiff->h;
                $diff['menit'] = $finalDiff->i;
                $diff['detik'] = $finalDiff->s;
                $item->end_date = $diff;
            }

            if ($item->discount_price != null && $diffInSeconds > 1) {
                // discount
                $discount = (($price - $item->discount_price) * 100) / $price;
                $item->discount = '-' . floor($discount) . '%';
                $item->discount_price = $item->discount_price;
            }
            // if ($item->description != null) {
            //     $description = $item->description;
            // } else {
            //     $description = $item->variants->product->description;
            // }
            $description = $item->description;
            $item->description = $description;

            // if ($item->category_id != null) {
            //     $cat = Category::where('id', $item->category_id)->first();
            //     if ($cat != null) {
            //         $item->category_name = $cat->name;
            //     }
            // } else {
            //     $cat = Category::where('id', $item->product->category_id)->first();
            //     if ($cat != null) {
            //         $item->product->category_name = $cat->name;
            //     }
            // }

            // if ($item->sub_category_id != null) {
            //     $subcat = Category::where('id', $item->sub_category_id)->first();
            //     if ($subcat != null) {
            //         $item->sub_category_name = $subcat->name;
            //     }
            // } else {
            //     $subcat = Category::where('id', $item->product->sub_category_id)->first();
            //     if ($subcat != null) {
            //         $item->product->sub_category_name = $subcat->name;
            //     }
            // }

            // $variant = ItemStockVariant::where('item_id', $item_id)->get();
            // $item->variants = $variant;
        }
        return $items;
    }

    public function getFilter(Request $request)
    {
        // page_section = ItemHot,ItemNewest,ItemSelect,ItemBest
        $page_section = 'itemhot';
        $category_id = 2;
        $specification = [];
        if ($page_section != null) {
            $section = ItemHot::with('product')->get();
            $items = collect([]);
            foreach ($section as $s) {
                $item_spec = json_decode($s->product->spesification);
                foreach ($item_spec as $k => &$v) {
                    if (array_key_exists($k, $specification)) {
                        if (!in_array($v, $specification[$k])) {
                            array_push($specification[$k], $v);
                        }
                    } else {
                        $specification[$k] = [$v];
                    }
                }
                $items->push($s->product);
            }
            $cats = Category::whereIn('id', $section->pluck('product.category_id')->toArray())->get();
        } else if ($category_id != null) {
            $category = Category::where('id', $category_id)->get();
            $item = Item::whereIn('category_id', $category->pluck('id')->toArray())->get();
            foreach ($item as $s) {
                $item_spec = json_decode($s->spesification);
                foreach ($item_spec as $k => &$v) {
                    if (array_key_exists($k, $specification)) {
                        if (!in_array($v, $specification[$k])) {
                            array_push($specification[$k], $v);
                        }
                    } else {
                        $specification[$k] = [$v];
                    }
                }
            }
            $items = $item;
            $cats = $category;
        }
        $d['category'] = $cats;
        $d['specification'] = $specification;
        $d['items'] = $items;

        return view('filter', $d);
    }
}
