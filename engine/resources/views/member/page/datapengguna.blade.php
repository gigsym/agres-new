<!-- start:block content -->
<div class="block__content">
    <!-- start:block content title -->
    <div class="block__content__title">
        <h2>Informasi Akun</h2>
    </div>
    <!-- end:/block content title -->

    <!-- start:information main -->
    <div class="information information--account">
        <!-- start:data pengguna -->
        <div class="information-title">Data Pengguna</div>
        <div class="information__content">
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Nama Lengkap</span>
                </div>
                <div class="col d-flex">
                    <span>{{$profile->name ?? '-'}}</span>
                    <a href="javascript:;" class="btn-action" data-toggle="modal"
                        data-target="#modalDataPengguna">Ubah</a>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Tanggal Lahir</span>
                </div>
                <div class="col d-flex">
                    <span>{{(Carbon\Carbon::parse($profile->birthday)->format('d F Y')) ?? '-'}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Jenis Kelamin</span>
                </div>
                <div class="col d-flex">
                    <span>
                        @if ($profile->gender == 0) Perempuan @elseif ($profile->gender == 1) Laki-laki
                        @else - @endif
                    </span>
                </div>
            </div>
        </div>
        <!-- end:/data pengguna -->

        <!-- start:login akun -->
        <div class="information-title">Login Akun</div>
        <div class="information__content">
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Email</span>
                </div>
                <div class="col d-flex">
                    <span>{{$profile->email}} <span class="text-verif">Terverifikasi</span></span>
                    <a href="javascript:;" class="btn-action" data-toggle="modal" data-target="#modalEmail">@if ($profile->email != null) Ubah @else Tambah @endif</a>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">No. Hp</span>
                </div>
                <div class="col d-flex">
                    <span>{{$profile->phone ?? '-'}}</span>
                    <a href="javascript:;" class="btn-action" data-toggle="modal" data-target="#modalHandphone">@if ($profile->phone != null) Ubah @else Tambah @endif</a>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Password</span>
                </div>
                <div class="col d-flex">
                    <span>**********</span>
                    <a href="javascript:;" class="btn-action">Ubah</a>
                </div>
            </div>
        </div>
        <!-- end:/login akun -->

        <!-- start:alamat pengiriman -->
        <div class="information-title">Alamat Pengiriman</div>
        <div class="information__content">
            @if($alamat_pengiriman != null)
            <div class="card card--address">
                <div class="card__header">
                    <h3>{{$alamat_pengiriman->address_name}}</h3>
                    <div class="label">Alamat Pengiriman</div>
                </div>
                <div class="card__content">
                    <h4>{{$alamat_pengiriman->name}}</h4>
                    <p>{{$alamat_pengiriman->address}}</p>
                    <p>
                        {{$alamat_pengiriman->area->name ?? ''}} -
                        {{$alamat_pengiriman->district->name ?? ''}} -
                        {{$alamat_pengiriman->cities->name ?? ''}} -
                        {{$alamat_pengiriman->province->name ?? ''}} -
                        {{$alamat_pengiriman->postal_code ?? ''}}</p>
                    <ul class="list-unstyled">
                        <li>Telp/HP : {{$alamat_pengiriman->phone}}</li>
                        <li>Email : {{$alamat_pengiriman->email}}</li>
                        <li><i class="fa fa-map-marker-alt"></i> <span class="ml-2">Pin lokasi
                                alamat sudah di set</span></li>
                    </ul>
                </div>
                <div class="card__footer">
                    <ul class="list-unstyled">
                        <li><button type="button" class="btn btn-link" disabled>Jadikan Alamat
                                Pengiriman</a></li>
                        <li><button type="button" class="btn btn-link active">Jadikan Alamat
                                Penagihan</a></li>
                        <li><button type="button" class="btn btn-link">Ubah</a></li>
                        <li><button type="button" class="btn btn-link" disabled>Hapus</a></li>
                    </ul>
                </div>
            </div>
            @else
            <button type="button" class="btn btn-primary">+ Tambah Alamat Baru</button>
            @endif
            <!-- end:/alamat pengiriman -->
        </div>
        <!-- end:/alamat pengiriman -->

        <!-- start:alamat penagihan -->
        <div class="information-title">Alamat Penagihan</div>
        <div class="information__content">
            @if($alamat_penagihan != null)
            <div class="card card--address">
                <div class="card__header">
                    <h3>{{$alamat_penagihan->address_name}}</h3>
                    <div class="label">Alamat Penagihan</div>
                </div>
                <div class="card__content">
                    <h4>{{$alamat_penagihan->name}}</h4>
                    <p>{{$alamat_penagihan->address}}</p>
                    <p>
                        {{$alamat_pengiriman->area->name ?? ''}} -
                        {{$alamat_pengiriman->district->name ?? ''}} -
                        {{$alamat_pengiriman->cities->name ?? ''}} -
                        {{$alamat_pengiriman->province->name ?? ''}} -
                        {{$alamat_pengiriman->postal_code ?? ''}}</p>
                    <ul class="list-unstyled">
                        <li>Telp/HP : {{$alamat_penagihan->phone}}</li>
                        <li>Email : {{$alamat_penagihan->email}}</li>
                        <li><i class="fa fa-map-marker-alt"></i> <span class="ml-2">Pin lokasi alamat
                                sudah di set</span></li>
                    </ul>
                </div>
                <div class="card__footer">
                    <ul class="list-unstyled">
                        <li><button type="button" class="btn btn-link active">Jadikan Alamat
                                Pengiriman</a></li>
                        <li><button type="button" class="btn btn-link" disabled>Jadikan Alamat
                                Penagihan</a></li>
                        <li><button type="button" class="btn btn-link">Ubah</a></li>
                        <li><button type="button" class="btn btn-link" disabled>Hapus</a></li>
                    </ul>
                </div>
            </div>
            @else
            <button type="button" class="btn btn-primary">+ Tambah Alamat Baru</button>
            @endif
        </div>
        <!-- end:/alamat penagihan -->
    </div>
    <!-- end:/information main -->
</div>
<!-- end:/block content -->
