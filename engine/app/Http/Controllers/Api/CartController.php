<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\MemberCart;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function getCart()
    {
        $user_id = auth('api')->user()->id;

        $carts = MemberCart::with('product')->where('member_id', auth('api')->user()->id)->get();

        if ($carts->isNotEmpty()) {
            $total_qty = 0;
            $total_price = 0;
            foreach ($carts as $cart) {
                $total_qty += $cart->quantity;
                $total_price += ($cart->product->price * $cart->quantity);

                $image = ItemImage::where('item_id', $cart->product->id)->where('is_primary', 1)->first();
                $cart->product->image = null;
                if ($image != null) {
                    $cart->product->image = $image->file;
                } else {
                    $image = ItemImage::where('item_id', $cart->product->id)->first();
                    if ($image != null) {
                        $cart->product->image = $image->file;
                    }
                }

                $cat = Category::where('id', $cart->product->category_id)->first();
                $cart->product->category_name = null;
                if ($cat != null) {
                    $cart->product->category_name = $cat->name;
                }

                $subcat = SubCategory::where('id', $cart->product->sub_category_id)->first();
                $cart->product->sub_category_name = null;
                if ($subcat != null) {
                    $cart->product->sub_category_name = $subcat->name;
                }
            }
            $usercart['item'] = $carts;
            $usercart['total_qty'] = $total_qty;
            $usercart['total_price'] = $total_price;
            return $this->responseBase($usercart);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }

    public function createCart(Request $request)
    {
        $item = Item::find($request->item_id);
        $create = MemberCart::create([
            "member_id" => auth('api')->user()->id,
            "item_id" => $request->item_id,
            // "price" => $item->price,
            "quantity" => $request->quantity,
        ]);

        if ($create->exists()) {
            return $this->responseBase([], 200, "success add cart");
        } else {
            return $this->responseBase([], 400, "failed add cart");
        }
    }

    public function deleteCart($id)
    {
        $detele = MemberCart::find($id);
        $detele->delete();
        return $this->getCart();
    }

    public function updateQuantity(Request $request, $id)
    {
        $qty = MemberCart::find($id);
        $qty->quantity = $request->quantity;
        $qty->save();

        return $this->getCart();
    }

    public function getSubTotal(Request $request)
    {
        $carts = MemberCart::with('product')->where('member_id', auth('api')->user()->id)->get();

        if ($carts->isNotEmpty()) {
            $total_price = 0;
            foreach ($carts as $item) {
                // ///////////////////////////////////////////////////////////////
                $total_price += ($item->item->price * $item->quantity);
                // ///////////////////////////////////////////////////////////////
            }
            return $this->responseBase($total_price);
        } else {
            return $this->responseBase([], 204, 'data is empty');
        }
    }
}
