<?php

namespace App\Http\Controllers\Member;
use App\Http\Controllers\Controller;
use App\Models\ItemReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiscussionController extends Controller
{
    public function index()
    {
        // get dari order tapi belu dibuat ordernya
        $addresses = order::with('')->where('member_id', Auth::user()->id)->get();
        return view();
    }

    public function reviewDetail($order_id)
    {
        $addresses = ItemReview::where('order_id', $order_id)->where('member_id',  Auth::user()->id)->get();
        return view();
    }
}
