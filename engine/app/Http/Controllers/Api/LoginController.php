<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Mail\verifRegister;
use App\Models\Member;
use App\Models\MemberPasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Request as FirebaseRequest;
use Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Ramsey\Uuid\Uuid;
use Image;

class LoginController extends Controller
{
    public function loginWaOtp(Request $request)
    {
        $member = null;
        $subno = $request->phone;
        if ($request->phone[0] == "+") {
            $subno = substr($request->phone, 3, strlen($subno));
        } else if (substr($request->phone, 0, 2) == "62") {
            $subno = substr($request->phone,  2, strlen($subno));
        } else if ($request->phone[0] == "0") {
            $subno = substr($request->phone, 1, strlen($subno));
        }
        $member = Member::where("phone", "like", "%" . $subno . "%")->first();

        if ($member == null) {
            return $this->responseBase([], 400, 'no hp tidak ditemukan');
        }

        $otp =  mt_rand(100000, 999999);
        $passres = MemberPasswordReset::where("member_id", $member->id)->first();
        if ($passres) {
            $passres->token = strtoupper($otp);
            $passres->type = "login";
            $passres->save();
        } else {
            $passres = MemberPasswordReset::create([
                "member_id" => $member->id,
                "token" => strtoupper($otp),
                "type" => 'login',
                "updated_at" => Carbon::now()
            ]);
        }

        $string = "otp anda: *" . strtoupper($otp) . "* expired dalam 15 menit.";
        $data["contact"] = $request->phone;
        $data["msg"] = $string;
        // $this->kirimOtp($data);

        return $this->responseBase(['otp' => $string], 200, 'berhasil kirim otp');
    }

    public function loginWaOtpVerif(Request $request)
    {
        $member = null;
        $subno = $request->phone;
        if ($request->phone[0] == "+") {
            $subno = substr($request->phone, 3, strlen($subno));
        } else if (substr($request->phone, 0, 2) == "62") {
            $subno = substr($request->phone,  2, strlen($subno));
        } else if ($request->phone[0] == "0") {
            $subno = substr($request->phone, 1, strlen($subno));
        }
        $member = Member::where("phone", "like", "%" . $subno . "%")->first();

        if ($member != null) {
            $checkotp = MemberPasswordReset::where("member_id", $member->id)->where("token", $request->otp)->where("type", "login")->first();
            if ($checkotp) {
                $now = Carbon::now();
                $minutes = $now->diffInMinutes($checkotp->updated_at);
                if ($minutes <= 15) {
                    $member->email_verified_at = Carbon::now();
                    $member->save();

                    $token = auth('api')->login($member);
                    $d['token'] = [
                        'access_token' => $token,
                        'token_type' => 'bearer',
                    ];
                    return $this->responseBase($d, 200, 'otp verified');
                } else {
                    return $this->responseBase([], 400, 'otp expired');
                }
            } else {
                return $this->responseBase([], 400, 'otp salah');
            }
        } else {
            return $this->responseBase([], 400, 'member tidak ditemukan');
        }
        return $this->responseBase([], 500, 'NOK ALL');
    }

    public function login(Request $request)
    {
        $login_type = filter_var($request->email_phone, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $credentials = [$login_type => $request->email_phone, 'password' => $request->password];
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = auth('api')->attempt($credentials)) {
                return $this->responseBase([], 400, 'salah email/password');
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->responseBase([], 500, 'server error cant create token');
        }
        if (empty(auth('api')->user()->email_verified_at)) {
            auth('api')->logout();
            return $this->responseBase([], 401, 'otp is not verified');
        }
        $token = [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
        return $this->responseBase($token, 200, 'sucess login');
    }

    public function register(Request $request)
    {
        $subno = $request->phone;
        if ($request->phone[0] == "+") {
            $subno = substr($request->phone, 3, strlen($subno));
        } else if (substr($request->phone, 0, 2) == "62") {
            $subno = substr($request->phone,  2, strlen($subno));
        } else if ($request->phone[0] == "0") {
            $subno = substr($request->phone, 1, strlen($subno));
        }

        if (Member::where('email', $request->password)->orWhere('phone', $subno)->first() != null) {
            return $this->responseBase([], 400, 'email / phone sudah di pakai');
        }
        $arr['phone'] = $subno;
        $arr['password'] = bcrypt($request->password);
        $arr['login_from'] = 1;
        //////////////////////////////////////////////////////////////////////////// bypass verif email
        $arr['email_verified_at'] = Carbon::now();
        $request->merge($arr);
        $member = Member::create($request->except('_token'));
        ///////////////////////////////
        $token = auth('api')->login($member);
        $d['token'] = [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
        // $token = Uuid::uuid4()->getHex();
        // MemberPasswordReset::create([
        //     'member_id' => $member->id,
        //     'token' => $token,
        //     'type' => 'verification'

        // ]);
        ////////////////////////////////
        $url = url('/') . '/member/verification?token=' . $token;

        $details = [
            'url' => $url,
        ];

        // Mail::to($member->email)->send(new verifRegister($details));
        return $this->responseBase($d, 200);
        ////////////////////////////////////////////////////////////////////////////
    }

    public function loginSosmed(Request $request)
    {
        $member = Member::where('email', $request->email)->first();
        if ($member != null) {
            $token = auth('api')->login($member);
            $d['token'] = [
                'access_token' => $token,
                'token_type' => 'bearer',
            ];
        } else {
            $member = new Member;
            $member->email = $request->email;
            // $member->password = bcrypt($request->token);
            $member->name = $request->name;
            $member->phone = $request->phone;
            $member->login_from = 2;

            if ($request->has('image')) {

                $image = file_get_contents($request->image);
                $destinationPath = 'assets/image/profile/';
                $img = Image::make($image);
                $ex = explode('/', $img->mime);
                $imageName =  uniqid() . '_' . str_replace(' ','_',$request->name) . '.' . $ex[1];
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $img->resize(674, 674, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $member->image = $destinationPath . '/' . $imageName;
            }
            $member->save();

            $token = auth('api')->login($member);
            $d['token'] = [
                'access_token' => $token,
                'token_type' => 'bearer',
            ];
        }
        return $this->responseBase($d, 200, 'login berhasil');
    }

    public function logout()
    {
        auth('api')->logout();
        return $this->responseBase([], 200, 'logout berhasil');
    }
}
