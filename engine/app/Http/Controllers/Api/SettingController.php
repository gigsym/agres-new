<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
    public function getSetting(){
        $setting = Setting::all();
        return response()->json($setting);
    }
}
