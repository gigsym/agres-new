<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->integer('id', true);
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->string('name');
            $table->bigInteger('price');
            $table->bigInteger('discount_price')->nullable();
            $table->timestamp('end_deal')->nullable();
            $table->integer('stock')->nullable();
            $table->string('description');
            $table->text('spesification')->nullable()->comment('field seperated comma');
            $table->integer('is_bestseller')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('sub_category_id')->nullable();
            $table->longText('variant')->nullable();
            $table->text('slug')->nullable();
            $table->integer('view_counter')->default(0);
            $table->integer('sold_counter')->default(0);
            $table->double('rating')->nullable()->default(0);
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
