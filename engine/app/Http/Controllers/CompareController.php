<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class CompareController extends Controller
{
    public function index(Request $request)
    {
        $product = Item::whereIn('id', $request->arr)->get();
        return view('mana atuh frontendnya',['products' => $product]);
    }
}
