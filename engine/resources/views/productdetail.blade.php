@extends('layouts.master')
@section('content-main')
<!-- start:product detail -->
<section class="product product--detail">
    <div class="container">
        <!-- start:breadcrumb navigation -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("/")}}">Home</a></li>
                <li class="breadcrumb-item"><a href="#">{{$data->category->name}}</a></li>
                <li class="breadcrumb-item"><a href="#">{{$data->subcategory->name}}</a></li>
                <li class="breadcrumb-item active">{{$data->name}}</li>
            </ol>
        </nav>
        <!-- end:/breadcrumb navigation -->

        <!-- start:product main -->
        <div class="product__main">
            <!-- start:product main header -->
            <div class="product__main__header">
                <div class="row">
                    <div class="col-lg-6 main-header__left">
                        <!-- start:carousel main -->
                        <div class="carousel-main">
                            <!-- start:carousel -->
                            <div id="carouselDetails" class="carousel slide" data-ride="carousel">
                                <div class="row">
                                    <div class="col-lg navigation">
                                        <ol class="carousel-indicators carousel-indicators--images">
                                            @foreach ($data->image as $image)
                                            <li data-target="#carouselDetails" data-slide-to="0" @if($loop->iteration ==
                                                1)class="active @endif">
                                                <img src="{{asset($image->img_path.$image->img_name)}}"
                                                    alt="Image indicator">
                                            </li>
                                            @endforeach
                                            {{--
                                            <li data-target="#carouselDetails" data-slide-to="1">
                                                <img src="./assets/images/products/product-2.jpg" alt="Image indicator">
                                            </li>
                                            <li data-target="#carouselDetails" data-slide-to="2">
                                                <img src="./assets/images/products/product-1.jpg" alt="Image indicator">
                                            </li>
                                            <li data-target="#carouselDetails" data-slide-to="3">
                                                <img src="./assets/images/products/product-2.jpg" alt="Image indicator">
                                            </li>
                                            <li data-target="#carouselDetails" data-slide-to="4">
                                                <img src="./assets/images/products/product-1.jpg" alt="Image indicator">
                                            </li> --}}
                                        </ol>
                                    </div>

                                    <div class="col-lg col-padless-left">
                                        <div class="carousel-inner">
                                            @foreach ($data->image as $image)
                                            <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                                <div class="carousel-item__box">
                                                    <img src="{{asset($image->img_path.$image->img_name)}}"
                                                        class="d-block w-100" alt="Image Preview">
                                                </div>
                                            </div>
                                            @endforeach
                                            {{-- <div class="carousel-item">
                                                <div class="carousel-item__box">
                                                    <img src="./assets/images/products/product-2.jpg"
                                                        class="d-block w-100" alt="Image Preview">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="carousel-item__box">
                                                    <img src="./assets/images/products/product-1.jpg"
                                                        class="d-block w-100" alt="Image Preview">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="carousel-item__box">
                                                    <img src="./assets/images/products/product-2.jpg"
                                                        class="d-block w-100" alt="Image Preview">
                                                </div>
                                            </div>
                                            <div class="carousel-item">
                                                <div class="carousel-item__box">
                                                    <img src="./assets/images/products/product-1.jpg"
                                                        class="d-block w-100" alt="Image Preview">
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/carousel -->
                        </div>
                        <!-- end:/carousel main -->
                    </div>
                    <div class="col-lg-6 main-header__right">
                        <div class="block">
                            <!-- start:tags -->
                            <ul class="block__tags list-unstyled">
                                @if($data->isNew())
                                <li><a href="#">New</a></li>
                                @endif
                                @if($data->end_deal != null)
                                <li><a href="#">Deals</a></li>
                                @endif
                            </ul>
                            <!-- end:/tags -->

                            <!-- start:title -->
                            <h2 class="block__title">
                                {{$data->name}}
                            </h2>
                            <!-- end:/title -->
                            <!-- start:rating -->
                            <div class="block__rating">
                                <div class="star-product" data-rating="{{$data->review->avg('rating')}}"></div>
                                <ul class="list-unstyled">
                                    <li>({{count($data->review)}} Ulasan)</li>
                                    <li>{{$data->sold_counter}} Terjual</li>
                                    <li>{{$data->view_counter}}x Dilihat</li>
                                </ul>
                            </div>
                            <!-- end:/rating -->

                            <!-- start:discount -->
                            <div class="block__discount" id="disc_box" style="visibility: hidden">
                                <div class="percent" id="disc">-</div>
                                <div class="price" id="disc_price">0</div>
                            </div>
                            <!-- end:/discount -->

                            <!-- start:price -->
                            <h3 class="block__price" id="main_price">0</h3>
                            <!-- end:/price -->

                            <!-- start:description -->
                            <p>Availability : <span class="color-red" id="stock">Pilih Warna</span></p>
                            <p>
                                <input type="hidden" class="countdownDate">
                                <p class="countdown">
                                </p>
                            </p>
                            <p class="font-small">
                                {{$data->description}}
                            </p>
                            <!-- end:/description -->

                            <!-- start:form -->
                            <form class="block__form">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Warna</label>
                                    <div class="col-sm-4">
                                        <select autocomplete="off" class="form-control" id="pickColor">
                                            <option selected disabled>Pilih warna disini</option>
                                            @foreach($variant as $item)
                                            <option value={{$item->stock}} data-price="{{$item->price}}"
                                                data-disc_price="{{$item->discount_price}}"
                                                data-disc="{{$item->discount}}" data-end_deal="{{$item->end_deal}}"
                                                data-stock="{{$item->stock}}" data-variant_id="{{$item->id}}">
                                                {{$item->variant}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jumlah</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-primary btnqty" disabled id="qty_down"
                                                    type="button">-</button>
                                            </div>
                                            <input type="text" id="qty_buy" class="form-control" placeholder="Jumlah"
                                                name="qty_buy" value="0">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btnqty" disabled id="qty_up"
                                                    type="button">+</button>
                                            </div>
                                        </div>
                                        <p class="font-italic" id="warning-qty" style="display: none">*Stok <span
                                                class="color-red">tersisa < 10</span>, beli sekarang!</p> </div> </div>
                                                    <!-- start:category and share social media -->
                                                    <div class="block__footer">
                                                        <div class="block__footer__tags">Categori :
                                                            {{$data->category->name}}, {{$data->subcategory->name}}
                                                        </div>
                                                        <div class="block__footer__share">
                                                            <span>Share</span>
                                                            <ul class="list-unstyled">
                                                                <li><button type="button" class="btn btn-sosmed"><i
                                                                            class="fab fa-facebook-f"></i></button></li>
                                                                <li><button type="button" class="btn btn-sosmed"
                                                                        disabled><i class="fab fa-twitter"></i></button>
                                                                </li>
                                                                <li><button type="button" class="btn btn-sosmed"
                                                                        disabled><i
                                                                            class="fab fa-instagram"></i></button></li>
                                                                <li><button type="button" class="btn btn-sosmed"
                                                                        disabled><i
                                                                            class="fab fa-google-plus-g"></i></button>
                                                                </li>
                                                                <li><button type="button" class="btn btn-sosmed"
                                                                        disabled><i
                                                                            class="fab fa-pinterest"></i></button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- end:/category and share social media -->
                                    </div>
                                </div>
                            </form> <!-- end:/form -->
                        </div>
                    </div>
                    <!-- end:/product main header -->

                    <!-- start:product main body -->
                    <div class="product__main__body">
                        <ul class="nav nav-pills" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#deskripsi" role="tab">Deskripsi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#spesifikasi" role="tab">Spesifikasi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#ulasan" role="tab">Ulasan</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <!-- start:deskripsi -->
                            <div class="tab-pane fade show active" id="deskripsi" role="tabpanel">
                                <p>
                                    {!!$data->longDescription->long_description!!}
                                </p>
                            </div>
                            <!-- end:/deskripsi -->

                            <!-- start:spesifikasi -->
                            <div class="tab-pane fade" id="spesifikasi" role="tabpanel">
                                <div class="tab-content__spec">
                                    @foreach (json_decode($data->spesification,true) as $key=>$item)
                                    <div class="spec__item">
                                        <div class="item-title">{{$key}}</div>
                                        <div class="item-description">{{$item}}</div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- end:/spesifikasi -->

                            <!-- start:ulasan -->
                            <div class="tab-pane fade" id="ulasan" role="tabpanel">
                                <div class="tab-content__blank">
                                    <div class="block-wrap">
                                        <img src="./assets/images/image-ulasan-detail.png" alt="Image ulasan empty">
                                        <h4>Belum Ada Ulasan Produk</h4>
                                        <p>Beli Sekarang dan Bagikan pendapatmu!</p>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/ulasan -->
                        </div>
                    </div>
                    <!-- end:/product main body -->
                </div>
                <!-- end:/product main -->
            </div>
</section>
<!-- end:/product detail -->

<!-- start:product rekomendasi -->
<section class="product product--detail mt-5">
    <div class="container">
        <!-- start:title section -->
        <div class="title-section">
            <h3>Kamu Mungkin Juga Suka</h3>
            <p>Rekomendasi Produk Berdasarkan Pencarianmu</p>
        </div>
        <!-- end:/title section -->

        <!-- start:product slider -->
        <div class="product__slider">
            <!-- start:slider -->
            <div class="slider slider--vertical">
                @foreach ($featured as $item)
                <div class="slider__box">
                    <div class="slider-wrap">
                        @if($item->is_bestseller == 1)
                        <div class="slider__box__label">BEST</div>
                        @endif
                        <div class="slider__box__favorite active">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                    fill="white" stroke="#B21628" />
                            </svg>
                        </div>
                        <div class="slider__box__image">
                            <img class="image-slider" src="https://picsum.photos/600/400" alt="image slider">
                        </div>
                        <div class="slider__box__content">
                            <div class="block">
                                <div class="block__tags">{{$item->category->name}}</div>
                                <div class="block__title">{{$item->name}}</div>
                                <div class="block__star">
                                    <div class="star-product" data-rating="2.5"></div>
                                </div>
                                @if($item->end_deal != null)
                                <div class="block__discount">
                                    <div class="percent">-20%</div>
                                    <div class="price">Rp. 60.999.000</div>
                                </div>
                                @endif
                                <div class="block__price">{{$item->price}}</div>
                                <div class="block__action">
                                    <div class="block__action__cart">
                                        <button type="button" class="btn btn-primary">+ Keranjang</button>
                                    </div>
                                    <div class="block__action__buy">
                                        <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <!-- end:/slider -->
        </div>
        <!-- end:/product slider -->
    </div>
</section>
<!-- end:/product rekomendasi -->

<!-- start:product detail floating -->
<section class="product-floating">
    <div class="container">
        <div class="product-floating__main">
            <div class="product-floating__left">
                <div class="image-wrap">
                    <img src="{{asset($data->imagePrimary->img_path.$data->imagePrimary->img_name)}}"
                        alt="Product image">
                </div>
                <p class="mb-0">{{$data->name}}</p>
            </div>
            <div class="product-floating__right">
                <div class="block">
                    <div class="block__price" id="add_to_cart_price">
                    </div>
                    <div class="block__action">
                        <a href="" class="addToWishlist" data-item_id="{{$data->id}}">
                            <div
                                class="block__action__favorite @if (Auth::check() && in_array($data->id, Auth::user()->wishlist->pluck('item_id')->toArray())) active @endif">
                                <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                        fill="white" stroke="#B21628" />
                                </svg>
                            </div>
                        </a>
                        <div class="block__action__cart">
                            <button type="button" class="btn btn-primary addToCart" data-item_id="{{$data->id}}"
                                data-price="{{$data->dicsount_price}}">+ Keranjang</button>
                        </div>
                        <div class="block__action__buy">
                            <button type="button" class="btn btn-primary">Beli Sekarang</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end:/product detail floating -->

@endsection

@push("js")
<script src="https://cdn.jsdelivr.net/npm/star-rating-svg@3.5.0/src/jquery.star-rating-svg.js"></script>
<script src="{{asset("assets/js/slick-slider.js")}}"></script>
<!-- end:/javascripts -->

<script>
    // Star rating in product
    $(".star-product").starRating({
      totalStars: 5,
      strokeColor: '#FDD835',
      readOnly: true,
      starSize: 18,
      useFullStars: true,
      useGradient: false
    });

    $(".star-featured").starRating({
      totalStars: 5,
      strokeColor: '#FDD835',
      readOnly: true,
      starSize: 14,
      useFullStars: true,
      useGradient: false
    });
</script>

<script>
    var count_now = 0;
    var timeout = null
    $("#pickColor").change(function(){
        var selected = $('#pickColor :selected');
        $("#stock").text(selected.data("stock"));
        $("#main_price").text("Rp." + number_format(selected.data("price"),0));
        $("#qty_buy").data("stock",selected.data("stock"));
        if (selected.data('disc') != null){
            $('#disc_box').css('visibility','visible')
            $('#disc').text(selected.data('disc'))
            $('#disc_price').text("Rp." + number_format(selected.data("price"),0))
            $("#main_price").text("Rp." + number_format(selected.data("disc_price"),0))
            $('.countdownDate').val(selected.data('end_deal'))
            init_countdown()
        }

        var count_now =parseInt($("#qty_buy").val());
        if(count_now > selected.data("stock")){
            $("#qty_buy").val(selected.data("stock"));
        }

        $(".btnqty").attr("disabled",false);
        if(selected.data("stock") < 10){
            $("#warning-qty").show();
        }else{
            $("#warning-qty").hide();
        }

        getSubTotal(selected.data('variant_id'),count_now)
    })

    $("#qty_up").click(function(){
        var count_now =parseInt($("#qty_buy").val());
        if(count_now < 0){
            count_now = 0;
        }
        count_now +=1;
        if(parseInt($("#qty_buy").data("stock")) < count_now){
            count_now =parseInt($("#qty_buy").data("stock"));
        }
        $("#qty_buy").val(count_now);

        var variant_id = $('#pickColor :selected').data('variant_id');
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            getSubTotal(variant_id,count_now)
        }, 500)
    })

    $("#qty_down").click(function(){
        var count_now =parseInt($("#qty_buy").val());
        count_now -=1;
        if(count_now < 0){
            count_now = 0;
        }
        $("#qty_buy").val(count_now);

        var variant_id = $('#pickColor :selected').data('variant_id');
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            getSubTotal(variant_id,count_now)
        }, 500)
    })

    function getSubTotal(variant_id,qty){
        $.ajax({
                url: `{{url('/')}}/getsubtotal`,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                data:{variant_id:variant_id,qty:qty},
                success: function (data){
                    $('#add_to_cart_price').text('').text("Rp." + number_format(data,0));
                }
            })
    }

</script>
@endpush
