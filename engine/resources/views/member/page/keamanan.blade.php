<!-- start:block content -->
<div class="block__content">
    <!-- start:block content title -->
    <div class="block__content__title">
        <h2>Keamanan</h2>
    </div>
    <!-- end:/block content title -->

    <!-- start:information main -->
    <div class="information information--account">
        <!-- start:login akun -->
        <div class="information-title">Login Akun</div>
        <div class="information__content">
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Email</span>
                </div>
                <div class="col d-flex">
                    <span>{{$profile->email}} @if ($profile->email_verified_at != null)<span class="text-verif">
                            Terverifikasi </span> @else <span class="text-verif"> Belum Terverifikassi </span>
                        @endif </span>
                    <a href="javascript:;" class="btn-action" data-toggle="modal" data-target="#modalEmail">
                        @if ($profile->phone != null) Ubah @else Tambah @endif
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">No. Hp</span>
                </div>
                <div class="col d-flex">
                    <span>{{$profile->phone}} @if ($profile->phone_verified_at != null)<span class="text-verif">
                            Terverifikasi </span> @else <span class="text-verif"> Belum Terverifikassi </span>
                        @endif</span>
                    <a href="javascript:;" class="btn-action" data-toggle="modal" data-target="#modalHandphone">
                        @if ($profile->phone != null) Ubah @else Tambah @endif</a>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <span class="font-light">Password</span>
                </div>
                <div class="col d-flex">
                    <span>**********</span>
                    <a href="javascript:;" class="btn-action">Ubah</a>
                </div>
            </div>
        </div>
        <!-- end:/login akun -->

        <!-- start:notifikasi OTP -->
        <div class="information-title">Notifikasi OTP</div>
        <div class="information__content">
            <div class="form-checkbox">
                <div class="form-check abc-radio form-check-inline">
                    <input class="form-check-input" type="radio" id="radio1" value="whatsapp" name="radioInline">
                    <label class="form-check-label" for="radio1">Whatsapp</label>
                </div>
                <div class="form-check abc-radio form-check-inline ml-4">
                    <input class="form-check-input" type="radio" id="radio2" value="sms" name="radioInline" checked>
                    <label class="form-check-label" for="radio2">SMS</label>
                </div>
            </div>
        </div>
        <!-- end:/notifikasi OTP -->

        <!-- start:tautkan akun -->
        <div class="information-title">Tautkan Akun</div>
        <div class="information__content">
            <div class="d-flex">
                <i class="fab fa-facebook-f align-self-center"></i>
                <span class="ml-2">Akun tertaut dengan : <span class="color-red">Singgih
                        Saputro</span></span>
                <a href="javascript:;" class="btn-action">Lepas Tautan</a>
            </div>
        </div>
        <!-- end:/tautkan akun -->
    </div>
    <!-- end:/information main -->
</div>
<!-- end:/block content -->
