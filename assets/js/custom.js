
$(window).on("load", function () {
    $("#loadingShow").hide("slow")
    // console.log($(".countdownDate").length);

    $(".block__price").each(function (i, obj) {
        var harga = $(obj).text();
        // console.log("harga diskon"+i+" "+harga)

        $(obj).text("Rp. " + number_format(harga, 0));
    });
    $(".block__discount > .price").each(function (i, obj) {
        var harga = $(obj).text();
        // console.log("harga biasa"+i+" "+harga);
        $(obj).text("Rp. " + number_format(harga, 0));
    });

})
function init_countdown() {
    $(".countdownDate").each(function (i, obj) {
        timer($(obj).val(), "countdown", i);
    });
}
function timer(date, className, index) {
    // Set the date we're counting down to
    var countDownDate = new Date(date).getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        var dayHTML = `<span class="color-red">` + days + `</span> Hari :`;
        var hourHTML = `<span class="color-red">` + hours + `</span> Jam :`;
        var minuteHTML = `<span class="color-red">` + minutes + `</span> Menit :`;
        var secondHTML = `<span class="color-red">` + seconds + `</span> Detik`;
        document.getElementsByClassName(className)[index].innerHTML = dayHTML + hourHTML + minuteHTML + secondHTML;

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            var dayHTML = `<span>` + 0 + `</span> Hari :`;
            var hourHTML = `<span>` + 00 + `</span> Jam :`;
            var minuteHTML = `<span>` + 00 + `</span> Menit :`;
            var secondHTML = `<span>` + 00 + `</span> Detik`;
            document.getElementsByClassName(className)[index].innerHTML = dayHTML + hourHTML + minuteHTML + secondHTML;
        }
    }, 1000);

}


function number_format(t, e) {
    t = (t + "").replace(/[^0-9+\-Ee.]/g, "");
    var a = isFinite(+t) ? +t : 0,
        n = isFinite(+e) ? Math.abs(e) : 0,
        o = ",",
        s = ".",
        u = "";
    return (
        (u = (n ?
            (function (t, e) {
                var i = Math.pow(10, e);
                return "" + (Math.round(t * i) / i).toFixed(e);
            })(a, n) :
            "" + Math.round(a)
        ).split("."))[0].length > 3 && (u[0] = u[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, o)),
        (u[1] || "").length < n && ((u[1] = u[1] || ""), (u[1] += new Array(n - u[1].length + 1).join("0"))),
        u.join(s + "")
    );
}