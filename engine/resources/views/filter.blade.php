@extends('layouts.master')
@section('content-main')
<!-- start:search result -->
<section class="search-result search-result--category">
    <div class="container">
        <div class="search-result__wrap">
            <!-- start:search result left -->
            <div class="search-result__left">
                <h4>Filter</h4>
                <div class="search-result__box">
                    <div class="accordion" id="accordionFilter">
                        <!-- start:filter item -->
                        <div class="card">
                            <div class="card-header">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapse_1">
                                        Kategori
                                    </button>
                                </h2>
                            </div>
                            <div id="collapse_1" class="collapse" data-parent="#accordionFilter">
                                <div class="card-body">
                                    <div class="box">
                                        <ul class="list-unstyled">
                                            @foreach ($category as $cat)
                                            <li>
                                                <!-- start:sub -->
                                                <div class="accordion accordion--sub" id="accordionSub-{{$cat->id}}">
                                                    <!-- start:sub parent -->
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <button class="btn btn-link collapsed" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#accordion-{{$cat->id}}">
                                                                <div class="form-check abc-checkbox">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="label-{{$cat->id}}" value="{{$cat->id}}" name="cat[]">
                                                                    <label class="form-check-label"
                                                                        for="label-{{$cat->id}}">
                                                                        {{$cat->name}}
                                                                    </label>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        @forelse ($cat->subcat as $subcat)
                                                        <div id="accordion-{{$cat->id}}" class="collapse"
                                                            data-parent="#accordionSub-{{$cat->id}}">
                                                            <div class="card-body children">
                                                                <!-- start:sub children -->
                                                                <ul class="list-unstyled">
                                                                    <li class="nav-item">
                                                                        <div class="form-check abc-checkbox">
                                                                            <input class="form-check-input" name="subcat[]"
                                                                                type="checkbox" value="{{$subcat->id}}"
                                                                                id="label-sub-{{$subcat->id}}">
                                                                            <label class="form-check-label"
                                                                                for="label-sub-{{$subcat->id}}">{{$subcat->name}}</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <!-- end:/sub children -->
                                                            </div>
                                                        </div>
                                                        @empty

                                                        @endforelse
                                                    </div>
                                                    <!-- end:/sub parent -->
                                                </div>
                                                <!-- end:/sub -->
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end:/filter item -->
                        @foreach ($specification as $k => $v)
                        <!-- start:filter item -->
                        <div class="card">
                            <div class="card-header">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#spec-{{$k}}">
                                        {{$k}}
                                    </button>
                                </h2>
                            </div>
                            <div id="spec-{{$k}}" class="collapse" data-parent="#accordionFilter">
                                <div class="card-body">
                                    <div class="box">
                                        <ul class="list-unstyled">
                                            @foreach ($v as $v)
                                            <li>
                                                <div class="card">
                                                    <div class="form-check abc-checkbox">
                                                        <input class="form-check-input" type="checkbox" id="label-subspec-{{$v}}" value="{{$v}}" name="spec[]">
                                                        <label class="form-check-label" for="label-subspec-{{$v}}">{{$v}}</label>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end:/filter item -->
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- end:/search result left -->

            <!-- start:search result right -->
            <div class="search-result__right">
                <h4><span class="font-weight-light">Menampilkan semua barang dengan kata</span> 'Asus'</h4>
                <!-- start:search result right filter -->
                <div class="search-result__right__filter">
                    <div class="filter-selected">
                        <div class="filter-selected__checkbox">
                            <div class="form-check abc-checkbox">
                                <input class="form-check-input" type="checkbox" id="filter-tags1" checked>
                                <label class="form-check-label" for="filter-tags1">Produk Promosi (40)</label>
                            </div>
                            <div class="form-check abc-checkbox">
                                <input class="form-check-input" type="checkbox" id="filter-tags2">
                                <label class="form-check-label" for="filter-tags2">Best Seller (20)</label>
                            </div>
                            <div class="form-check abc-checkbox">
                                <input class="form-check-input" type="checkbox" id="filter-tags2" checked>
                                <label class="form-check-label" for="filter-tags2">
                                    <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M8 0l2 5.95 6 .16-4.764 3.838L12.946 16 8 12.422 3.055 16l1.709-6.05L0 6.11l6-.162L8 0z"
                                            fill="#FDD835" /></svg>
                                    <span>4 Ke Atas</span>
                                </label>
                            </div>
                        </div>
                        <div class="filter-selected__price">
                            <form class="d-flex">
                                <span class="align-self-center font-weight-light mr-2">Harga</span>
                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Minimal"
                                            value="10.000.000">
                                    </div>
                                    <span class="mx-1 align-self-center font-weight-light">-</span>
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Maksimal">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="filter-action">
                        <button type="button" class="btn btn-primary btn-sm">Terapkan</button>
                        <button type="button" class="btn btn-outline-primary btn-sm">Batal</button>
                    </div>
                </div>
                <!-- end:/search result right filter -->

                <!-- start:search result right tags -->
                <div class="search-result__right__tags">
                    <div class="tags">
                        <!-- start:tags item -->
                        <div class="tags__item">
                            <span>Laptop</span>
                            <button class="btn btn-remove">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M11.301 10.358a.668.668 0 11-.943.944L8 8.943l-2.356 2.358a.667.667 0 01-.944-.943L7.057 8 4.7 5.642a.667.667 0 01.943-.943L8 7.057l2.358-2.358a.667.667 0 01.943.943L8.944 8l2.357 2.358zm2.36-8.017a8 8 0 100 11.32 8.013 8.013 0 000-11.32z"
                                            fill="#333" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <path fill="#fff" d="M0 0h16v16H0z" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </button>
                        </div>
                        <!-- end:/tags item -->

                        <!-- start:tags item -->
                        <div class="tags__item">
                            <span>Aksesoris</span>
                            <button class="btn btn-remove">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M11.301 10.358a.668.668 0 11-.943.944L8 8.943l-2.356 2.358a.667.667 0 01-.944-.943L7.057 8 4.7 5.642a.667.667 0 01.943-.943L8 7.057l2.358-2.358a.667.667 0 01.943.943L8.944 8l2.357 2.358zm2.36-8.017a8 8 0 100 11.32 8.013 8.013 0 000-11.32z"
                                            fill="#333" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <path fill="#fff" d="M0 0h16v16H0z" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </button>
                        </div>
                        <!-- end:/tags item -->

                        <!-- start:tags item -->
                        <div class="tags__item">
                            <span>Produk Promosi</span>
                            <button class="btn btn-remove">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M11.301 10.358a.668.668 0 11-.943.944L8 8.943l-2.356 2.358a.667.667 0 01-.944-.943L7.057 8 4.7 5.642a.667.667 0 01.943-.943L8 7.057l2.358-2.358a.667.667 0 01.943.943L8.944 8l2.357 2.358zm2.36-8.017a8 8 0 100 11.32 8.013 8.013 0 000-11.32z"
                                            fill="#333" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <path fill="#fff" d="M0 0h16v16H0z" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </button>
                        </div>
                        <!-- end:/tags item -->

                        <!-- start:tags item -->
                        <div class="tags__item">
                            <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M8 0l2 5.95 6 .16-4.764 3.838L12.946 16 8 12.422 3.055 16l1.709-6.05L0 6.11l6-.162L8 0z"
                                    fill="#FDD835" />
                            </svg>
                            <span>4 Ke Atas</span>
                            <button class="btn btn-remove">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M11.301 10.358a.668.668 0 11-.943.944L8 8.943l-2.356 2.358a.667.667 0 01-.944-.943L7.057 8 4.7 5.642a.667.667 0 01.943-.943L8 7.057l2.358-2.358a.667.667 0 01.943.943L8.944 8l2.357 2.358zm2.36-8.017a8 8 0 100 11.32 8.013 8.013 0 000-11.32z"
                                            fill="#333" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <path fill="#fff" d="M0 0h16v16H0z" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </button>
                        </div>
                        <!-- end:/tags item -->

                        <!-- start:tags button remove all -->
                        <div class="tags__remove">
                            <button class="btn btn-link">Hapus Semua</button>
                        </div>
                        <!-- end:/tags button remove all -->
                    </div>
                    <form>
                        <span class="align-self-center mr-2">Urutkan</span>
                        <div class="form-row">
                            <div class="col">
                                <select class="form-control">
                                    <option selected>Paling Sesuai</option>
                                    <option>Termurah</option>
                                    <option>Termahal</option>
                                    <option>Terbaru</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- end:search result right tags -->

                <!-- start:search result right list -->
                <div class="search-result__right__list">
                    <!-- start:product list -->
                    <div class="product-list list-5">
                        <div class="flex-container wrap">
                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-2.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-3.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-4.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-5.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-6.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-6.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-5.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-4.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-3.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-2.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->

                            <!-- start:product item -->
                            <div class="product-list__item">
                                <div class="item__label">BEST</div>
                                <div class="item__favorite">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M7.63759 2.05917L7.99964 2.43923L8.36167 2.05915C8.8244 1.57334 9.38351 1.18426 10.0053 0.916033C10.6265 0.6481 11.297 0.506476 11.9761 0.500001C13.8701 0.501438 15.5 2.33032 15.5 4.8917C15.5 7.66776 13.5944 11.6574 8.00228 13.4734C2.4056 11.6574 0.5 7.66475 0.5 4.8917C0.5 2.33026 2.12996 0.501448 4.0231 0.500001C4.70224 0.506518 5.37272 0.648161 5.99387 0.916093C6.61571 1.18432 7.17483 1.57339 7.63759 2.05917Z"
                                            fill="white" stroke="#B21628" />
                                    </svg>
                                </div>
                                <div class="item__image">
                                    <img class="image-slider" src="./assets/images/products/product-category-1.jpg"
                                        alt="image product">
                                </div>
                                <div class="item__content">
                                    <div class="block">
                                        <div class="block__tags">Laptop Gaming</div>
                                        <div class="block__title">Asus ROG Zephyrus S GX701GWR-I7R72T I7-9750H/32GB/1TB
                                            SSD/RTX2070...
                                        </div>
                                        <div class="block__star">
                                            <div class="star-small" data-rating="4"></div>
                                            <span>(25)</span>
                                        </div>
                                        <div class="block__discount">
                                            <div class="percent">-20%</div>
                                            <div class="price">Rp. 60.999.000</div>
                                        </div>
                                        <div class="block__price">Rp. 48.799.200</div>
                                        <div class="block__action">
                                            <div class="block__action__cart">
                                                <button type="button" class="btn btn-primary">+ Keranjang</button>
                                            </div>
                                            <div class="block__action__buy">
                                                <button type="button" class="btn btn-primary">Beli Sekarang</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end:/product item -->
                        </div>
                    </div>
                    <!-- end:/product list -->

                    <!-- start:pagination -->
                    <nav class="d-flex justify-content-center mt-4">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="javascript:;" aria-label="Previous">
                                    <svg width="8" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path d="M6.962 13.964L.88 7.5l6.082-6.463" stroke="#F44336"
                                                stroke-linecap="round" stroke-linejoin="round" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <path fill="#fff" transform="rotate(-90 7.5 7)" d="M0 0h14v7H0z" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:;">4</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:;">5</a></li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:;" aria-label="Next">
                                    <svg width="7" height="15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path d="M.538 1.036L6.62 7.5.538 13.963" stroke="#F44336"
                                                stroke-linecap="round" stroke-linejoin="round" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <path fill="#fff" transform="rotate(90 3.25 3.75)" d="M0 0h14v7H0z" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- end:/pagination -->
                </div>
                <!-- end:/search result right list -->
            </div>
            <!-- end:/search result right -->
        </div>
    </div>
</section>
<!-- end:/search result -->
@endsection
