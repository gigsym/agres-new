@extends('layouts.auth-master')
@section('content')

    <!-- start:acccounts -->
    <section class="accounts accounts--login">
        <div class="accounts__content">
            <!-- start:title -->
            <div class="title">
                <h3>Masuk ke Akunmu</h3>
                <p>Belum Punya Akun Agres.id? <a href="#">Klik Disini!</a></p>
            </div>
            <!-- end:/title -->

            <!-- start:form -->
            <form action="{{route('login.wa')}}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="tel" class="form-control" name="phone" required>
                    <label class="placeholder">Nomor Handphone</label>
                </div>
                <button type="submit" class="btn btn-primary d-block m-auto">Kirim Otp</button>
            </form>
            <!-- end:/form -->

            <!-- start:content bottom -->
            <div class="accounts__content__bottom">
                <p class="italic">Atau masuk menggunakan</p>
                <ul class="list-unstyled">
                    <li>
                        <a href="javascript:;" id="google_signin">
                            <img src="{{asset("assets/images/icons/icon-register-google.svg")}}" alt="Icon google">
                            <span>Google</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" id="facebook_signin">
                            <img src="{{asset("assets/images/icons/icon-register-facebook.svg")}}" alt="Icon facebook">
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/login">
                            <img src="{{asset("assets/images/icons/icon-register-email.svg")}}" alt="Icon whatsapp">
                            <span>Email</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- end:/content bottom -->
        </div>
    </section>
@endsection
