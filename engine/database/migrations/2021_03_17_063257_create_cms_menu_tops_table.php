<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsMenuTopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_menu_tops', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name', 50);
            $table->string('description')->nullable();
            $table->string('link', 100)->default('#');
            $table->integer('order')->default(0);
            $table->string('icon')->nullable();
            $table->timestamps();
            $table->softDeletes();        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_menu_tops');
    }
}
