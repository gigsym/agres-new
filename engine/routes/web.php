<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('offline', function () {
//     return view('vendor.laravelpwa.offline');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

// require __DIR__ . '/auth.php';
Route::get('/', 'HomeController@index')->name('home');

//temporary
Route::get('detail/{slug}', 'ItemController@show');
Route::post('compare', 'CompareController@index');

Route::post('getsubtotal', 'ItemController@getSubTotal');
Route::get('filter', 'FilterController@getFilter');


Route::prefix('member')->group(function () {
    Route::get('login/email', 'LoginController@showLoginEmail')->name('login.email');
    Route::post('login/email', 'LoginController@loginEmail')->name('login.email');
    Route::get('login/wa', 'LoginController@loginWa')->name('login.wa');
    Route::post('login/wa', 'LoginController@loginWaOtp')->name('login.wa');
    Route::get('login/wa-verif/{phone}', 'LoginController@loginWaVerif')->name('login.wa-verif');
    Route::post('login/wa-verif', 'LoginController@loginWaVerifOtp')->name('login.wa-verif');
    Route::post('login/sosmed', 'LoginController@loginWithSosmed');

    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::get('register', 'LoginController@showRegister')->name('register.email');
    Route::post('register', 'LoginController@register')->name('register.email');
    Route::get('verification', 'LoginController@verification')->name('register.verification');


    // Route::get('showlogin', 'LoginController@showLogin');
    // Route::get('showregister', 'LoginController@showRegister');
    // Route::post('register', 'LoginController@register');
    // Route::post('sendotplogin', 'LoginController@sendotp');subTotal

    Route::group(['middleware' => ['auth']], function () {
        Route::post('sendotp', 'Member\KeamananController@sendOtp');
        Route::post('verifyOtp', 'Member\KeamananController@verifyOtp');

        Route::resource('cart', 'Member\CartController');
        Route::post('cart/getsubtotal', 'Member\CartController@getSubTotal');
        Route::post('cart/updateqty', 'Member\CartController@updateQuantity');
        Route::resource('wishlist', 'Member\WishlistController');
        Route::resource('address', 'Member\AddressController');

        Route::resource('profile', 'Member\ProfileController');
        Route::resource('keamanan', 'Member\KeamananController');
    });
});
Route::get('{category}', 'ItemCategoryController@category');
Route::get('{category}/{sub_category}', 'ItemCategoryController@show'); //wuaaaaaaaaaa gelut dengan route yang lain wkwkwkkwk
