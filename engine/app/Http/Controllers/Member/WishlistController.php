<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\MemberWishlist;
use Illuminate\Http\Request;
use Auth;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlist = MemberWishlist::with('item')->where('member_id', Auth::user()->id)->get();

        return view('');
    }

    public function store(Request $request)
    {
        if (Auth::check()) {
            $create = MemberWishlist::create([
                "member_id" => Auth::user()->id,
                "item_id" => $request->item_id
            ]);
            return back()->with('success', 'Success add wishlist');
        } else {
            return back()->with('error', 'login terlebih dulu');
        }
    }

    public function destroy($id)
    {
        $detele = MemberWishlist::find($id);
        $detele->delete();

        return back()->with('success', 'Success add wishlist');
    }
}
