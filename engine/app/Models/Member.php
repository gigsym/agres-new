<?php

namespace App\Models;

use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable implements JWTSubject
{
    use HasFactory;
    protected $casts = [
        'created_at'  => 'date:d F Y',
        'email_verified_at'  => 'date:d F Y',
        'birthday'  => 'date:d F Y',
    ];

    protected $guarded = [];
    protected $hidden = [
        'password',
        'remember_token',
        // 'email_verified_at',
        // 'created_at',
        'updated_at',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    function address()
    {
        return $this->hasMany('App\Models\MemberAddress');
    }
    function primaryAddress()
    {
        return $this->hasOne('App\Models\MemberAddress')->where('is_primary', 1);
    }
    function wishlist()
    {
        return $this->hasMany('App\Models\MemberWishlist', 'member_id');
    }
    function cart()
    {
        return $this->hasMany('App\Models\MemberCart', 'member_id');
    }
}
