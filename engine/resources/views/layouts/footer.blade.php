<!-- start:footer -->
<footer class="footer">
    <!-- start:footer main -->
    <div class="footer__main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <!-- start:box -->
                    <div class="box">
                        <img class="logo" src="{{asset('/assets/images/logo-white.svg')}}" alt="Logo Agres ID">
                        <p class="mb-0">PT. AGRES INFO TEKNOLOGI (AGRES.ID)</p>
                        <p class="mb-0">Ruko Mangga Dua Square, Jl. Gn. Sahari No.1, RT.11/RW.6, Ancol, Pademangan,
                            North Jakarta City, Jakarta 14420</p>
                        <ul class="list-unstyled">
                            <li>Telpon : (021) 22681040</li>
                            <li>Mobile : +62 812-9700-9700</li>
                            <li>Email : info@agres.co.id</li>
                        </ul>
                    </div>
                    <!-- end:/box -->
                </div>
                <div class="col-lg-8">
                    <!-- start:block -->
                    <div class="block">
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- start:block menu descktop -->
                                <div class="block__menu block__menu--desktop row">
                                    <div class="col">
                                        <!-- start:box -->
                                        <div class="box">
                                            <h5 class="title-header">Kategori</h5>
                                            <ul class="list-unstyled">
                                                <li><a href="#">Bundling Office</a></li>
                                                <li><a href="#">Laptop</a></li>
                                                <li><a href="#">AIO & Desktop PC</a></li>
                                                <li><a href="#">PC / Desktop</a></li>
                                                <li><a href="#">Printer</a></li>
                                                <li><a href="#">Software</a></li>
                                                <li><a href="#">Aksesoris</a></li>
                                            </ul>
                                        </div>
                                        <!-- end:/box -->
                                    </div>
                                    <div class="col">
                                        <!-- start:box -->
                                        <div class="box">
                                            <h5 class="title-header">Sitemap</h5>
                                            <ul class="list-unstyled">
                                                <li><a href="#">Tentang Agres ID</a></li>
                                                <li><a href="#">Kontak</a></li>
                                                <li><a href="#">Member</a></li>
                                                <li><a href="#">Konfirmasi Pembayaran</a></li>
                                                <li><a href="#">Artikel</a></li>
                                                <li><a href="#">Promo</a></li>
                                                <li><a href="#">FAQ</a></li>
                                                <li><a href="#">Panduan Belanja</a></li>
                                                <li><a href="#">Informasi Umum</a></li>
                                            </ul>
                                        </div>
                                        <!-- end:/box -->
                                    </div>
                                </div>
                                <!-- end:/block menu descktop -->

                                <!-- start:block menu mobile -->
                                <div class="block__menu block__menu--mobile">
                                    <div class="accordion" id="accordionFooter">
                                        <div class="card">
                                            <div class="card-header">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link collapsed" type="button"
                                                        data-toggle="collapse" data-target="#collapse_1">
                                                        Kategori
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse_1" class="collapse" data-parent="#accordionFooter">
                                                <div class="card-body">
                                                    <div class="box">
                                                        <ul class="list-unstyled">
                                                            <li><a href="#">Bundling Office</a></li>
                                                            <li><a href="#">Laptop</a></li>
                                                            <li><a href="#">AIO & Desktop PC</a></li>
                                                            <li><a href="#">PC / Desktop</a></li>
                                                            <li><a href="#">Printer</a></li>
                                                            <li><a href="#">Software</a></li>
                                                            <li><a href="#">Aksesoris</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link collapsed" type="button"
                                                        data-toggle="collapse" data-target="#collapse_2">
                                                        Sitemap
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse_2" class="collapse" data-parent="#accordionFooter">
                                                <div class="card-body">
                                                    <div class="box">
                                                        <ul class="list-unstyled">
                                                            <li><a href="#">Tentang Agres ID</a></li>
                                                            <li><a href="#">Kontak</a></li>
                                                            <li><a href="#">Member</a></li>
                                                            <li><a href="#">Konfirmasi Pembayaran</a></li>
                                                            <li><a href="#">Artikel</a></li>
                                                            <li><a href="#">Promo</a></li>
                                                            <li><a href="#">FAQ</a></li>
                                                            <li><a href="#">Panduan Belanja</a></li>
                                                            <li><a href="#">Informasi Umum</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end:/block menu mobile -->
                            </div>
                            <div class="col-lg-6">
                                <!-- start:box -->
                                <div class="box">
                                    <h5 class="title-header">Sign Up Newsletter</h5>
                                    <p>Daftar & dapatkan update terbaru serta penawaran spesial.</p>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Masukan Email Anda Disini">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">Sign Up</button>
                                        </div>
                                    </div>
                                    <ul class="box__social-media list-unstyled">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                                <!-- end:/box -->
                            </div>
                        </div>
                    </div>
                    <!-- end:/block -->
                </div>
                <div class="col-lg-12">
                    <p class="mb-0">Agres.id menjual berbagai macam kategori produk laptop, gadget dan komputer. Mulai
                        dari Ultrabook, PC desktop, PC
                        tablet, Printer, Projector, Scanner, Software, hingga berbagai macam peripheral dan aksesoris
                        baik untuk laptop maupun
                        PC desktop. Anda dapat melihat lokasi dimana toko-toko cabang Agres.id</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end:/footer main -->

    <!-- start:footer copyright -->
    <div class="footer__copyright">
        <p>Copyright © 2021 AGRES. All Rights Reserved</p>
    </div>
    <!-- end:/footer copyright -->
</footer>
<!-- end:/footer -->
