<div class="container-header">
    <div class="container" id="main_container">
        <header>
            
            <div class="navbar-second">
                <div class="navbar">
                    <nav class="nav">
                        <a class="nav-link" href="#">Tentang Agres</a>
                        <a class="nav-link" href="#">Kontak</a>
                        <a class="nav-link" href="#">Member</a>
                        <a class="nav-link" href="#">Konfirmasi Pembayaran</a>
                        <a class="nav-link" href="#">Artikel</a>
                        <a class="nav-link" href="#">Promo</a>
                    </nav>
                    <nav class="nav justify-content-end">
                        <a class="nav-link" href="#">Masuk</a>
                        <a class="nav-link" href="#">Daftar</a>
                        <a class="nav-link" href="#">Wishlist</a>
                        <a class="nav-link" href="#">Keranjang</a>
                    </nav>
                </div>
            </div>
            <div class="navbar-first">
                <nav class="navbar navbar-expand-lg navbar-light bg-white rounded shadow">
                    <a class="navbar-brand" href="#">
                        <img class="logo-homepage" src="<?php echo e(asset('assets/image/logo/agres-id-logo-color.png')); ?>" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Bundling Office</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Laptop</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    AIO & Desktop PC
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">PC / Desktop</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Printer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Software</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Aksesoris</a>
                            </li>
                        </ul>
                        <form class="form-inline">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="cari produk disini"
                                    aria-label="cari produk disini" aria-describedby="basic-addon1">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1"><img class="icon-search"
                                            src="<?php echo e(asset('assets/image/icon/icon-search.png')); ?>"
                                            alt="agres-icon"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </nav>
            </div>
            
            
            <div class="homepage-banner">
                <div>
                    <img class="banner-image" src="<?php echo e(asset('assets/image/banner/banner-home.jpg')); ?>" alt="">
                </div>
            </div>
            
        </header>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\agres-server\engine\resources\views/layouts/header.blade.php ENDPATH**/ ?>