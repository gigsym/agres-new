<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemBest;
use App\Models\ItemHot;
use App\Models\ItemNewest;
use App\Models\ItemSelect;
use App\Models\ItemStockVariant;
use App\Models\MemberCart;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;

        $carts = MemberCart::with('item')->where('member_id', Auth::user()->id)->get();

        if ($carts->isNotEmpty()) {
            $total_qty = 0;
            $total_price = 0;
            foreach ($carts as $item) {
                $total_qty += $item->quantity;
                $total_price += ($item->item->price * $item->quantity);
            }
            $usercart['item'] = $carts;
            $usercart['total_qty'] = $total_qty;
            $usercart['total_price'] = $total_price;
        }
        return view('', $usercart);
    }

    public function store(Request $request)
    {
        $create = MemberCart::create([
            "member_id" => Auth::user()->id,
            "varian_id" => $request->varian_id,
            "quantity" => $request->quantity ?? 1,
        ]);

        return back()->with('success', 'Success add to cart');
    }

    public function destroy($id)
    {
        $detele = MemberCart::find($id);
        $detele->delete();
        return back()->with('success', 'Success delete cart item');
    }

    public function updateQuantity(Request $request, $id)
    {
        $qty = MemberCart::find($id);
        $qty->quantity = $request->quantity;
        $qty->save();

        return back()->with('success', 'Success update cart item');
    }

    public function getSubTotal(Request $request)
    {
        // arr = [
        //     0 : {
        //         'variant_id': 1,
        //         'qty':1
        //     },
        //     1 : {
        //         'variant_id': 2,
        //         'qty':1
        //     }
        // ]

        $total_item = count($request->arr);
        $disc_price = 0;
        $normal_price = 0;


        foreach ($request->arr as $arr) {
            $item = ItemStockVariant::find($arr['varian_id']);

            $normal_price += $item->price * $arr['qty'];

            if (($item->discount_price != null) && (Carbon::now()->diffInSeconds(Carbon::parse($item->end_deal), false) > 1)) {
                $disc_price += $item->discount_price * $arr['qty'];
            } else {
                $disc_price += $item->price * $arr['qty'];
            }
        }

        $d['normal_price'] = $normal_price;
        $d['total_item'] = $total_item;
        $d['discount_price'] = $disc_price;
        return response($d);
    }

    public function checkVoucher(Request $request)
    {
        $voucher = Voucher::where('code', $request)->first();
        if ($voucher != null) {
            if ($voucher->item_id == 'ALLPRODUCT' || $voucher->item_id == $request->item_id) {
                if ($request->subtotal > $voucher->min_subtotal) {
                    if ($voucher->discount_type == 'AMOUNT') {
                        $potongan = $voucher->discount;
                        $new_subtotal = $request->subtotal - $voucher->potongan;
                    } else {
                        $potongan = ($request->subtotal * $voucher->discount) / 100;
                        $new_subtotal = $request->subtotal - $potongan;
                    }
                    return (['status' => 1, 'potongan', $potongan, 'new_subtotal' => $new_subtotal]);
                } else {
                    return (['status' => 0, 'message', $voucher->subtotal_message]);
                }
            } else {
                return (['status' => 0, 'message', 'Code voucher salah']);
            }
        } else {
            return (['status' => 0, 'message', 'Code voucher salah']);
        }
    }
}
