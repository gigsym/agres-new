<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
use SoftDeletes;
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function recItem()
    {
        return $this->hasMany('App\Models\Item')->orderBy('created_at', 'desc')->limit(12);
    }
}
