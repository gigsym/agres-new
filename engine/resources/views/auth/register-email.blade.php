@extends('layouts.auth-master')
@section('content')

<!-- start:acccounts -->
<section class="accounts accounts--lg">
    <div class="accounts__content">
        <div class="row">
            <div class="col-lg-6 align-self-center">
                <div class="block">
                    <!-- start:title -->
                    <div class="title">
                        <h3>Daftar Sekarang!</h3>
                        <p>Sudah Punya Akun Agres.id? <a href="{{route('login.email')}}">Klik Disini!</a></p>
                    </div>
                    <!-- end:/title -->

                    <!-- start:form -->
                    <form action="{{route('register.email')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email" class="form-control @error('email') invalid @enderror"
                                value="{{old('email')}}" required>
                            <label class="placeholder">Email</label>
                            <span class="icon icon--close"></span>
                            @error('email')
                            <p class="invalid-info">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password-field" type="password" name="password"
                                class="form-control @error('password') invalid @enderror" value="" required>
                            <label class="placeholder">Password</label>
                            <span toggle="#password-field" class="icon icon--eye close toggle-password"></span>
                            @error('password')
                            <p class="invalid-info">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check abc-checkbox abc-checkbox-primary">
                                <input class="form-check-input" name="policy" id="policy" type="checkbox">
                                <label class="form-check-label form-check-label--normal" for="policy">
                                    <em>
                                        Dengan klik daftar, kamu telah menyetujui
                                        <a href="#"><b>Aturan Penggunaan</b></a> serta
                                        <a href="#"><b>Kebijakan Privasi</b></a> dari Agres ID
                                    </em>
                                </label>
                                @error('policy')
                                <p class="invalid-info">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-check abc-checkbox abc-checkbox-primary mt-2">
                                <input class="form-check-input" name="subscribe" id="subscribe" type="checkbox">
                                <label class="form-check-label form-check-label--normal" for="subscribe">
                                    <em>Berlangganan Newsletter</em>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary d-block m-auto">Daftar</button>
                    </form>
                    <!-- end:/form -->

                    <!-- start:content bottom -->
                    <div class="accounts__content__bottom">
                        <p class="italic">Atau daftar menggunakan</p>
                        <ul class="list-unstyled">
                            <li>
                                <a href="javascript:;" id="google_signin">
                                    <img src="{{asset("assets/images/icons/icon-register-google.svg")}}"
                                        alt="Icon google">
                                    <span>Google</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" id="facebook_signin">
                                    <img src="{{asset("assets/images/icons/icon-register-facebook.svg")}}"
                                        alt="Icon facebook">
                                    <span>Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/login">
                                    <img src="{{asset("assets/images/icons/icon-register-email.svg")}}"
                                        alt="Icon whatsapp">
                                    <span>Email</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- end:/content bottom -->
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="block">
                    <img width="90%" class="d-block m-auto" src="{{asset('/assets/images/image-register.png')}}"
                        alt="Image register">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end:/acccounts -->
@endsection
@push('js')
<script>
    $(".toggle-password").click(function () {
      $(this).toggleClass("open");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
</script>
@endpush
